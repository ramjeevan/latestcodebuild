//calls on page load
$("td a").on("click", function() {
    $("body").scrollTop(0);
});
$(document).ready(function(){
       loadGif();
       setTargetData();
  });
var DREdata;
var defectsData;
var reviewData;
var DRTable;
var defectsTable;
var SRFGraph;
var SRFTable;
var buttonData;
var atpData;
var cGraph;
var cData;
var appData;
var aspectData;
var complaintData;
var velocityChart;
var rgraph;
var rtable;
var cidiData;
var requirementData;
	    /*   $(window).resize(function(){
	    	 
	    	}); */
//ajax for target
function setTargetData(){  	
	  $.ajax({
          url: "getTargetData",
          beforeSend: function (request) {
              var token = $('meta[name="token"]').attr("content");
              request.setRequestHeader("Authorization", "JWT " + token);
          }
        }).done(function(data) {
        	getCSATData();
     	   getMetricsData();
     	   getRequirementsData();
     	   getCIDIData();
        }); 
	   
  }
$("#moreTab").click(function(){
	$(this).parent().parent().find('li a.active').removeClass('active');
});
	    	function atp(){
	    		window.open("https://seneca-global.atlassian.net/wiki/spaces/JPT/pages/60391427/About+the+Project","_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function sc(){
	    		window.open("https://seneca-global.atlassian.net/wiki/spaces/JPT/pages/72548359/Project+Requirements+And+Client+Supplied+Items","_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function ccc(){
	    		window.open("https://seneca-global.atlassian.net/wiki/spaces/JPT/pages/60522526/Configuration+and+Change+Control","_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	function te(){
	    		window.open("https://seneca-global.atlassian.net/wiki/spaces/JPT/pages/60522521/Technology+Environment","_blank","width=1000,height=500, scrollbars=1");
	    	}
	    	//Client Satisfaction Rating Graph
	    	google.charts.load('current', {'packages':['line']});
	    	function drawCSATChart(x) {
	    	      var values2 = x;    
	    	      var results1 = JSON.parse(values2);    
	    	       var data = new google.visualization.DataTable();
	    	        data.addColumn('string', 'Period');
	    	        data.addColumn('number', 'CSAT Rating');
	    	        // A column for custom tooltip content
	    	        data.addColumn({type: 'string', role: 'tooltip'});
	    	        data.addColumn({type: 'string', role: 'annotation'});
	    	        data.addColumn('number','Target(<=)');
	    	        data.addRows(results1);
	    	  		var options = { 
	    	  				title: 'CSAT Rating [Target>=3]',
	    	  	    		 titleFontSize:17,
	    	  				 hAxis: {title: 'Period'},
	    	  	             vAxis: {title: 'CSAT Rating',format: '0', viewWindow:{min:0, max:5}, gridlines:{count:6}},
	    	  	              pointsVisible: true, 
	    	  	             focusTarget: 'category',
	    	  	             series: {
	    	  	   	    	  0:{color: '#097138'},
	    	  	   	    	  1:{ color: 'red',
	    	  	                 	   pointsVisible: false }
	    	  	   	      },
	    	  	             is3D:true
	    	       }; 
	    	  		loadData('csat_div');
	    	  		var chart = new google.visualization.LineChart(document.getElementById('csat_div'));
	    	  		chart.draw(data, options);
	    	      }
	    	//Risk Exposure Rating
	    	function drawRiskRatingGraph(x) {
	    	    var values2 = x;
	    	   
	    	   var results1 = JSON.parse(values2);
	    	    
	    	     var data = new google.visualization.DataTable();
	    	      data.addColumn('string', 'Month');
	    	      data.addColumn('number', 'Before Risk Treatment');
	    	      data.addColumn({type: 'string', role: 'tooltip'});
	    	      data.addColumn({type: 'string', role: 'annotation'});
	    	      data.addColumn('number', 'After Risk Treatment');
	    	      data.addColumn({type: 'string', role: 'tooltip'});
	    	      data.addColumn({type: 'string', role: 'annotation'});
	    	      data.addColumn('number','Target(>=)');
	    	      data.addRows(results1); 
	    	     var options = {
	    	    		 title: 'Risk Exposure Rating [Target <= 1]', 
	    	    		 titleFontSize:16,
	    	    		 hAxis: {
	    	    	          title: 'Months'
	    	    	        },
	    	    	        vAxis: {
	    	    	          title: 'Risk Exposure Rating',format: '0', viewWindow:{min:0, max:5}, gridlines:{count:6},ticks :[0,1,2,3,4,5]
	    	    	        },
	    	    	        pointsVisible: true,
	    	    	      focusTarget: 'category',
	    	    	      series: {
	    	    	    	  1:{color: '#097138'},
	    	    	    	  2:{ color: 'red',
	    	                  	   pointsVisible: false }
	    	    	      }
	    	    	      };

	    			var chart = new google.visualization.LineChart(document.getElementById('riskrating_div'));

	    		       chart.draw(data, options);
	    	    }  
	    	//DRE Graph
	    	function drawDREChart(x) {
	    	    var values2 = x;
	    	   var results1 = JSON.parse(values2);
	    	   var data = new google.visualization.DataTable();
	    	   data.addColumn('string', 'Month');
	    	   data.addColumn('number', 'DRE (%)');
	    	   data.addColumn({type: 'string', role: 'annotation'});
	    	   data.addColumn('number','Target(%)');
	    	   data.addRows(results1);
	    	    var options = { 
	    	           title: 'Defect Removal Efficiency [Target= 97%]',
	    	           titleFontSize:17,
	    	           hAxis: {title: 'Months'},
	    	           vAxis: {title: 'Defect Removal Efficiency(%)', format: '#', viewWindow:{min:0, max:100},ticks :[0,25,50,75,100]},
	    	           pointsVisible: true,
	    	           series: {
	    	               1: { color: 'red',
	    	            	   pointsVisible: false }
	    	               }
	    	     }; 
	    	    var chart = new google.visualization.LineChart(document.getElementById('graph'));

	    	         chart.draw(data, options);
	    	    }     
	    	//Defects Data Distribution Graph
	    	google.charts.load('current', {'packages':['corechart','bar','line']});
	    	function drawBarGraph(x) {
	    	    var values = x;
	    	    var result1 = JSON.parse(values);
	    	    var data = new google.visualization.DataTable();
	    	    data.addColumn('string', 'Month');
	    	    data.addColumn('number', 'Delivery Defects');
	    	    data.addColumn({type: 'string', role: 'annotation'});
	    	    data.addColumn('number', 'Testing Defects');
	    	    data.addColumn({type: 'string', role: 'annotation'});
	    	    data.addColumn('number', 'Review Defects');
	    	    data.addColumn({type: 'string', role: 'annotation'});
	    	    data.addRows(result1);
	    	    var options = {
	    	    		/*chart: {
	    			          title: 'CSAT Rating',
	    			          subtitle: 'Target <= 3'
	    			        },*/
	    	            title: 'Defects Count',
	    	            titleFontSize:16,
	    	            hAxis: {title: 'Months'},
	    	            vAxis: {title: 'Count of defects',format: '#',ticks :[0,2,4,6,8,10,12]},
	    	            series: {
	    	              0: { color: 'red' }, 
	    	              1: { color: '#ff6600' },
	    	              2: {color: 'orange'}
	    	            }
	    	      }; 

	    	      var chart = new google.visualization.ColumnChart(document.getElementById('graph'));
	    	    
	    	      chart.draw(data, options);
	    	    }
	    	//Review Effectiveness Graph
	    	function drawLineChart(x) {
	    	    var values2 = x;
	    	   var results1 = JSON.parse(values2);
	    	   var data = new google.visualization.DataTable();
	    	   data.addColumn('string', 'Month');
	    	   data.addColumn('number', 'Review Effectiveness (%)');
	    	   data.addColumn({type: 'string', role: 'annotation'});
	    	   data.addColumn('number','Target(%)');
	    	   data.addRows(results1);
	    	  var options = { 
	    	           title: 'Review Effectiveness % [Target = 60%]',
	    	           titleFontSize:16,
	    	           hAxis: {title: 'Months'},
	    	           vAxis: {title: 'Review Effectiveness %', format: "#",viewWindow:{min:0, max :100},ticks: [0,25,50,75,100]},
	    	           pointsVisible: true,
	    	           series: {
	    	               1: { color: 'red',
	    	            	   pointsVisible: false }
	    	               }
	    	     }; 
	    	  var chart = new google.visualization.LineChart(document.getElementById('graph'));

	    	       chart.draw(data, options);
	    	    }          
	    	//SRF Graph
	    	function drawSRFGraph(x) {
	    	    var values2 = x;  
	    	   var results1 = JSON.parse(values2);
	    	   var data = new google.visualization.DataTable();
	    	   data.addColumn('string', 'Month');
	    	   data.addColumn('number', 'Software Requirement Functioning(%)');
	    	   data.addColumn({type: 'string', role: 'annotation'});
	    	   data.addColumn('number','Target(%)');
	    	   data.addRows(results1);
	    			var options = { 
	    	           title: 'Software Requirements Functioning (%) [target = 97%] ',
	    	           titleFontSize:16,
	    	           hAxis: {title: 'Months'},
	    	           vAxis: {title: 'SRF (%)', format: '#', viewWindow:{min:0, max:100},ticks: [0,25,50,75,100]},
	    	           pointsVisible: true,
	    	           series: {
	    	               1: { color: 'red',
	    	            	   pointsVisible: false }
	    	               }
	    	     }; 
	    			var chart = new google.visualization.LineChart(document.getElementById('graph'));

	    		       chart.draw(data, options);
	    	    }      
//Able toggle buttons (graph and table)
function buttonAble(a,b){
	console.log(a + b);
	document.getElementById(a).disabled = false;
    document.getElementById(a).style.cursor = 'pointer';
    document.getElementById(b).disabled = false;
    document.getElementById(b).style.cursor = 'pointer';
}
//Disable toggle buttons (graph and table)
function buttonDisable(a,b){
	document.getElementById(a).disabled = true;
  document.getElementById(a).style.cursor = 'default';
  document.getElementById(b).disabled = true;
  document.getElementById(b).style.cursor = 'default';
}
//Show graph
function showGraphs(a , b){
	document.getElementById(a).style.display = 'block'; 
	document.getElementById(a).style.width = '100%'; 
  	document.getElementById(b).style.display = 'none';
}
//Show Table
function showData(a,b){
		  document.getElementById(a).style.display = 'none';
		  document.getElementById(b).style.display = 'block';
}
//Maximize Card
$(document).ready(function () {
    $("#maximize").click(function (e) {
    	e.preventDefault();
       
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
    });
    $("#maximize1").click(function (e) {
    	e.preventDefault();
       
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
    });
    $("#maximize3").click(function (e) {
    	e.preventDefault();
    	
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
            /*$("#tblDre").removeClass('fixTable');
            $(".zui-sticky-col,.tableh").removeClass('zui-sticky-col tableh');*/
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
            /*$("#tblDre").addClass('fixTable');
            $("#tblDre tr th:nth-child(1)").addClass('zui-sticky-col tableh');
            $("#tblDre tr td:nth-child(1)").addClass('zui-sticky-col');*/
        }
        $(this).closest('.card').toggleClass('maximize');

        $("#tblDre tr th").css('min-height','0px !important');
        var maxHeight = Math.max.apply(null, $("#tblDre tr th").map(function ()
   				{
   				    return $(this).height();
   				}).get());
   		$("#tblData tr th").css('min-height',maxHeight + 'px !important');
   		
   		$("#tblData tr th").css('min-height','0px !important');
        var height = Math.max.apply(null, $("#tblData tr th").map(function ()
   				{
   				    return $(this).height();
   				}).get());
   		
   		$("#tblData tr th").css('min-height',height + 'px !important');
    });
    $("#maximize4").click(function (e) {
    	e.preventDefault();
       
    	var $this = $(this);
    
        if ($this.children('i').hasClass('fa fa-expand'))
        {
            $this.children('i').removeClass('fa fa-expand');
            $this.children('i').addClass('fa fa-compress');
        }
        else if ($this.children('i').hasClass('fa fa-compress'))
        {
            $this.children('i').removeClass('fa fa-compress');
            $this.children('i').addClass('fa fa-expand');
        }
        $(this).closest('.card').toggleClass('maximize');
    });
});
//ajax for CSAT data
function getCSATData(){  	
  	  $.ajax({
            url: "clientSatisfactionControl",
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            }
          }).done(function(data) {
          	loadData("cTable");
          	cGraph =  $($.parseHTML(data)).filter("#CSAT"); 
          	cData =  $($.parseHTML(data)).filter("#table");
          	google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
          	$("#cTable").html(cData);   
          }); 
  	   
    }
//load CSAT Data
function loadCSAT(){
    buttonAble("one","two");
    google.charts.setOnLoadCallback(drawCSATChart(cGraph.text()));
    $("#cTable").html(cData);
    }
//Ajax for Appreciation Data
function getAppreciationData(){
  	$.ajax({
          url: "clientAppreciation",
          cache: true,
          beforeSend: function (request) {
              var token = $('meta[name="token"]').attr("content");
              request.setRequestHeader("Authorization", "JWT " + token);
          }
        }).done(function(data) {
            appData = data;
        });
  }

//load Appreciation Data
  $("#appreciation").click(function(){
	  buttonDisable("one","two");
	  showData("csat_div", "cTable");
            loadData("cTable");
        	$('#cTable').html(appData);
  });
  
//Ajax for Complaint Data
function getComplaintData(){
	  	$.ajax({
      url: "clientComplaint",
      cache: true,
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      }
    }).done(function(data) {
        complaintData = data;
    });
}
//load Complaint Data
$("#complaint").click(function(){
		buttonDisable("one","two");
  	    showData("csat_div", "cTable");
        loadData("cTable");
    	$('#cTable').html(complaintData);
}); 
//Ajax to get Aspect Data
function getAspectData(){
$.ajax({
      url: "cSFAControlMethod",
      cache: true,
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      }
    }).done(function(data) {  	
    	console.log(data);
    	console.log("hi how are you");
        aspectData = data;
    });
}
//load Aspect Data
$("#aspect").click(function(){
  buttonDisable("one","two");
  showData("csat_div", "cTable");
  loadData("cTable");
  $('#cTable').html(aspectData);
}); 


//ajax for Risk Data
function getRiskData(){
  	  $.ajax({
            url: "deliveryRiskControl",
            cache: true,
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            }
          }).done(function(data) {
          	rgraph =  $($.parseHTML(data)).filter("#graphData"); 
          	rtable =  $($.parseHTML(data)).filter("#tableData");
          	/*google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
          	$("#riskTable").html(rtable);   */
          });   
    }
$("#risks").click(function(){
	buttonAble("five","six");
	google.charts.setOnLoadCallback(drawRiskRatingGraph(rgraph.text()));
    $("#riskTable").html(rtable);
})
//ajax for Velocity Chart
function getVelocityData(){
  	  $.ajax({
            url: "velocityProjectList",
            cache: true,
            beforeSend: function (request) {
                var token = $('meta[name="token"]').attr("content");
                request.setRequestHeader("Authorization", "JWT " + token);
            }
          }).done(function(data) {
        	  	velocityChart = data;
          });   
    }
$("#velocityChart"). click(function(){
	buttonDisable("five","six");
	showData('riskrating_div', 'riskTable');
    $("#riskTable").html(velocityChart);
})

//Ajax for DRE Data
function getMetricsData(){
    $.ajax({
        url: "monthlyMetricsData",
        cache: true,
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }
      }).done(function(data) {
        loadData("graph");
        DREdata =  $($.parseHTML(data)).filter("#DRE"); 
        defectsData = $($.parseHTML(data)).filter("#defectsBar");
        reviewData = $($.parseHTML(data)).filter("#LineEff");
        SRFData = $($.parseHTML(data)).filter("#SRF");
        DRtable = $($.parseHTML(data)).filter(".zui-wrapper");
        defectsTable =  $($.parseHTML(data)).filter("#tableOne");
        buttonData = $($.parseHTML(data)).filter("#buttonData");
        $("#buttonData").html(buttonData);
        buttonAble("three","four");
 	   $("#buttonData").show();
 	   $("#dtable").html(defectsTable);
 	google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));
        
      });
}
//get requirement fulfillment data
function getRequirementsData(){
    $.ajax({
        url: "iReport",
        cache: true,
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }
      }).done(function(data) {
        requirementData = data;
        buttonDisable("five","six");
        showData('riskrating_div', 'riskTable');    
        $("#riskTable").html(requirementData);
      });
}
//load requirement fulfillment data
$("#reqFulfillment").click(function(){
	buttonDisable("five","six");
    showData('riskrating_div', 'riskTable');    
    $("#riskTable").html(requirementData);
    });


//load DRE Data
$("#dre").click(function(){
	buttonAble("three","four");
	$("#buttonData").hide();
	$("#dtable").html(DRtable);
	google.charts.setOnLoadCallback(drawDREChart(DREdata.text()));
	});
//load Defect Distribution Data
$("#defects").click(function(){
	   buttonAble("three","four");
	   $("#buttonData").show();
	   $("#dtable").html(defectsTable);
	google.charts.setOnLoadCallback(drawBarGraph(defectsData.text()));
	});
//load Review Efficiency Data
	$("#rEff").click(function(){
	buttonAble("three","four");
	$("#buttonData").hide();
	$("#dtable").html(DRtable);
		/*var height = $("#tblDre tr th:nth-child(2)").height();
		
		var maxHeight = Math.max.apply(null, $("#tblDre tr th").map(function ()
				{
				    return $(this).height();
				}).get());
		
		debugger;
		
		$("#tblDre tr th:nth-child(1)").css('min-height',maxHeight);*/
	google.charts.setOnLoadCallback(drawLineChart(reviewData.text())); 
	});
//Ajax for SRF Data
function getSRFData(){
	$.ajax({
	  url: "tests",
	  beforeSend: function (request) {
	      var token = $('meta[name="token"]').attr("content");
	      request.setRequestHeader("Authorization", "JWT " + token);
	  }
	}).done(function(data) {
	
	SRFGraph =  $($.parseHTML(data)).find("#graphData"); 
	 SRFTable =  $($.parseHTML(data)).find(".zui-wrapper");
    	
    });
}
//load Testing Data
$("#testing").click(function(){
	  buttonDisable("three","four");
	  $("#buttonData").hide();
		  showData('graph', 'dtable');
			$("#dtable").html(SRFTable);
	  });
//load SRF Data
   $("#srf").click(function(){
  buttonDisable("three","four");
  showGraphs('graph', 'dtable');
  $("#buttonData").hide();
		google.charts.setOnLoadCallback(drawSRFGraph(SRFGraph.text()));
  });
//Ajax for About the project Data
function getAtpData(){
    $.ajax({
        url: "aboutProjectInfo",
        cache: true,
        beforeSend: function (request) {
            var token = $('meta[name="token"]').attr("content");
            request.setRequestHeader("Authorization", "JWT " + token);
        }
      }).done(function(data) {
        loadData("processData");
        $("#processData").html(data); 
      });
}
//Ajax for CIDI 
function getCIDIData(){
  $.ajax({
      url: "cidiControl",
      cache: true,
      beforeSend: function (request) {
          var token = $('meta[name="token"]').attr("content");
          request.setRequestHeader("Authorization", "JWT " + token);
      }
    }).done(function(data) {
    	console.log(data);
    	cidiData = data;
    	loadData("processData");
        $('#processData').html(data);
       getSRFData();
  	   getAppreciationData();
  	   getComplaintData();
  	   getAspectData();
  	   getVelocityData();
  	   getRiskData();
    });
}
//load CIDI data
$("#cidi").click(function(){
	
	$('#processData').html(data);
})
//code for loader
var img = document.createElement("IMG");
img.src = "images/712.GIF";
img.height = "70";
img.width = "70";
img.style.margin ="50px 0px 0px 0px";

function loadGif(){ 
	img.height = "70";
	img.width = "70";
	$("#csat_div, #riskrating_div,#graph,#processData").html(img);
}
//Code to reset div tag
function loadData(a){
document.getElementById(a).innerHTML = "";
}
