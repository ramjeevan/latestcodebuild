	
function isProjectSelected(){
     			var val = document.getElementById("projectname").value;
     			if(val == ""){
     				return false;
     			}
     			else{
     				return true;
     			}
     		}

function isSurveyPeriodSelected(){
	var val = document.getElementById("quarter").value;
	if(val == ""){
		return false;
	}else{
		return true;
	}
	
}
function isDateSelected(val){
	if(val == ""){
		return false;
	}else{
		return true;
	}
	
}

function validationForTheProjectList(){
	var projectSelectedValue = isProjectSelected();
	var error = document.getElementById("error");
	if(projectSelectedValue == false){
		error.innerHTML = "Please select a project!";
		return false;
	}
	else{
		error.innerHTML = "";
		$(".container").hide();
		$(".loading-dots").show();
		return true;
	}
}


function validationForLandingPage(){
	var projectSelectedValue = isProjectSelected();
	var error = document.getElementById("error");
	if(projectSelectedValue == false){
		error.innerHTML = "Please select a project!";
		return false;
	}
	else{
		error.innerHTML = "";
		$(".loading-dots").show();
		$(".container").hide();
	   return true;
	}
}

function validationForContinue(){
	console.log("continue");
	/*document.getElementById("loading-dots").show();
*/	$(".loading-dots").show();
	$(".container").hide();
	return true;
	
}



function validationForClientSatisfaction(){
	var periodSelectedValue = isSurveyPeriodSelected();
	 if(periodSelectedValue == false){
		error.innerHTML = "Please select a survey period!";
	return false;
	}else{
		error.innerHTML = " ";
		$(".container").hide();
		$(".loading-dots").show();
		return true;
	}
}


function validationForCummulativeSelection(){
	var sDate = document.getElementById("startdate").value;
	var eDate = document.getElementById("enddate").value;
	var stdatevalue = isDateSelected(sDate);
	var eddatevalue = isDateSelected(eDate);
	 if(stdatevalue == false && eddatevalue == false){
		error.innerHTML = "Please select start date and end date!";
		return false;
	}else if(stdatevalue == true && eddatevalue == false){
		error.innerHTML = "Please select end date!";
		return false;
	}else if(stdatevalue == false && eddatevalue == true){
		error.innerHTML = "Please select start date!";
		return false;
	}else if(stdatevalue == true && eddatevalue == true){
		var d1 = new Date(sDate);
		var d2 = new Date(eDate);
		if(d1 >= d2){
			error.innerHTML = "From date should be less than start date!";
			return false;
		}
		else{
			error.innerHTML = "";
			return true;
		}
	}
	
}
