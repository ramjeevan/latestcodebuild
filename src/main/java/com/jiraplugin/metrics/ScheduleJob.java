package com.jiraplugin.metrics;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.jiraplugin.metrics.service.ProjectService;

@Component
public class ScheduleJob {

	@Autowired
	private ProjectService projectObj;


	@Scheduled(fixedRate = 72000)
	public void scheduleCall() {


		projectObj.FiledCutFlag = 0;

	}

}
