package com.jiraplugin.metrics.model;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

public class AuditInfo {
	String id, area, auditors, auditees, auditDate, preparedBy,goodPoints;
	public String getGoodPoints() {
		return goodPoints;
	}
	public void setGoodPoints(String goodPoints) {
		this.goodPoints = goodPoints;
	}
	ArrayList<NcData> nc = new ArrayList<NcData>();
	ArrayList<IoData> io = new ArrayList<IoData>();
	NcData ncd=new NcData();
	IoData iod=new IoData();
	public ArrayList<IoData> getIo() {
		return io;
	}
	public void setIo(ArrayList<IoData> iodt) {
		this.io = iodt;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getAuditors() {
		return auditors;
	}
	public void setAuditors(String auditors) {
		this.auditors = auditors;
	}
	public String getAuditees() {
		return auditees;
	}
	public void setAuditees(String auditees) {
		this.auditees = auditees;
	}
	public String getAuditDate() {
		return auditDate;
	}
	public void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}
	public String getPreparedBy() {
		return preparedBy;
	}
	public void setPreparedBy(String preparedBy) {
		this.preparedBy = preparedBy;
	}
	public ArrayList<NcData> getNc() {
		return nc;
	}
	public void setNc(ArrayList<NcData> nc) {
		this.nc = nc;
	}
	
}
