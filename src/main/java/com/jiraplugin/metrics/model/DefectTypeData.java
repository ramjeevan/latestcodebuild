package com.jiraplugin.metrics.model;

public class DefectTypeData {
	int hCount, lCount, mCount, tCount;

	public int gethCount() {
		return hCount;
	}

	public void sethCount(int hCount) {
		this.hCount = hCount;
	}

	public int getlCount() {
		return lCount;
	}

	public void setlCount(int lCount) {
		this.lCount = lCount;
	}

	public int getmCount() {
		return mCount;
	}

	public void setmCount(int mCount) {
		this.mCount = mCount;
	}

	public int gettCount() {
		return tCount;
	}

	public void settCount(int tCount) {
		this.tCount = tCount;
	}
	
}
