package com.jiraplugin.metrics.model;

import java.time.LocalDate;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

public class SelectChoice {

	@NotNull
	public String projectname;

	public String atlassianConnectToken;

	
	public String quarter;

	public String getAtlassianConnectToken() {
		return atlassianConnectToken;
	}

	public void setAtlassianConnectToken(String atlassianConnectToken) {
		this.atlassianConnectToken = atlassianConnectToken;
	}

	public String getProjectname() {
		return projectname;
	}

	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}
	
	public Date selectedDate;
	public Date getSelectedDate() {
		return selectedDate;
	}

	public void setSelectedDate(Date selectedDate) {
		this.selectedDate = selectedDate;
	}

	public Date  startdate;

	public Date enddate;
	public String getCycle() {
		return cycle;
	}
	public void setCycle(String cycle) {
		this.cycle = cycle;
	}
	public String cycle;
	 
public String audityear;
public String getAudityear() {
	return audityear;
}
public void setAudityear(String audityear) {
	this.audityear = audityear;
}
public String catId;

public String getCatId() {
	return catId;
}

public void setCatId(String catId) {
	this.catId = catId;
}

}