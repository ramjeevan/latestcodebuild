package com.jiraplugin.metrics.model;
import java.util.ArrayList;
import java.util.List;

public class MetricsList{
	
	List<Integer> rdlist = new ArrayList<Integer>();
	public List<Integer> getRdlist() {
		return rdlist;
	}
	public void setRdlist(List<Integer> rdlist2) {
		this.rdlist = rdlist2;
	}
	public List<Integer> getTdlist() {
		return tdlist;
	}
	public void setTdlist(List<Integer> tdlist) {
		this.tdlist = tdlist;
	}
	public List<Integer> getDdlist() {
		return ddlist;
	}
	public void setDdlist(List<Integer> ddlist) {
		this.ddlist = ddlist;
	}
	public List<Integer> getIdlist() {
		return idlist;
	}
	public void setIdlist(List<Integer> idlist) {
		this.idlist = idlist;
	}
	public ArrayList<Double> getDrelist() {
		return drelist;
	}
	public void setDrelist(ArrayList<Double> drelist) {
		this.drelist = drelist;
	}
	public ArrayList<Double> getReflist() {
		return reflist;
	}
	public void setReflist(ArrayList<Double> reflist) {
		this.reflist = reflist;
	}
	public ArrayList<Double> getTeflist() {
		return teflist;
	}
	public void setTeflist(ArrayList<Double> teflist) {
		this.teflist = teflist;
	}
	List<Integer> tdlist = new ArrayList<Integer>();
	List<Integer> ddlist = new ArrayList<Integer>();
	List<Integer> idlist = new ArrayList<Integer>();
	ArrayList<Double> drelist = new ArrayList<Double>();
	ArrayList<Double> reflist = new ArrayList<Double>();
	ArrayList<Double> teflist = new ArrayList<Double>();
}