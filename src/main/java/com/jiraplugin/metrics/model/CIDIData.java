package com.jiraplugin.metrics.model;

import java.util.ArrayList;

import com.jiraplugin.metrics.model.ClientSatisfactionFA;

public class CIDIData {

	public ArrayList<ClientSatisfactionFA> getcCData() {
		return cCData;
	}

	public void setcCData(ArrayList<ClientSatisfactionFA> cCData) {
		this.cCData = cCData;
	}

	public ArrayList<ClientSatisfactionFA> getcSFAData() {
		return cSFAData;
	}

	public void setcSFAData(ArrayList<ClientSatisfactionFA> cSFAData) {
		this.cSFAData = cSFAData;
	}

	public ArrayList<ClientSatisfactionFA> getpDPIData() {
		return pDPIData;
	}

	public void setpDPIData(ArrayList<ClientSatisfactionFA> pDPIData) {
		this.pDPIData = pDPIData;
	}

	public ArrayList<ClientSatisfactionFA> getpCIData() {
		return pCIData;
	}

	public void setpCIData(ArrayList<ClientSatisfactionFA> pCIData) {
		this.pCIData = pCIData;
	}

	public ArrayList<ClientSatisfactionFA> getiSWData() {
		return iSWData;
	}

	public void setiSWData(ArrayList<ClientSatisfactionFA> iSWData) {
		this.iSWData = iSWData;
	}

	public ArrayList<ClientSatisfactionFA> getiSEData() {
		return iSEData;
	}

	public void setiSEData(ArrayList<ClientSatisfactionFA> iSEData) {
		this.iSEData = iSEData;
	}

	public ArrayList<ClientSatisfactionFA> getrEData() {
		return rEData;
	}

	public void setrEData(ArrayList<ClientSatisfactionFA> rEData) {
		this.rEData = rEData;
	}

	public ArrayList<ClientSatisfactionFA> getdIData() {
		return dIData;
	}

	public void setdIData(ArrayList<ClientSatisfactionFA> dIData) {
		this.dIData = dIData;
	}

	public ArrayList<ClientSatisfactionFA> getcIData() {
		return cIData;
	}

	public void setcIData(ArrayList<ClientSatisfactionFA> cIData) {
		this.cIData = cIData;
	}

	ArrayList<ClientSatisfactionFA> cCData;
	ArrayList<ClientSatisfactionFA> cSFAData;
	ArrayList<ClientSatisfactionFA> pDPIData;
	ArrayList<ClientSatisfactionFA> pCIData;
	ArrayList<ClientSatisfactionFA> iSWData;

	ArrayList<ClientSatisfactionFA> iSEData;
	ArrayList<ClientSatisfactionFA> rEData;
	ArrayList<ClientSatisfactionFA> dIData;
	ArrayList<ClientSatisfactionFA> cIData;

}
