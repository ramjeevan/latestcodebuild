package com.jiraplugin.metrics.model;

public class Metrics{
	
	public int getReview_Efficiency() {
		return Review_Efficiency;
	}
	public void setReview_Efficiency(int review_Efficiency) {
		Review_Efficiency = review_Efficiency;
	}
	public float getReview_Effectiveness() {
		return Review_Effectiveness;
	}
	public void setReview_Effectiveness(float review_Effectiveness) {
		Review_Effectiveness = review_Effectiveness;
	}
	public float getDRE() {
		return DRE;
	}
	public void setDRE(float dRE) {
		DRE = dRE;
	}
	/*public float getCDRE() {
		return CDRE;
	}
	public void setCDRE(float cDRE) {
		CDRE = cDRE;
	}*/
	public float getTesting_Effectiveness() {
		return Testing_Effectiveness;
	}
	public void setTesting_Effectiveness(float testing_Effectiveness) {
		Testing_Effectiveness = testing_Effectiveness;
	}
	public int getDelivered_Defects() {
		return Delivered_Defects;
	}
	public void setDelivered_Defects(int delivered_Defects) {
		Delivered_Defects = delivered_Defects;
	}
	public float Review_Effectiveness;
	public float DRE;
	//public float CDRE;
	public float Testing_Effectiveness;
	public int Delivered_Defects;
	public int Review_Efficiency;
	public int InprocessDefects;
	public int Review_Defects;
	public int Testing_Defects;
	
	public int getTesting_Defects() {
		return Testing_Defects;
	}

	
	public int getReview_Defects() {
		return Review_Defects;
	}
	
	public int getInprocessDefects() {
		return InprocessDefects;
	}
	public void setInprocessDefects(int inprocessDefects) {
		InprocessDefects = inprocessDefects;
	}
	/*public int getReview_Defects(SelectChoiceEntity sce) {
		// TODO Auto-generated method stub
		return Review_Defects;
	}
	public int getTesting_Defects(SelectChoiceEntity sce) {
		// TODO Auto-generated method stub
		return Testing_Defects;
	}
	public int getInprocessDefects(Metrics m, SelectChoiceEntity sce) {
		// TODO Auto-generated method stub
		return InprocessDefects;
		}*/
	
	public void setReview_Defects(int review_Defects) {
		Review_Defects = review_Defects;
	}
	public void setTesting_Defects(int testing_Defects) {
		Testing_Defects = testing_Defects;
	
}
	/*public int getDelivered_Defects(SelectChoiceEntity sce) {
		// TODO Auto-generated method stub
		return  Delivered_Defects;
	} */
}