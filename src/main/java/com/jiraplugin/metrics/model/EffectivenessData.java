package com.jiraplugin.metrics.model;

public class EffectivenessData {
	String month;
	int sCount, tCount,iCount,crCount,pisCount,nrCount;
	public int getiCount() {
		return iCount;
	}
	public void setiCount(int iCount) {
		this.iCount = iCount;
	}
	public int getCrCount() {
		return crCount;
	}
	public void setCrCount(int crCount) {
		this.crCount = crCount;
	}
	public int getPisCount() {
		return pisCount;
	}
	public void setPisCount(int pisCount) {
		this.pisCount = pisCount;
	}
	public int getNrCount() {
		return nrCount;
	}
	public void setNrCount(int nrCount) {
		this.nrCount = nrCount;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getsCount() {
		return sCount;
	}
	public void setsCount(int sCount) {
		this.sCount = sCount;
	}
	public int gettCount() {
		return tCount;
	}
	public void settCount(int tCount) {
		this.tCount = tCount;
	}
}
