package com.jiraplugin.metrics.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProjectsData implements Serializable {
	// private String projectTypeKey;

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	@JsonProperty("name")
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	   @JsonProperty("key")
	    private String key;



		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}
}