package com.jiraplugin.metrics.model;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jiraplugin.metrics.service.JiraCustomFieldMap;

public class MetricsData {
            @JsonProperty("summary")
    public String summary; 
            DEffectivenessData d;
            public DEffectivenessData getD() {
				return d;
			}
			public void setD(DEffectivenessData d) {
				this.d = d;
			}
			int total_test_design_defects;
            public int getTotal_test_design_defects() {
                return total_test_design_defects;
            }
            public void setTotal_test_design_defects(int total_test_design_defects) {
                this.total_test_design_defects = total_test_design_defects;
            }
            double total_testable;
            public double getTotal_testable() {
                return total_testable;
            }
            public void setTotal_testable(double total_testable) {
                this.total_testable = total_testable;
            }
            int revd;
            public int getRevd() {
                        return revd;
            }
            public void setRevd(int revd) {
                        this.revd = revd;
            }
            public int getTesd() {
                        return tesd;
            }
            public void setTesd(int tesd) {
                        this.tesd = tesd;
            }
            public int getDeld() {
                        return deld;
            }
            public void setDeld(int deld) {
                        this.deld = deld;
            }
            public int getInpd() {
                        return inpd;
            }
            public void setInpd(int inpd) {
                        this.inpd = inpd;
            }
            public String getRevef() {
                        return revef;
            }
            public void setRevef(String revef) {
                        this.revef = revef;
            }
            public String getTesteff() {
                        return testeff;
            }
            public void setTesteff(String testeff) {
                        this.testeff = testeff;
            }
            public String getDreff() {
                        return dreff;
            }
            public void setDreff(String dreff) {
                        this.dreff = dreff;
            }
            int tesd;
            int deld;
            int inpd;
            int total_review_defects;
            int total_inprocess_defects;
            public int getTotal_inprocess_defects() {
                        return total_inprocess_defects;
            }
            public void setTotal_inprocess_defects(int total_inprocess_defects) {
                        this.total_inprocess_defects = total_inprocess_defects;
            }
            public int getTotal_review_defects() {
                        return total_review_defects;
            }
            public void setTotal_review_defects(int total_review_defects) {
                        this.total_review_defects = total_review_defects;
            }
            String revef;
            String testeff;
            String dreff;
            String review_effort;
            double test_design_review_effort;
            public double getTest_design_review_effort() {
                        return test_design_review_effort;
            }
            public void setTest_design_review_effort(double test_design_review_effort) {
                        this.test_design_review_effort = test_design_review_effort;
            }
            public String getReview_effort() {
                        return review_effort;
            }
            public void setReview_effort(String review_effort) {
                        this.review_effort = review_effort;
            }
            public String getSummary() {
                        return summary;
            }
            public void setSummary(String summary) {
                        this.summary = summary;
            }
            public String getMonth() {
                        return month;
            }
            public void setMonth(String month) {
                        this.month = month;
            }
            public String month;
            public String review_efficiency;
            public String getReview_efficiency() {
                        return review_efficiency;
            }
            public void setReview_efficiency(String review_efficiency) {
                        this.review_efficiency = review_efficiency;
            }
            String total_review_effort;
            public String getTotal_review_efficiency() {
	        return total_review_efficiency;
	    }
	    public void setTotal_review_efficiency(String total_review_efficiency) {
	        this.total_review_efficiency = total_review_efficiency;
	    }
	    public String total_review_efficiency;
            public String getTotal_review_effort() {
                        return total_review_effort;
            }
            public void setTotal_review_effort(String total_review_effort) {
                        this.total_review_effort = total_review_effort;
            }           
            public String review_effectiveness_adc;
            public String getReview_effectiveness_adc() {
                        return review_effectiveness_adc;
            }
            public void setReview_effectiveness_adc(String review_effectiveness_adc) {
                        this.review_effectiveness_adc = review_effectiveness_adc;
            }
}
