package com.jiraplugin.metrics.model;

import java.util.ArrayList;
import java.util.Map;

public class RiskManagementData {

	public Map<String, String> getCriticalRiskCount() {
		return criticalRiskCount;
	}

	public void setCriticalRiskCount(Map<String, String> criticalRiskCount) {
		this.criticalRiskCount = criticalRiskCount;
	}

	public Map<String, String> getRiskAcceptablePercentage() {
		return riskAcceptablePercentage;
	}

	public void setRiskAcceptablePercentage(Map<String, String> riskAcceptablePercentage) {
		this.riskAcceptablePercentage = riskAcceptablePercentage;
	}

	public Map<String, String> getRiskOccurenceCount() {
		return riskOccurenceCount;
	}

	public void setRiskOccurenceCount(Map<String, String> riskOccurenceCount) {
		this.riskOccurenceCount = riskOccurenceCount;
	}

	public ArrayList<ResultRangeData> getExposureValues() {
		return exposureValues;
	}

	public void setExposureValues(ArrayList<ResultRangeData> exposureValues) {
		this.exposureValues = exposureValues;
	}

	Map<String, String> criticalRiskCount;
	Map<String, String> riskAcceptablePercentage;
	Map<String, String> riskOccurenceCount;
	ArrayList<ResultRangeData> exposureValues;

}
