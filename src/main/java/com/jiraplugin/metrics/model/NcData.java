package com.jiraplugin.metrics.model;

public class NcData {
	int sno;
	String description,process,processRequirement,severity,evidence,cause,correction,correctionResponsibility,correctionPlannedDate,correctionActualDate,correctiveAction,actionPlannedDate,actionActualDate,actionResponsibility,verificaitonDetails,verifiedBy,verifiedDate,closureRemark;

	public String getActionActualDate() {
		return actionActualDate;
	}
	public void setActionActualDate(String actionActualDate) {
		this.actionActualDate = actionActualDate;
	}
	public String getCorrectiveAction() {
		return correctiveAction;
	}
	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}
	public String getCause() {
		return cause;
	}
	public void setCause(String cause) {
		this.cause = cause;
	}
	public String getProcessRequirement() {
		return processRequirement;
	}
	public void setProcessRequirement(String processRequirement) {
		this.processRequirement = processRequirement;
	}
	public int getSno() {
		return sno;
	}
	public void setSno(int sno) {
		this.sno = sno;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}
	public String getEvidence() {
		return evidence;
	}
	public void setEvidence(String evidence) {
		this.evidence = evidence;
	}
	public String getCorrection() {
		return correction;
	}
	public void setCorrection(String correction) {
		this.correction = correction;
	}
	public String getCorrectionResponsibility() {
		return correctionResponsibility;
	}
	public void setCorrectionResponsibility(String correctionResponsibility) {
		this.correctionResponsibility = correctionResponsibility;
	}
	public String getCorrectionPlannedDate() {
		return correctionPlannedDate;
	}
	public void setCorrectionPlannedDate(String correctionPlannedDate) {
		this.correctionPlannedDate = correctionPlannedDate;
	}
	public String getCorrectionActualDate() {
		return correctionActualDate;
	}
	public void setCorrectionActualDate(String correctionActualDate) {
		this.correctionActualDate = correctionActualDate;
	}
	public String getActionPlannedDate() {
		return actionPlannedDate;
	}
	public void setActionPlannedDate(String actionPlannedDate) {
		this.actionPlannedDate = actionPlannedDate;
	}
	
	public String getActionResponsibility() {
		return actionResponsibility;
	}
	public void setActionResponsibility(String actionResponsibility) {
		this.actionResponsibility = actionResponsibility;
	}
	public String getVerificaitonDetails() {
		return verificaitonDetails;
	}
	public void setVerificaitonDetails(String verificaitonDetails) {
		this.verificaitonDetails = verificaitonDetails;
	}
	public String getVerifiedBy() {
		return verifiedBy;
	}
	public void setVerifiedBy(String verifiedBy) {
		this.verifiedBy = verifiedBy;
	}
	public String getVerifiedDate() {
		return verifiedDate;
	}
	public void setVerifiedDate(String verifiedDate) {
		this.verifiedDate = verifiedDate;
	}
	public String getClosureRemark() {
		return closureRemark;
	}
	public void setClosureRemark(String closureRemark) {
		this.closureRemark = closureRemark;
	}
	

}
