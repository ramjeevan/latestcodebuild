// corrective/improvement action
package com.jiraplugin.metrics.model;


public class CreativeIdea{
	public String date;
	public String getDate() {
		return date;
	}
	public void setDate(String repdt) {
		this.date = repdt;
	}
	public String status;
	public String getStatus() {
	    return status;
	}
	public void setStatus(String status) {
	    this.status = status;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getRoot_cause() {
		return root_cause;
	}
	public void setRoot_cause(String root_cause) {
		this.root_cause = root_cause;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	public String getTarget_date() {
		return target_date;
	}
	public void setTarget_date(String target_date) {
		this.target_date = target_date;
	}
	public String getActual_date() {
		return actual_date;
	}
	public void setActual_date(String actual_date) {
		this.actual_date = actual_date;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String Category;
    public String Description;
    public String priority;
    public String root_cause;
    public String action;
    public String getAction_type() {
		return action_type;
	}
	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}
	public String action_type;
  /* public ArrayList<String> getResponsibility() {
		return responsibility;
	}
	public void setResponsibility(ArrayList<String> responsibility) {
		this.responsibility = responsibility;
	}
ArrayList<String> responsibility=new ArrayList<String>();*/
	public String responsibility;
    public String getResponsibility() {
		return responsibility;
	}
	public void setResponsibility(String responsibility) {
		this.responsibility = responsibility;
	}
	public String target_date;
    public String actual_date;
    public String remarks;
    public String getDefect_type() {
		return defect_type;
	}
	public void setDefect_type(String defect_type) {
		this.defect_type = defect_type;
	}
	public String defect_type;
}