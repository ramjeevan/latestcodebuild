package com.jiraplugin.metrics.model;

public class ResultRangeData {

	
	public String month, avg_risk_value, avg_residual_value;
	public int avg_risk,avg_residual;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getAvg_risk() {
		return avg_risk;
	}
	public void setAvg_risk(int avg_risk) {
		this.avg_risk = avg_risk;
	}
	public String getAvg_risk_value() {
		return avg_risk_value;
	}
	public void setAvg_risk_value(String avg_risk_value) {
		this.avg_risk_value = avg_risk_value;
	}
	public int getAvg_residual() {
		return avg_residual;
	}
	public void setAvg_residual(int avg_residual) {
		this.avg_residual = avg_residual;
	}
	public String getAvg_residual_value() {
		return avg_residual_value;
	}
	public void setAvg_residual_value(String avg_residual_value) {
		this.avg_residual_value = avg_residual_value;
	}
	
	
	
	
}
