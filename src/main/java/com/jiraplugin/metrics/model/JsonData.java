//This class is a model for Json of all the metrics
package com.jiraplugin.metrics.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JsonData implements Serializable
{
  


   // private String id;
	@JsonProperty("total")
    private int total;

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}



 


}