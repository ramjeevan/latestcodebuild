package com.jiraplugin.metrics.model;

public class CumulativeData {
	int rHCount,rMCount,rLCount,rTCount, tHCount,tMCount,tLCount,tTCount,iHCount,iMCount,iLCount,iTCount,dHCount,dMCount,dLCount,dTCount;

	public int getrHCount() {
		return rHCount;
	}

	public void setrHCount(int rHCount) {
		this.rHCount = rHCount;
	}

	public int getrMCount() {
		return rMCount;
	}

	public void setrMCount(int rMCount) {
		this.rMCount = rMCount;
	}

	public int getrLCount() {
		return rLCount;
	}

	public void setrLCount(int rLCount) {
		this.rLCount = rLCount;
	}

	public int getrTCount() {
		return rTCount;
	}

	public void setrTCount(int rTCount) {
		this.rTCount = rTCount;
	}

	public int gettHCount() {
		return tHCount;
	}

	public void settHCount(int tHCount) {
		this.tHCount = tHCount;
	}

	public int gettMCount() {
		return tMCount;
	}

	public void settMCount(int tMCount) {
		this.tMCount = tMCount;
	}

	public int gettLCount() {
		return tLCount;
	}

	public void settLCount(int tLCount) {
		this.tLCount = tLCount;
	}

	public int gettTCount() {
		return tTCount;
	}

	public void settTCount(int tTCount) {
		this.tTCount = tTCount;
	}

	public int getiHCount() {
		return iHCount;
	}

	public void setiHCount(int iHCount) {
		this.iHCount = iHCount;
	}

	public int getiMCount() {
		return iMCount;
	}

	public void setiMCount(int iMCount) {
		this.iMCount = iMCount;
	}

	public int getiLCount() {
		return iLCount;
	}

	public void setiLCount(int iLCount) {
		this.iLCount = iLCount;
	}

	public int getiTCount() {
		return iTCount;
	}

	public void setiTCount(int iTCount) {
		this.iTCount = iTCount;
	}

	public int getdHCount() {
		return dHCount;
	}

	public void setdHCount(int dHCount) {
		this.dHCount = dHCount;
	}

	public int getdMCount() {
		return dMCount;
	}

	public void setdMCount(int dMCount) {
		this.dMCount = dMCount;
	}

	public int getdLCount() {
		return dLCount;
	}

	public void setdLCount(int dLCount) {
		this.dLCount = dLCount;
	}

	public int getdTCount() {
		return dTCount;
	}

	public void setdTCount(int dTCount) {
		this.dTCount = dTCount;
	}

}
