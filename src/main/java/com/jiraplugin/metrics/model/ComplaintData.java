package com.jiraplugin.metrics.model;

public class ComplaintData {
 String rDate, description,action,rFrom, status;

public String getrDate() {
	return rDate;
}

public void setrDate(String rDate) {
	this.rDate = rDate;
}

public String getDescription() {
	return description;
}

public void setDescription(String description) {
	this.description = description;
}

public String getAction() {
	return action;
}

public void setAction(String action) {
	this.action = action;
}

public String getrFrom() {
	return rFrom;
}

public void setrFrom(String rFrom) {
	this.rFrom = rFrom;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}
 
}
