package com.jiraplugin.metrics.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RiskRating {
	
	@JsonProperty("fields")
	ArrayList<RiskRangeData> fields = new ArrayList<RiskRangeData>();

	public ArrayList<RiskRangeData> getFields() {
		return fields;
	}

	public void setFields(ArrayList<RiskRangeData> fields) {
		this.fields = fields;
	}
	
}

