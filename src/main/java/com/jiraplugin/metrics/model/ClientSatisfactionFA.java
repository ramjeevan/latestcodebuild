package com.jiraplugin.metrics.model;

public class ClientSatisfactionFA {

	String Reported_Date;

	public String getReported_Date() {
		return Reported_Date;
	}

	public void setReported_Date(String reported_Date) {
		Reported_Date = reported_Date;
	}

	public String getCategory() {
		return Category;
	}

	public void setCategory(String category) {
		Category = category;
	}

	public String getIssue_Description() {
		return Issue_Description;
	}

	public void setIssue_Description(String issue_Description) {
		Issue_Description = issue_Description;
	}

	public String getReceived_From() {
		return Received_From;
	}

	public void setReceived_From(String received_From) {
		Received_From = received_From;
	}

	public String getSeverity() {
		return Severity;
	}

	public void setSeverity(String severity) {
		Severity = severity;
	}

	public String getRoot_Cause() {
		return Root_Cause;
	}

	public void setRoot_Cause(String root_Cause) {
		Root_Cause = root_Cause;
	}

	public String getAction_Type() {
		return Action_Type;
	}

	public void setAction_Type(String action_Type) {
		Action_Type = action_Type;
	}

	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}

	public String getResponsibility() {
		return Responsibility;
	}

	public void setResponsibility(String responsibility) {
		Responsibility = responsibility;
	}

	public String getActual_Date() {
		return Actual_Date;
	}

	public void setActual_Date(String actual_Date) {
		Actual_Date = actual_Date;
	}

	public String getTarget_Date() {
		return Target_Date;
	}

	public void setTarget_Date(String target_Date) {
		Target_Date = target_Date;
	}

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	String Category;
	String Issue_Description;
	String Received_From;
	String Severity;
	String Defect_Type;

	public String getDefect_Type() {
		return Defect_Type;
	}

	public void setDefect_Type(String defect_Type) {
		Defect_Type = defect_Type;
	}

	String Root_Cause;
	String Action_Type;
	String Action;
	String Responsibility;
	String Actual_Date;
	String Target_Date;
	String Remarks;
}
