package com.jiraplugin.metrics.model;

public class ClientSatisfactionData {

	public String getClient_Name() {
		return Client_Name;
	}

	public void setClient_Name(String client_Name) {
		Client_Name = client_Name;
	}

	public String getSent_Date() {
		return Sent_Date;
	}

	public void setSent_Date(String sent_Date) {
		Sent_Date = sent_Date;
	}

	public String getReceived_Date() {
		return Received_Date;
	}

	public void setReceived_Date(String received_Date) {
		Received_Date = received_Date;
	}

	public String getQuarter_Start_Date() {
		return Quarter_Start_Date;
	}

	public void setQuarter_Start_Date(String quarter_Start_Date) {
		Quarter_Start_Date = quarter_Start_Date;
	}

	public String getCSAT_Q1() {
		return CSAT_Q1;
	}

	public void setCSAT_Q1(String cSAT_Q1) {
		CSAT_Q1 = cSAT_Q1;
	}

	public String getCSAT_Q2() {
		return CSAT_Q2;
	}

	public void setCSAT_Q2(String cSAT_Q2) {
		CSAT_Q2 = cSAT_Q2;
	}

	public String getCSAT_Q3() {
		return CSAT_Q3;
	}

	public void setCSAT_Q3(String cSAT_Q3) {
		CSAT_Q3 = cSAT_Q3;
	}

	public String getCSAT_Q4() {
		return CSAT_Q4;
	}

	public void setCSAT_Q4(String cSAT_Q4) {
		CSAT_Q4 = cSAT_Q4;
	}

	public String getCSAT_Q5() {
		return CSAT_Q5;
	}

	public void setCSAT_Q5(String cSAT_Q5) {
		CSAT_Q5 = cSAT_Q5;
	}

	public String getCSAT_Q6() {
		return CSAT_Q6;
	}

	public void setCSAT_Q6(String cSAT_Q6) {
		CSAT_Q6 = cSAT_Q6;
	}

	public String getCSAT_Q7() {
		return CSAT_Q7;
	}

	public void setCSAT_Q7(String cSAT_Q7) {
		CSAT_Q7 = cSAT_Q7;
	}

	public String getCSAT_Q8() {
		return CSAT_Q8;
	}

	public void setCSAT_Q8(String cSAT_Q8) {
		CSAT_Q8 = cSAT_Q8;
	}

	String Client_Name;
	String Sent_Date;
	String Received_Date;
	String Quarter_Start_Date;
	String Year;
	public String getYear() {
		return Year;
	}

	public void setYear(String year) {
		Year = year;
	}

	String CSAT_Q1;
	String CSAT_Q2;
	String CSAT_Q3;
	String CSAT_Q4;
	String CSAT_Q5;
	String CSAT_Q6;
	String CSAT_Q7;
	String CSAT_Q8;

}
