package com.jiraplugin.metrics.model;
public class AnnualAudit {
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getExternalfrom() {
		return externalfrom;
	}
	public void setExternalfrom(String externalfrom) {
		this.externalfrom = externalfrom;
	}
	public String getExternalto() {
		return externalto;
	}
	public void setExternalto(String externalto) {
		this.externalto = externalto;
	}
	public String getCycle1from() {
		return cycle1from;
	}
	public void setCycle1from(String cycle1from) {
		this.cycle1from = cycle1from;
	}
	public String getCycle1to() {
		return cycle1to;
	}
	public void setCycle1to(String cycle1to) {
		this.cycle1to = cycle1to;
	}
	public String getCycle2from() {
		return cycle2from;
	}
	public void setCycle2from(String cycle2from) {
		this.cycle2from = cycle2from;
	}
	public String getCycle2to() {
		return cycle2to;
	}
	public void setCycle2to(String cycle2to) {
		this.cycle2to = cycle2to;
	}
	public String getCycle3from() {
		return cycle3from;
	}
	public void setCycle3from(String cycle3from) {
		this.cycle3from = cycle3from;
	}
	public String getCycle3to() {
		return cycle3to;
	}
	public void setCycle3to(String cycle3to) {
		this.cycle3to = cycle3to;
	}
	public String from;
	public String to;
	public String externalfrom;
	public String externalto;
	public String cycle1from;
	public String cycle1to;
	public String cycle2from;
	public String cycle2to;
	public String cycle3from;
	public String cycle3to;
	
}