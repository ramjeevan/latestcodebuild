package com.jiraplugin.metrics.model;

public class AppreciationData {
	String dReceived, rFrom, context, rFor, details;

	public String getdReceived() {
		return dReceived;
	}

	public void setdReceived(String dReceived) {
		this.dReceived = dReceived;
	}

	public String getrFrom() {
		return rFrom;
	}

	public void setrFrom(String rFrom) {
		this.rFrom = rFrom;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public String getrFor() {
		return rFor;
	}

	public void setrFor(String rFor) {
		this.rFor = rFor;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
	
}
