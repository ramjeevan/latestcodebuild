package com.jiraplugin.metrics.model;
 
public class TestCase{
	public int testdesign_planned;
	public int getTestdesign_planned() {
		return testdesign_planned;
	}
	public void setTestdesign_planned(int total_test_cases_planned_to_design) {
		this.testdesign_planned = total_test_cases_planned_to_design;
	}
	public int getTestdesign_developed() {
		return testdesign_developed;
	}
	public void setTestdesign_developed(int total_test_cases_designed) {
		this.testdesign_developed = total_test_cases_designed;
	}
	public int getTotaltestdefects() {
		return totaltestdefects;
	}
	public void setTotaltestdefects(int totaltestdefects) {
		this.totaltestdefects = totaltestdefects;
	}
	int test_cases_actually_executed;
	public int getTest_cases_actually_executed() {
	    return test_cases_actually_executed;
	}
	public void setTest_cases_actually_executed(int total_test_cases_actually_executed) {
	    this.test_cases_actually_executed = total_test_cases_actually_executed;
	}
	int total_testable_requirements;
	int test_cases_planned_for_execution;
	public int getTest_cases_planned_for_execution() {
	    return test_cases_planned_for_execution;
	}
	public void setTest_cases_planned_for_execution(int total_test_cases_planned_for_execution) {
	    this.test_cases_planned_for_execution = total_test_cases_planned_for_execution;
	}
	public int getTotal_testable_requirements() {
	    return total_testable_requirements;
	}
	public void setTotal_testable_requirements(int total_testable_requirements) {
	    this.total_testable_requirements = total_testable_requirements;
	}
	public int getTestcases_planned() {
		return testcases_planned;
	}
	public void setTestcases_planned(int testcases_planned) {
		this.testcases_planned = testcases_planned;
	}
	public int getTestcases_executed() {
		return testcases_executed;
	}
	public void setTestcases_executed(int testcases_executed) {
		this.testcases_executed = testcases_executed;
	}
	
	public int testdesign_developed;
	public int totaltestdefects;
	public String getTestdesignquality() {
		return testdesignquality;
	}
	public void setTestdesignquality(String testdesignquality) {
		this.testdesignquality = testdesignquality;
	}

	public String testdesignquality;
	public int testcases_planned;
	public int testcases_executed;
	public String testexecution;
	public String getTestexecution() {
		return testexecution;
	}
	public void setTestexecution(String testexecution) {
		this.testexecution = testexecution;
	}
	public String getTestcoverage() {
		return testcoverage;
	}
	public void setTestcoverage(String testcoverage) {
		this.testcoverage = testcoverage;
	}

	public String testcoverage;
	public String month;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public double test_cases_passed;
	public double getTest_cases_passed() {
		return test_cases_passed;
	}
	public void setTest_cases_passed(double total_test_cases_passed) {
		this.test_cases_passed = total_test_cases_passed;
	}
	public double getTest_cases_failed() {
		return test_cases_failed;
	}
	public void setTest_cases_failed(double total_test_cases_failed) {
		this.test_cases_failed = total_test_cases_failed;
	}

	public double test_cases_failed;
	public double test_design_review_effort;
	public double getTest_design_review_effort() {
	    return test_design_review_effort;
	}
	public void setTest_design_review_effort(double test_design_review_effort) {
	    this.test_design_review_effort = test_design_review_effort;
	}
	public int testable_requirements_executed;
	public int getTestable_requirements_executed() {
	    return testable_requirements_executed;
	}
	public void setTestable_requirements_executed(int testable_requirements_executed) {
	    this.testable_requirements_executed = testable_requirements_executed;
	}
	public double getFirst_time_right() {
	    return first_time_right;
	}
	public void setFirst_time_right(double first_time_right) {
	    this.first_time_right = first_time_right;
	}
	public double first_time_right;
          public String srf;
	public String getSrf() {
	    return srf;
	}
	public void setSrf(String srf) {
	    this.srf = srf;
	}	
}