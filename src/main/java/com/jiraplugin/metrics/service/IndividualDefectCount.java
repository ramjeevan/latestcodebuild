package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.util.concurrent.CompletableFuture;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class IndividualDefectCount {
	@Autowired
	private AtlassianHostRestClients restClients;
	@Async
	public CompletableFuture<Integer> getValues(String p,String name, int sDate, String type, @AuthenticationPrincipal AtlassianHostUser hostUser) throws JSONException, ParseException {
	    String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {name}) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({sDate})) AND 'Defect Severity'= {type}";
	    if(name =="Delivery Defect (NWF)") {
	    url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {name}) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({sDate})) AND 'Severity of the Issue'= {type}";
	    }else if(name.equals("Review Defect (NWF)")) {
		url = "/rest/api/2/search?jql=project= {p} AND  (issuetype = {name} OR issuetype = 'Analysis and Design Review Defect (NWF)' OR issuetype = 'Unit Testing Defect (NWF)' OR issuetype = 'Test Case Review Defect (NWF)') AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({sDate})) AND 'Defect Severity'= {type}";
	    }
	    String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\""+ name + "\"", "\""+ sDate + "\"" , "\""+ sDate + "\"", "\"" + type + "\"" ).toUri(), String.class);
		JSONObject response1 = new JSONObject(response);
		int total = Integer.parseInt(response1.get("total").toString());
		return CompletableFuture.completedFuture(total);
}
}
