package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;

@Service
public class CorrImprActionService {
    @Autowired
    private AtlassianHostRestClients restClients;
    @Autowired
    ErrorService es;
    @Autowired
    ProjectService jcfm;
    @Value("${delivery_defect}")
    String delivery_defect;
    @Value("${corrective_improvement_actions}")
    String corrective_improvement_actions;
    @Value("${reported_date}")
    String reported_date;
    @Value("${category_corrective_improvement_actions}")
    String category_corrective_improvement_actions;
    @Value("${issue_description}")
    String issue_description;
    @Value("${defect_type}")
    String defect_type;
    @Value("${priority}")
    String priority;
    @Value("${root_cause}")
    String root_cause;
    @Value("${action}")
    String action;
    @Value("${action_type}")
    String action_type;
    @Value("${responsibility}")
    String responsibility;
    @Value("${actual_date}")
    String actual_date;
    @Value("${target_date}")
    String target_date;

    @Async
    public CompletableFuture<ArrayList<CreativeIdea>> getCIAOpen(AtlassianHostUser hostUser, String p)
            throws Exception {
	ArrayList<CreativeIdea> cialist = new ArrayList<CreativeIdea>();

	try {
	    String url = "/rest/api/2/search?jql=project= {p} AND (issuetype= {corrective_improvement_actions} OR issuetype= {delivery_defect} )  AND status!='closed'";
	    String response = restClients.authenticatedAs(hostUser)
	            .getForObject(UriComponentsBuilder
	                    .fromUriString(url).buildAndExpand("\"" + p + "\"",
	                            "\"" + corrective_improvement_actions + "\"", "\"" + delivery_defect + "\"")
	                    .toUri(), String.class);
	    JSONObject response1 = new JSONObject(response);
	    int count = response1.getInt("total");
	    if (count != 0) {
		cialist = getParsedData(response1, hostUser);
	    }

	} catch (Exception e) {
	    es.writeException(e, p);
	}
	return CompletableFuture.completedFuture(cialist);
    }

    // Corrective Improvement Actions last month and before last month
    @Async
    public CompletableFuture<ArrayList<CreativeIdea>> getCIAClosedOneMonthback(AtlassianHostUser hostUser, String p,
            int monthsDifference) throws Exception {
	ArrayList<CreativeIdea> cialist = new ArrayList<CreativeIdea>();
	try {
	    int i = -monthsDifference - 1;
	    int j = -monthsDifference - 2;
	    String url = "/rest/api/2/search?jql=project= {p} AND (issuetype= {corrective_improvement_actions} OR issuetype= {delivery_defect} )AND 'Issue Closed Date'>=startOfMonth({j}) AND 'Issue Closed Date'<=endOfMonth({i}) AND status= 'closed'";

	    String response = restClients.authenticatedAs(hostUser)
	            .getForObject(UriComponentsBuilder
	                    .fromUriString(url).buildAndExpand("\"" + p + "\"",
	                            "\"" + corrective_improvement_actions + "\"", "\"" + delivery_defect + "\"", j, i)
	                    .toUri(), String.class);
	    JSONObject response1 = new JSONObject(response);

	    int count = response1.getInt("total");
	    if (count != 0) {
		cialist = getParsedData(response1, hostUser);
	    }
	} catch (Exception e) {
	    es.writeException(e, p);
	}
	return CompletableFuture.completedFuture(cialist);
    }
    // Corrective Improvement Actions before last month
    /*
     * @Async public CompletableFuture<ArrayList<CorrImprAction>>
     * getCIAClosedTwoMonthsback(AtlassianHostUser hostUser, String p) throws
     * Exception { ArrayList<CorrImprAction> cialist = new
     * ArrayList<CorrImprAction>(); try { String url =
     * "/rest/api/2/search?jql=project= {p} AND (issuetype= {ciac} OR issuetype= {dd} )AND 'Reported Date'>=startOfMonth(-2) AND 'Reported Date'<=endOfMonth(-2) AND status= 'closed'"
     * ;
     * 
     * String response = restClients.authenticatedAs(hostUser) .getForObject(
     * UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\""
     * + ciac + "\"","\"" + dd + "\"").toUri(), String.class); JSONObject response1
     * = new JSONObject(response); int count = response1.getInt("total"); if(count
     * != 0) { cialist = getParsedData(response1,hostUser); } } catch(Exception e) {
     * es.writeException(e, p); } return CompletableFuture.completedFuture(cialist);
     * }
     */

    public ArrayList<CreativeIdea> getParsedData(JSONObject response, @AuthenticationPrincipal
    AtlassianHostUser hostUser) throws Exception {
	ArrayList<CreativeIdea> cialist = new ArrayList<CreativeIdea>();

	Map<String, String> map = jcfm.customFieldIDMap;
	JSONArray jsonArray = response.getJSONArray("issues");

	if (jsonArray != null) {
	    for (int i = 0; i < jsonArray.length(); i++) {
		JSONObject issuesData = jsonArray.getJSONObject(i);
		JSONObject fieldsData = issuesData.getJSONObject("fields");
		CreativeIdea cia = new CreativeIdea();
		JSONObject iss_type = fieldsData.getJSONObject("issuetype");
		String issuetype = iss_type.getString("name");
		String repdate = fieldsData.getString(map.get(reported_date));
		Date rep = new SimpleDateFormat("yyyy-MM-dd").parse(repdate);
		String repdt = new SimpleDateFormat("dd-MMM-yyyy").format(rep);
		cia.setDate(repdt);
		cia.setStatus(fieldsData.getJSONObject("status").getString("name"));
		if (issuetype.equals(delivery_defect))
		    cia.setCategory("Delivery Defect");
		else {
		    JSONObject category = fieldsData.getJSONObject(map.get(category_corrective_improvement_actions));
		    cia.setCategory(category.getString("value"));
		}
		cia.setDescription(fieldsData.getString(map.get(issue_description)));

		if (issuetype.equals(delivery_defect))
		    cia.setDefect_type("Delivery");
		else {
		    JSONObject defecttype = fieldsData.getJSONObject(map.get(defect_type));

		    cia.setDefect_type(defecttype.getString("value"));
		}
		JSONObject prio = fieldsData.getJSONObject(map.get("Severity of the Issue"));
		cia.setPriority(prio.getString("value"));
		cia.setRoot_cause(fieldsData.getString(map.get(root_cause)));
		cia.setAction(fieldsData.getString(map.get(action)));
		if (issuetype.equals(delivery_defect))
		    cia.setAction_type("Corrective Action");
		else {
		    JSONObject action_type_object = fieldsData.getJSONObject(map.get(action_type));
		    cia.setAction_type(action_type_object.getString("value"));
		}

		JSONArray robject = fieldsData.getJSONArray(map.get(responsibility));
		String resp = "";
		boolean flag = false;
		for (int k = 0; k < robject.length(); k++) {
		    if (flag == true) {
			resp += ",";
		    }
		    JSONObject jo = robject.getJSONObject(k);
		    resp += jo.getString("displayName");
		    flag = true;
		}
		cia.setResponsibility(resp);
		String tgd = fieldsData.getString(map.get(target_date));
		if (!tgd.equals("null") && !tgd.isEmpty()) {
		Date tdate = new SimpleDateFormat("yyyy-MM-dd").parse(tgd);
		String td = new SimpleDateFormat("dd-MMM-yyyy").format(tdate);
		cia.setTarget_date(td);
		}
		String acd = fieldsData.get(map.get(actual_date)).toString();
	//	String acd = fieldsData.getString(map.get(actual_date));
		if (!acd.equals("null") && !acd.isEmpty()) {
		    Date ad = new SimpleDateFormat("yyyy-MM-dd").parse(acd);
		    String adt = new SimpleDateFormat("dd-MMM-yyyy").format(ad);
		    cia.setActual_date(adt);
		}
		else
		    cia.setActual_date("-"); 
		// cia.setRemarks(fieldsData.getString(customfields.get("Remarks (Corr./Improv.
		// Actions)")));
		cialist.add(cia);

	    }
	}

	return cialist;
    }
}
