package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskRangeData;
import com.jiraplugin.metrics.model.RiskRating;

@Service
public class RiskManagementDateRangeService {

	@Value("${Exposure_IssueType}")
	String exposureIssueType;

	@Value("${Treated_RiskValue}")
	String filterTreatedRiskValue;

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	@Value("${Domian_Url}")
	String domianUrl;

	@Value("${Total_Treated_value}")
	String filterTotalTreatedValue;

	@Value("${UserName}")
	String userName;

	@Value("${PassWord}")
	String passWord;

	@Value("${DomainSuffix}")
	String domainSuf;

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	final static String Response_Issue = "issues";
	final static String Response_Field = "fields";

	@Autowired
	private AtlassianHostRestClients restClients;

	RestTemplate restTemplate = new RestTemplate();

	@Async
	public CompletableFuture<Map<String, String>> getAcceptablePercentage(AtlassianHostUser hostUser,
			String snapProject, String pkey2, Map<String, String> userDateRange) {

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName.concat(domainSuf), passWord));

		String totalTreatedUrl = domianUrl.concat(searchIdentifiedUrl).concat(filterTotalTreatedValue);

		String totalTreatedRiskResponse = restTemplate.getForObject(UriComponentsBuilder.fromUriString(totalTreatedUrl)
				.buildAndExpand("\"" + snapProject + "\"", "\"" + exposureIssueType + "\"",
						"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"",
						"\"" + pkey2 + "\"")
				.toUri(), String.class);

		return CompletableFuture.completedFuture(getPercentageCal(totalTreatedRiskResponse, userDateRange, hostUser));
	}

	private Map<String, String> getPercentageCal(String totalTreatedRiskResponse, Map<String, String> userDateRange,
			AtlassianHostUser hostUser) {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);

		Map<String, Double> totalTreatedMonthMap = new LinkedHashMap<>(initialMonthMap);

		JSONObject initialResponse = new JSONObject(totalTreatedRiskResponse);
		JSONArray initialArray = initialResponse.getJSONArray(Response_Issue);
		for (int i = 0; i < initialArray.length(); i++) {

			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);

			String reponseDate = fields.getString(fieldList.get("Reported Date"));
			int riskValue = fields.getInt(fieldList.get("Risk Value"));

			String dateFormate = getConvertDate(reponseDate);

			if (riskValue >= 1 && riskValue <= 4) {
				initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
			} else {
				totalTreatedMonthMap.put(dateFormate, totalTreatedMonthMap.get(dateFormate) + 1);
			}

		}

		Map<String, String> tempMonthMap = new LinkedHashMap<>();

		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);

		for (int i = 0; i < initialMonthMap.size(); i++) {
			System.out.println(initialMonthMap.size());

			try {
				Double treatedV = (initialMonthMap.get(listForm.get(i)));

				Double totalTreatedV = initialMonthMap.get(listForm.get(i)) + totalTreatedMonthMap.get(listForm.get(i));

				Double result = (treatedV / totalTreatedV);

				Double resultValue = Math.floor(result * 100);

				String actualValue = "";

				if (resultValue.isNaN() || resultValue.equals(0.0)) {
					actualValue = "0";
				} else {

					actualValue = resultValue.toString();
				}

				tempMonthMap.put(listForm.get(i), actualValue);

			} catch (Exception e) {
				System.out.println(e + "0/0");
			}
		}

		return tempMonthMap;
	}

	private String getConvertDate(String reponseDate) {

		String sdate = "";
		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date responsd = formatter.parse(reponseDate);

			SimpleDateFormat formatter1 = new SimpleDateFormat("MMM-yy");
			sdate = formatter1.format(responsd);

		} catch (Exception e) {

			System.out.println(e);

		}

		return sdate;
	}

	public Map<String, Double> getMonthListFromDate(Map<String, String> userDateRange) {

		Map<String, Double> dates = new LinkedHashMap<>();

		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date sdate = formatter.parse(userDateRange.get("d1"));
			Date edate = formatter.parse(userDateRange.get("d2"));
			Date start = sdate;
			Date end = edate;

			while (start.compareTo(end) <= 0) {
				dates.put(new SimpleDateFormat("MMM-yy").format(start), 0.0);
				start = DateUtils.addMonths(start, 1);
			}

		} catch (Exception e) {
			System.out.println(e);
		}

		return dates;
	}

	public ArrayList<String> getMonthListFromDateListForm(Map<String, String> userDateRange) {

		ArrayList<String> listDates = new ArrayList<>();

		try {

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date sdate = formatter.parse(userDateRange.get("d1"));
			Date edate = formatter.parse(userDateRange.get("d2"));
			Date start = sdate;
			Date end = edate;

			while (start.compareTo(end) <= 0) {
				listDates.add(new SimpleDateFormat("MMM-yy").format(start));
				start = DateUtils.addMonths(start, 1);
			}

		} catch (Exception e) {
			System.out.println(e);
		}

		return listDates;
	}

	@Value("${filter_Identified_Url}")
	String filterIdentifiedUrl;

	@Async
	public CompletableFuture<Map<String, String>> getCriticalDeliveryIntified(AtlassianHostUser hostUser,
			String snapProject, String pkey2, Map<String, String> userDateRange) {

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName.concat(domainSuf), passWord));

		String identifiedUrl = domianUrl.concat(searchIdentifiedUrl).concat(filterIdentifiedUrl);

		String totalIndetifiedResponse = restTemplate
				.getForObject(
						UriComponentsBuilder.fromUriString(identifiedUrl)
								.buildAndExpand("\"" + snapProject + "\"", "\"" + userDateRange.get("d1") + "\"",
										"\"" + userDateRange.get("d2") + "\"", "\"" + pkey2 + "\"")
								.toUri(),
						String.class);

		return CompletableFuture
				.completedFuture(getCriticalCountMethod(totalIndetifiedResponse, userDateRange, hostUser));
	}

	private Map<String, String> getCriticalCountMethod(String totalIndetifiedResponse,
			Map<String, String> userDateRange, AtlassianHostUser hostUser) {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);

		JSONObject initialResponse = new JSONObject(totalIndetifiedResponse);
		JSONArray initialArray = initialResponse.getJSONArray(Response_Issue);
		for (int i = 0; i < initialArray.length(); i++) {

			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);

			String reponseDate = fields.getString(fieldList.get("Reported Date"));
			String riskProbabilityValue = fields.getJSONObject(fieldList.get("Risk Probability (RP)"))
					.getString("value");
			int checkNum = Integer.parseInt(riskProbabilityValue);
			String dateFormate = getConvertDate(reponseDate);

			if (checkNum >= 3) {
				initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
			}
		}

		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		Map<String, String> tempMonthMap = new LinkedHashMap<>();

		for (int i = initialMonthMap.size() - 1; i >= 0; i--) {

			String actualValue = "";

			System.out.println(initialMonthMap.size());

			if (initialMonthMap.get(listForm.get(i)).isNaN() || initialMonthMap.get(listForm.get(i)).equals(0.0)) {
				actualValue = "0";
			} else {

				Integer conVert = initialMonthMap.get(listForm.get(i)).intValue();
				actualValue = conVert.toString();
			}

			tempMonthMap.put(listForm.get(i), actualValue);
		}

		return tempMonthMap;
	}

	@Value("${Filter_Risk_Occurence}")
	String filterOccurence;

	@Async
	public CompletableFuture<Map<String, String>> getRiskOccurenceDetails(AtlassianHostUser hostUser, String pkey2,
			Map<String, String> userDateRange, String projectChosen) {

		String occurenceUrl = domianUrl.concat(searchIdentifiedUrl).concat(filterOccurence);

		String occurenceResponse = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(occurenceUrl).buildAndExpand("\"" + projectChosen + "\"",
						"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"").toUri(),
				String.class);

		return CompletableFuture.completedFuture(getOccurenceMethod(occurenceResponse, userDateRange, hostUser));
	}

	private Map<String, String> getOccurenceMethod(String occurenceResponse, Map<String, String> userDateRange,
			AtlassianHostUser hostUser) {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		Map<String, Double> initialMonthMap = getMonthListFromDate(userDateRange);

		JSONObject initialResponse = new JSONObject(occurenceResponse);
		JSONArray initialArray = initialResponse.getJSONArray(Response_Issue);
		for (int i = 0; i < initialArray.length(); i++) {

			JSONObject issueArray = initialArray.getJSONObject(i);

			JSONObject fields = issueArray.getJSONObject(Response_Field);

			String reponseDate = fields.getString(fieldList.get("Risk Occurrence Date"));

			String dateFormate = getConvertDate(reponseDate);

			initialMonthMap.put(dateFormate, initialMonthMap.get(dateFormate) + 1);
		}
		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		Map<String, String> tempMonthMap = new LinkedHashMap<>();

		for (int i = 0; i < initialMonthMap.size(); i++) {

			String actualValue = "";

			System.out.println(initialMonthMap.size());

			if (initialMonthMap.get(listForm.get(i)).isNaN() || initialMonthMap.get(listForm.get(i)).equals(0.0)) {
				actualValue = "0";
			} else {

				Integer conVert = initialMonthMap.get(listForm.get(i)).intValue();
				actualValue = conVert.toString();
			}

			tempMonthMap.put(listForm.get(i), actualValue);
		}

		return tempMonthMap;
	}

	@Async
	public CompletableFuture<ArrayList<ResultRangeData>> getExposureRatingValues(AtlassianHostUser hostUser,
			String snapProject, String pkey2, Map<String, String> userDateRange) {

		String rValue = "", residualRValue = "";

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		String url = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={project} AND issuetype='Exposure-Rating' AND 'Client Name' ~ {promain} AND 'Reported Date'>= {date} & 'Reported Date'<= {date}&startAt=0&maxResults=100";
		String response = restTemplate.getForObject(
				UriComponentsBuilder.fromUriString(url)
						.buildAndExpand("\"" + snapProject + "\"", "\"" + pkey2 + "\"",
								"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"")
						.toUri(),
				String.class);
		JSONObject jsonObj = new JSONObject(response);
		JSONArray jsonArray = jsonObj.getJSONArray("issues");
		int count = jsonObj.getInt("total");
		int maxResults = jsonObj.getInt("maxResults");
		if (count > maxResults) {
			url = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={project} AND issuetype='Exposure-Rating' AND 'Client Name' ~ {promain} AND 'Reported Date'>= {date} & 'Reported Date'<= {date}&startAt=100&maxResults=100";
			String response1 = restTemplate.getForObject(UriComponentsBuilder.fromUriString(url)
					.buildAndExpand("\"" + snapProject + "\"", "\"" + pkey2 + "\"",
							"\"" + userDateRange.get("d1") + "\"", "\"" + userDateRange.get("d2") + "\"")
					.toUri(), String.class);
			JSONObject jsonObj1 = new JSONObject(response1);
			JSONArray jsonArray1 = jsonObj1.getJSONArray("issues");
			for (int i = 0; i < jsonArray1.length(); i++) {
				JSONObject objects = jsonArray1.getJSONObject(i);
				jsonArray.put(objects);
			}
		}
		ArrayList<RiskRangeData> hdfc = new ArrayList<RiskRangeData>();
		RiskRating fielData = new RiskRating();
		for (int i = 0; i < jsonArray.length(); i++) {
			RiskRangeData data = new RiskRangeData();
			JSONObject issuesData = jsonArray.getJSONObject(i);
			JSONObject fieldsData = issuesData.getJSONObject("fields");
			Date monthandyear = null;
			try {
				monthandyear = new SimpleDateFormat("yyyy-MM")
						.parse(fieldsData.getString(fieldList.get("Reported Date")));
			} catch (JSONException e1) {

				e1.printStackTrace();
			} catch (ParseException e1) {

				e1.printStackTrace();
			}
			String month = new SimpleDateFormat("MMM-yy").format(monthandyear);
			data.setCreated(month);
			int riskValue = 0, residualValue = 0;
			try {
				riskValue = fieldsData.getInt(fieldList.get("Risk Value"));
				data.setRiskValue(riskValue);
			} catch (Exception e) {
				data.setRiskValue(0);
			}
			try {
				residualValue = fieldsData.getInt(fieldList.get("Residual Risk Value"));
				data.setResidualRiskValue(residualValue);
			} catch (Exception e) {
				data.setResidualRiskValue(0);
			}
			System.out.println(data.getCreated());
			System.out.println(data.getResidualRiskValue());
			System.out.println(data.getRiskValue());
			System.out.println(i);
			System.out.println("ïiiiiiiiiiiiii*****************************************************************");
			hdfc.add(data);
		}

		fielData.setFields(hdfc);
		ArrayList<ResultRangeData> result = new ArrayList<ResultRangeData>();

		ArrayList<String> listForm = getMonthListFromDateListForm(userDateRange);
		result = calAverage(fielData, listForm);
		return CompletableFuture.completedFuture(result);

	}

	public ArrayList<ResultRangeData> calAverage(RiskRating data, ArrayList<String> listForm) {

		ArrayList<String> tempMapValues = listForm;
		ArrayList<ResultRangeData> result = new ArrayList<>();
		ArrayList<RiskRangeData> fieldsData = new ArrayList<RiskRangeData>();
		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {
			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;
		}
		float riskSum = 0, residualSum = 0, count = 0, avgRisk = 0, avgResidual = 0;
		String month;

		fieldsData = data.getFields();

		for (int j = 0; j < tempMapValues.size(); j++) {
			ResultRangeData avgresult = new ResultRangeData();
			month = tempMapValues.get(j);
			for (RiskRangeData i : fieldsData) {
				if (month.equals(i.getCreated())) {
					residualSum += i.getResidualRiskValue();
					riskSum += i.getRiskValue();
					count += 1;
				}
			}
			if (count > 0) {
				avgRisk = riskSum / count;
				avgResidual = residualSum / count;
			} else {
				avgRisk = 0;
				avgResidual = 0;
			}
			count = 0;
			residualSum = 0;
			riskSum = 0;
			avgresult.setMonth(month);
			avgresult.setAvg_risk(riskRating(Math.round(avgRisk)));
			avgresult.setAvg_residual(riskRating(Math.round(avgResidual)));
			avgresult.setAvg_residual_value(bucketValueRange(Math.round(avgResidual)));
			avgresult.setAvg_risk_value(bucketValueRange(Math.round(avgRisk)));
			result.add(avgresult);
		}
		System.out.println(result);
		return result;
	}

	public int riskRating(int value) {
		int rating = 0;
		if ((value < 1) || (value > 36)) {
			rating = 0;
		} else if ((value >= 1) && (value <= 4)) {
			rating = 1;
		} else if ((value >= 5) && (value <= 9)) {
			rating = 2;
		} else if ((value >= 10) && (value <= 17)) {
			rating = 3;
		} else if ((value >= 18) && (value <= 28)) {
			rating = 4;
		} else if ((value >= 29) && (value <= 36)) {
			rating = 5;
		}
		return rating;
	}

	public String bucketValueRange(int value) {
		String range = "";
		if ((value < 1) || (value > 36)) {
			range = "-";
		} else if ((value >= 1) && (value <= 4)) {
			range = "1-Low";
		} else if ((value >= 5) && (value <= 9)) {
			range = "2-Medium";
		} else if ((value >= 10) && (value <= 17)) {
			range = "3-High";
		} else if ((value >= 18) && (value <= 28)) {
			range = "4-Very High";
		} else if ((value >= 29) && (value <= 36)) {
			range = "5-Critical";
		}
		return range;
	}

}
