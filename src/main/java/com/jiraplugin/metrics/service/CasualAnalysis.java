package com.jiraplugin.metrics.service;

import java.util.ArrayList;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;
@Service
public class CasualAnalysis {
	
	@Autowired
	public CorrImprActionService corrimpActionService;
	public ArrayList<CreativeIdea> getcasualanalysis( AtlassianHostUser hostUser,String p, int monthsDifference) throws Exception{
		ArrayList<CreativeIdea> cialist=new ArrayList<CreativeIdea>();
		
		CompletableFuture<ArrayList<CreativeIdea>> ciaopen = corrimpActionService.getCIAOpen( hostUser,p);

		CompletableFuture<ArrayList<CreativeIdea>> ciaclosed1month = corrimpActionService.getCIAClosedOneMonthback(hostUser,p,monthsDifference);
		//CompletableFuture<ArrayList<CorrImprAction>> ciaclosed2month = corrimpActionService.getCIAClosedTwoMonthsback(hostUser,p);
		//CompletableFuture.allOf(ciaopen, ciaclosed1month,ciaclosed2month);
		CompletableFuture.allOf(ciaopen, ciaclosed1month);
		cialist.addAll(ciaopen.get());
		cialist.addAll(ciaclosed1month.get());
		//cialist.addAll(ciaclosed2month.get());

	
		return cialist;
		
	}
}