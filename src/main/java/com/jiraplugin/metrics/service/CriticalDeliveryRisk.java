package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class CriticalDeliveryRisk {

	@Autowired
	CriticalDeliveryRiskAsyn criticalDeliveryRiskAsyn;

	@Async
	public CompletableFuture<ArrayList<ArrayList<String>>> startThreads(AtlassianHostUser hostUser,
			String selectedProject, String riskUrl, String promain) throws InterruptedException, ExecutionException {

		ArrayList<ArrayList<String>> riskAcceptableResult = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultArray;

		long start = System.currentTimeMillis();

		CompletableFuture<Integer> page1 = criticalDeliveryRiskAsyn.findCount(hostUser, -1, -1, riskUrl, promain);

		CompletableFuture<Integer> page2 = criticalDeliveryRiskAsyn.findCount(hostUser, -2, -2, riskUrl, promain);

		CompletableFuture<Integer> page3 = criticalDeliveryRiskAsyn.findCount(hostUser, -3, -3, riskUrl, promain);

		CompletableFuture<Integer> page4 = criticalDeliveryRiskAsyn.findCount(hostUser, -4, -4, riskUrl, promain);

		CompletableFuture.allOf(page1, page2, page3, page4).join();

		int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {

			resultArray = new ArrayList<String>();

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.add(months[j]);
			resultArray.add(String.valueOf(resultString[j]));

			riskAcceptableResult.add(resultArray);
		}

		return CompletableFuture.completedFuture(riskAcceptableResult);

	}

	@Async
	public CompletableFuture<ArrayList<ArrayList<String>>> ocurrenceThreads(AtlassianHostUser hostUser,
			String selectedProject, String riskUrl) throws InterruptedException, ExecutionException {

		ArrayList<ArrayList<String>> riskAcceptableResult = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultArray;

		long start = System.currentTimeMillis();

		CompletableFuture<Integer> page1 = criticalDeliveryRiskAsyn.findCountClient(hostUser, -1, riskUrl);

		CompletableFuture<Integer> page2 = criticalDeliveryRiskAsyn.findCountClient(hostUser, -2, riskUrl);

		CompletableFuture<Integer> page3 = criticalDeliveryRiskAsyn.findCountClient(hostUser, -3, riskUrl);

		CompletableFuture<Integer> page4 = criticalDeliveryRiskAsyn.findCountClient(hostUser, -4, riskUrl);

		CompletableFuture.allOf(page1, page2, page3, page4).join();

		int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {

			resultArray = new ArrayList<String>();

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.add(months[j]);
			resultArray.add(String.valueOf(resultString[j]));

			riskAcceptableResult.add(resultArray);
		}

		return CompletableFuture.completedFuture(riskAcceptableResult);

	}

	@Async
	public CompletableFuture<ArrayList<ArrayList<String>>> riskRecurrenceThreads(AtlassianHostUser hostUser,
			String selectedProject, String promain) throws InterruptedException, ExecutionException {

		ArrayList<ArrayList<String>> riskAcceptableResult = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultArray;

		String riskUrl = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project='" + selectedProject
				+ "' AND issuetype = 'Reccurence' AND 'Reported Date' >= startOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Client Name' ~ {promain}";

		long start = System.currentTimeMillis();

		Calendar cal = Calendar.getInstance();

		int monthCount = cal.get(Calendar.MONTH);

		int backMonth1 = 0;
		int backMonth2 = 0;
		int backMonth3 = 0;
		int backMonth4 = 0;

		switch (monthCount) {

		case 0:
			backMonth1 = 0;
			backMonth2 = -12;
			backMonth3 = -12;
			backMonth4 = -12;
			break;

		case 1:
			backMonth1 = -1;
			backMonth2 = -1;
			backMonth3 = -13;
			backMonth4 = -13;

			break;
		case 2:
			backMonth1 = -2;
			backMonth2 = -2;
			backMonth3 = -2;
			backMonth4 = -14;
			break;

		default:
			backMonth1 = -monthCount;
			backMonth2 = -monthCount;
			backMonth3 = -monthCount;
			backMonth4 = -monthCount;
			break;

		}

		CompletableFuture<Integer> page1 = criticalDeliveryRiskAsyn.findCount(hostUser, -1, backMonth1, riskUrl,
				promain);

		CompletableFuture<Integer> page2 = criticalDeliveryRiskAsyn.findCount(hostUser, -2, backMonth2, riskUrl,
				promain);

		CompletableFuture<Integer> page3 = criticalDeliveryRiskAsyn.findCount(hostUser, -3, backMonth3, riskUrl,
				promain);

		CompletableFuture<Integer> page4 = criticalDeliveryRiskAsyn.findCount(hostUser, -4, backMonth4, riskUrl,
				promain);

		CompletableFuture.allOf(page1, page2, page3, page4).join();

		int[] resultString = { page1.get(), page2.get(), page3.get(), page4.get() };

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {

			resultArray = new ArrayList<String>();

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.add(months[j]);
			resultArray.add(String.valueOf(resultString[j]));

			riskAcceptableResult.add(resultArray);
		}

		return CompletableFuture.completedFuture(riskAcceptableResult);

	}

	@Autowired
	private AboutProject aboutproject;

	@Autowired
	private ProjectService jiraFieldList;

	public Map<String, String> riskOpenIssuesMethod(String sce, AtlassianHostUser hostUser)
			throws JSONException, NullPointerException {

		Map<String, String> mapFieldsMain = new LinkedHashMap<String, String>();

		Map<String, String> fieldList = jiraFieldList.customFieldIDMap;

		JSONObject boj = aboutproject.getFieldKeys(sce, hostUser);

		JSONArray jsonArray = boj.getJSONArray("issues");

		try {

			JSONObject dataObject = jsonArray.getJSONObject(0);

			JSONObject real = dataObject.getJSONObject("fields");

			mapFieldsMain.put("Client Name", real.getString(fieldList.get("Client Name")));
			mapFieldsMain.put("Scope", real.getString(fieldList.get("Scope")));
			mapFieldsMain.put("Project Start Date", real.getString(fieldList.get("Project Start Date")));
			mapFieldsMain.put("Current Status", real.getString(fieldList.get("Current Status")));
			int KO = (real.getInt(fieldList.get("Team Size")));
			mapFieldsMain.put("Team Size", String.valueOf(KO));
			/*
			 * mapFieldsMain.put("Project End Date",
			 * real.getString(fieldList.get("Project End Date")));
			 */
		} catch (Exception e) {

			System.out.println(e);

		}

		return mapFieldsMain;

	}

	@Autowired
	private RiskService riskService;

	public Map<Integer, List<String>> riskOpenItems(String sce, AtlassianHostUser hostUser)
			throws JSONException, ParseException {

		int errorFlag = 0;

		Map<Integer, List<String>> mapFieldsMain = new LinkedHashMap<Integer, List<String>>();

		List<String> fnm;

		String tds = "-";
		String ads = "-";

		SimpleDateFormat dt1 = new SimpleDateFormat("dd-MMM-yyyy");

		try {

			Map<String, String> fieldList = jiraFieldList.customFieldIDMap;

			JSONArray jsonArray = riskService.riskOpenIssues(sce);

			if (jsonArray.length() != 0) {

				for (int j = 0; j < jsonArray.length(); j++) {

					fnm = new ArrayList<String>();
					// fnm.removeAll(fnm);

					JSONObject dataObject = jsonArray.getJSONObject(j);

					JSONObject real = dataObject.getJSONObject("fields");
					if (real.get(fieldList.get("Risk Description").toString()).equals(null)) {
						System.out.println("empty issue");

					} else {

						if (!real.get(fieldList.get("Target Date")).equals(null)) {

							Date td = new SimpleDateFormat("yyyy-MM-dd")
									.parse(real.getString(fieldList.get("Target Date")));

							tds = dt1.format(td);

						}
						if (!real.get(fieldList.get("Actual Date")).equals(null)) {

							Date ad = new SimpleDateFormat("yyyy-MM-dd")
									.parse(real.getString(fieldList.get("Actual Date")));

							ads = dt1.format(ad);
						}

						fnm.add(real.get(fieldList.get("Risk Description")).toString());
						fnm.add(real.get(fieldList.get("Risk Treatment Action / Control")).toString());
						fnm.add(tds);
						fnm.add(ads);
						String statusObj = real.get("status").toString();
						JSONObject jobj = new JSONObject(statusObj);
						String statusString = jobj.getString("name");

						fnm.add(statusString);

						fnm.replaceAll(s -> s == "null" ? "Yet To Plan" : s);
						mapFieldsMain.put(j, fnm);


					}

				}
			}


		} catch (Exception e) {
		    	e.printStackTrace();
			System.out.println(e);

			errorFlag = 1;

		}

		if (errorFlag == 1) {

			return null;

		} else {

			return mapFieldsMain;

		}

	}

}
