package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.jiraplugin.metrics.model.AuditInfo;
import com.jiraplugin.metrics.model.IoData;
import com.jiraplugin.metrics.model.NcData;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AnnualAudit;

@Service

public class AuditsService {
	@Autowired
	NCandIOService ncioservice;
	@Autowired
	public JiraCustomFieldMap jiraCustomFieldMap;
	@Autowired
	private AtlassianHostRestClients restClients;

	public AuditInfo getAuditReports(String p, String cyc, String years,AtlassianHostUser hostUser) throws Exception{
	
		String[] period=years.split("-");
		String year1=period[0];
		String year2=period[1];
		System.out.println(year1);
		String startdate=year1+"/04/01";
		String enddate=year2+"/03/31";		
		String cycle=cyc.replace("-", " ");
		String iar="Internal Audit Report";
		String aap="Annual Audit Plan";
		String aafrom="From";
		String aato="To";
		String aid="Audit ID";
		String adt="Audit Date";
		String cyclefrom = null,cycleto=null;
		Map<String, String> customFieldIDMap =jiraCustomFieldMap.getJiraFieldList(hostUser) ;
		String externalfrom=customFieldIDMap.get("From (external audit)");
		String externalto=customFieldIDMap.get("To (external audit)");
		String cycle1from=customFieldIDMap.get("From (cycle-1)");
		String cycle1to=customFieldIDMap.get("To (cycle-1)");
		String cycle2from=customFieldIDMap.get("From (cycle-2)");
		String cycle2to=customFieldIDMap.get("To (cycle-2)");
		String cycle3from=customFieldIDMap.get("From (cycle-3)");
		String cycle3to=customFieldIDMap.get("To (cycle-3)");
		String url="/rest/api/2/search?jql=project={p} AND issuetype={aap} AND {from}>={startdate} AND {to}<={enddate}";
		String resp = restClients.authenticatedAsHostActor().getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"","\"" + aap + "\"","\"" + aafrom + "\"","\"" + startdate + "\"","\"" + aato + "\"","\"" + enddate + "\"").toUri(), String.class);

		
		JSONObject jsonObject = new JSONObject(resp);
		JSONArray jsonArr =  jsonObject.getJSONArray("issues");
		if(jsonArr != null ) {
			for(int i = 0; i < jsonArr.length(); i++) {
		
				
				JSONObject issuesData = jsonArr.getJSONObject(i);
				JSONObject fieldsData = issuesData.getJSONObject("fields");
				
				if(cycle.equals("Cycle 1")){
					cyclefrom=fieldsData.getString(cycle1from);
					cycleto=fieldsData.getString(cycle1to);
				}
               if(cycle.equals("Cycle 2")){
            	   cyclefrom=fieldsData.getString(cycle2from);
					cycleto=fieldsData.getString(cycle2to);
				}
               if(cycle.equals("Cycle 3")){
            	   cyclefrom=fieldsData.getString(cycle3from);
					cycleto=fieldsData.getString(cycle3to);
                }
                 if(cycle.equals("External")){
	                 cyclefrom=fieldsData.getString(externalfrom);
	                 cycleto=fieldsData.getString(externalto);
                }
			
			   
			}
			}
		Date auditcyclefrom=new SimpleDateFormat("dd-MMM-yyyy").parse(cyclefrom);  
		String fromdate=new SimpleDateFormat("yyyy/MM/dd").format(auditcyclefrom);
		Date auditcycleto=new SimpleDateFormat("dd-MMM-yyyy").parse(cycleto); 
		String todate=new SimpleDateFormat("yyyy/MM/dd").format(auditcycleto);
		String urlString = "/rest/api/2/search?jql=project={p} AND issuetype={iar} AND {aid}~{cycl} AND {adt}>={d} AND {adt}<={e}";
		
		
		String response = restClients.authenticatedAsHostActor().getForObject(UriComponentsBuilder.fromUriString(urlString).buildAndExpand("\"" + p + "\"","\"" + iar + "\"","\"" + aid + "\"","\"" + cycle + "\"","\"" + adt + "\"","\"" + fromdate + "\"","\"" + adt + "\"","\"" + todate + "\"").toUri(), String.class);
		
	
		String id=customFieldIDMap.get("Audit ID");
		String adtarea=customFieldIDMap.get("Audit Area");
		String auditors=customFieldIDMap.get("Auditors");
		String auditees=customFieldIDMap.get("Auditees");
		String auditDate=customFieldIDMap.get("Audit Date");
		String preparedby=customFieldIDMap.get("Audit Report prepared by");
		String Good_Points=customFieldIDMap.get("Good Points");
		
		ArrayList<IoData> iolist=new ArrayList<IoData>();
		ArrayList<NcData> nclist=new ArrayList<NcData>();
		ArrayList<Integer> ncl=new ArrayList<Integer>();
		ArrayList<Integer> iol=new ArrayList<Integer>();
		AuditInfo auditinf=new AuditInfo();
		int nccount=0;
		int iocount=0;
		JSONObject jsonObj = new JSONObject(response);
		JSONArray jsonArray =  jsonObj.getJSONArray("issues");
		if(jsonArray != null ) {
			for(int i = 0; i < jsonArray.length(); i++) {
		
				
				JSONObject issuesData = jsonArray.getJSONObject(i);
				JSONObject fieldsData = issuesData.getJSONObject("fields");
				auditinf.setId(fieldsData.getString(id));
				System.out.println(fieldsData.getString(id));
			auditinf.setArea(fieldsData.getString(adtarea));
			   auditinf.setAuditors(fieldsData.getString(auditors));
			   auditinf.setAuditees(fieldsData.getString(auditees));
			   auditinf.setAuditDate(fieldsData.getString(auditDate));
			   auditinf.setPreparedBy(fieldsData.getString(preparedby));
			   if(!fieldsData.isNull(Good_Points))
			   auditinf.setGoodPoints(fieldsData.getString(Good_Points));
			   JSONArray subArray =  fieldsData.getJSONArray("subtasks");
			   if(subArray != null ) {
					for(int j = 0; j < subArray.length(); j++) {
				
						
						JSONObject stData = subArray.getJSONObject(j);
						JSONObject fddt=stData.getJSONObject("fields");
						JSONObject sttyp=fddt.getJSONObject("issuetype");
						if(sttyp.get("name").equals("NC (Non-Conformance)")){
							nccount++;
							ncl.add(stData.getInt("id"));
						}
						if(sttyp.get("name").equals("Improvement Opportunities")){
							iocount++;
							iol.add(stData.getInt("id"));
						}
					}
					}
			   System.out.println("iocount"+iocount);
			   System.out.println("iol"+iol);
			   System.out.println("nccount"+nccount);
			   System.out.println("ncl"+ncl);
			   //String sbturl = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={p} AND issuetype={iar} AND {aid}~{cycl} AND {adt}>={d} AND {adt}<={e}";
			   CompletableFuture<NcData> ncdt = null;
			   CompletableFuture<IoData> iodt = null;
			   int issueid;
				for(int m=0;m<nccount;m++){
					issueid=ncl.get(m);
					System.out.println("nc"+issueid);
        	 ncdt =ncioservice.getNCS(issueid,hostUser);
        	 NcData ncdata=ncdt.get();
        	 nclist.add(ncdata);
				}
				for(int n=0;n<iocount;n++){
					issueid=iol.get(n);
        		 iodt =ncioservice.getIOS(issueid,hostUser);
        		 IoData iodata=iodt.get();
        		 iolist.add(iodata);
				}
				if(iolist.isEmpty())
					CompletableFuture.allOf(ncdt);
				else if(iolist.isEmpty())
					CompletableFuture.allOf(iodt);
				else	
				CompletableFuture.allOf(iodt,ncdt).join();
				auditinf.setIo(iolist);
				auditinf.setNc(nclist);
	
			}
			}
		return auditinf;
	
	
	}
	}
