package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.MetricsData;

@Service
public class SprintService {
	@Autowired ProjectService ps;
	@Autowired
	private AtlassianHostRestClients restClients;
	/*@Cacheable("Defect Data")*/
	/*@CacheEvict(value="Defect Data", allEntries=true)*/
	@Autowired DevEffectivenessService deveffService;

	public ArrayList<MetricsData> getSprintsData(int id, AtlassianHostUser hostUser) throws Exception{
		ArrayList<MetricsData> data = new ArrayList<MetricsData>();
		ArrayList<String> names = new ArrayList<String>(); 
		Map<Integer, Integer> boardIds = ps.getBoardIds();
		ArrayList<Integer> ids = new ArrayList<Integer>();	
		int boardId = boardIds.get(id); 
		int startAt = 0;
		boolean isLast = false;
		JSONArray jsonArray = new JSONArray();
		while(isLast == false) {
			startAt = jsonArray.length();
			   String url = "/rest/agile/1.0/board/"+ boardId + "/sprint?state=closed&startAt="+startAt;
			   String response = restClients.authenticatedAs(hostUser).getForObject(url , String.class);
				JSONObject jsonObj1 = new JSONObject(response);
				isLast = jsonObj1.getBoolean("isLast");
				JSONArray jsonArray1 = jsonObj1.getJSONArray("values");
				for (int i = 0; i < jsonArray1.length(); i++) {
					JSONObject objects = jsonArray1.getJSONObject(i);
					jsonArray.put(objects);
				}
		}
		for(int i = jsonArray.length()-1 ; i >= 0 && (ids.size() <= 5); i--) {
			JSONObject jsob = jsonArray.getJSONObject(i);
			Date date1=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(jsob.getString("completeDate"));  
			SimpleDateFormat output = new SimpleDateFormat("dd-MM-yyyy");
			String formatted = output.format(date1);
			date1 = output.parse(formatted);
			Date iDate = new SimpleDateFormat("dd-MM-yyyy").parse("01-07-2018");
			iDate = output.parse(output.format(iDate));
			 if (date1.compareTo(iDate) <= 0) {
			ids.add(jsob.getInt("id"));
			names.add(jsob.getString("name"));
			 }
		}
		
		CompletableFuture<MetricsData>[] array = (CompletableFuture<MetricsData>[]) new CompletableFuture[ids.size()];
		for(int i = 0; i < ids.size(); i++) {
			/*array[i] = (CompletableFuture<MetricsData>) deveffService.getMonthlyDefectsData(names.get(i), ids.get(i), hostUser,1);*/
			array[i] = (CompletableFuture<MetricsData>) deveffService.getMonthlyDefectsData(names.get(i),ids.get(i), hostUser);
		}
		/*CompletableFuture<DEffectivenessData> t0 = d.getDefectsCount(names.get(0), ids.get(0), hostUser);
		
		CompletableFuture<DEffectivenessData> t1 = d.getDefectsCount(names.get(1), ids.get(1), hostUser);
		
		CompletableFuture<DEffectivenessData> t2 = d.getDefectsCount(names.get(2), ids.get(2), hostUser);	
		
		CompletableFuture<DEffectivenessData> t3 = d.getDefectsCount(names.get(3), ids.get(3), hostUser);
		
		CompletableFuture<DEffectivenessData> t4 = d.getDefectsCount(names.get(4), ids.get(4), hostUser);*/
		CompletableFuture.allOf(array).join();
		for(int i = 0; i < ids.size(); i++) {
			data.add(array[i].get());
		}
		
		/*data.add(t0.get());
		data.add(t1.get());
		data.add(t2.get());
		data.add(t3.get());
		data.add(t4.get());
		*/return data;
	}
}