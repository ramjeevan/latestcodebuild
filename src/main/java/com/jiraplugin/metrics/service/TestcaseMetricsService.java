package com.jiraplugin.metrics.service;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.TestCase;

@Service
public class TestcaseMetricsService {
    @Autowired
    private AtlassianHostRestClients restClients;
    @Autowired
    ErrorService es;
    @Autowired
    ProjectService jcfm;
    @Value("${development_effectiveness_testing}")
    String development_effectiveness_testing;
    @Value("${manual_test_req}")
    String manual_test_req;
    @Value("${automation_test_req}")
    String automation_test_req;
    @Value("${test_cases_planned_to_design}")
    String test_cases_planned_to_design;
    @Value("${test_cases_designed}")
    String test_cases_designed;
    @Value("${test_cases_planned_for_execution}")
    String test_cases_planned_for_execution;
    @Value("${test_cases_actually_executed}")
    String test_cases_actually_executed;
    @Value("${test_cases_passed}")
    String test_cases_passed;
    @Value("${test_cases_failed}")
    String test_cases_failed;
    @Value("${testable_requirements_for_which_tests_are_executed}")
    String testable_requirements_for_which_tests_are_executed;
    @Value("${ftr_by_client}")
    String ftr_by_client;
    @Value("${reported_date}")
    String reported_date;

    public CompletableFuture<TestCase> getTestingMetrics(String p, int monthsDifference, AtlassianHostUser hostUser,
            ArrayList<MetricsData> performanceList) throws Exception {
	TestCase data = new TestCase();

	try {

	    Map<String, String> customfields = jcfm.customFieldIDMap;
	    String url = "/rest/api/2/search?jql=project={p} AND (issuetype={development_effectiveness_testing} OR  issuetype={manual_test_req} OR issuetype={automation_test_req}) AND 'Reported Date'>=startOfMonth({i}) AND 'Reported Date'<=endOfMonth({i})&startAt=0&maxResults=100";
	    String response = restClients.authenticatedAs(hostUser)
	            .getForObject(UriComponentsBuilder.fromUriString(url)
	                    .buildAndExpand("\"" + p + "\"", "\"" + development_effectiveness_testing + "\"",
	                            "\"" + manual_test_req + "\"", "\"" + automation_test_req + "\"", monthsDifference,
	                            monthsDifference)
	                    .toUri(), String.class);
	    JSONObject jsonObj = new JSONObject(response);
	    JSONArray jsonArray = jsonObj.getJSONArray("issues");
	    int total_test_cases_planned_to_design = 0;
	    int total_test_cases_designed = 0;
	    int total_test_cases_passed = 0;
	    int total_test_cases_failed = 0;
	    int total_test_cases_planned_for_execution = 0;
	    int total_test_cases_actually_executed = 0;
	    int total_testable_requirements_for_which_tests_are_executed = 0;
	    int total_ftr=0;
	    double test_design_review_effort = 0;
	    if (jsonArray != null && jsonArray.length() > 0) {
		for (int i = 0; i < jsonArray.length(); i++) {
		    int tests_planned_to_design = 0;
		    int tests_designed = 0;
		    int tests_passed = 0;
		    int tests_failed = 0;
		    int tests_planned_for_execution = 0;
		    int tests_actually_executed = 0;
		    int testable_requirements_for_which_tests_executed = 0;
		    int ftr=0;
		    JSONObject issuesData = jsonArray.getJSONObject(i);
		    JSONObject fieldsData = issuesData.getJSONObject("fields");

		    String planned_to_design = fieldsData.get(customfields.get(test_cases_planned_to_design))
		            .toString();
		    if (!planned_to_design.equals("null") && !planned_to_design.isEmpty())
			tests_planned_to_design = (int) Double.parseDouble(planned_to_design);
		    String designed = fieldsData.get(customfields.get(test_cases_designed)).toString();
		    if (!designed.equals("null") && !designed.isEmpty())
			tests_designed = (int) Double.parseDouble(designed);

		    String passed = fieldsData.get(customfields.get(test_cases_passed)).toString();
		    if (!passed.equals("null") && !passed.isEmpty())
			tests_passed = (int) Double.parseDouble(passed);
		    String failed = fieldsData.get(customfields.get(test_cases_failed)).toString();
		    if (!failed.equals("null") && !failed.isEmpty())
			tests_failed = (int) Double.parseDouble(failed);
		    String planned_for_execution = fieldsData.get(customfields.get(test_cases_planned_for_execution))
		            .toString();
		    if (!planned_for_execution.equals("null") && !planned_for_execution.isEmpty())
			tests_planned_for_execution = (int) Double.parseDouble(planned_for_execution);
		    String actually_executed = fieldsData.get(customfields.get(test_cases_actually_executed))
		            .toString();
		    if (!actually_executed.equals("null") && !actually_executed.isEmpty())
			tests_actually_executed = (int) Double.parseDouble(actually_executed);
		    String executed_requirements = fieldsData
		            .get(customfields.get(testable_requirements_for_which_tests_are_executed)).toString();
		    if (!executed_requirements.equals("null") && !executed_requirements.isEmpty())
			testable_requirements_for_which_tests_executed = (int) Double.parseDouble(executed_requirements);
		    String ftr_client = fieldsData
		            .get(customfields.get(ftr_by_client)).toString();
		    if (!ftr_client.equals("null") && !ftr_client.isEmpty())
			ftr = (int) Double.parseDouble(ftr_client);
		    total_test_cases_planned_to_design = total_test_cases_planned_to_design + tests_planned_to_design;
		    total_test_cases_designed = total_test_cases_designed + tests_designed;
		    total_test_cases_passed = total_test_cases_passed + tests_passed;
		    total_test_cases_failed = total_test_cases_failed + tests_failed;
		    total_test_cases_planned_for_execution = total_test_cases_planned_for_execution
		            + tests_planned_for_execution;
		    total_test_cases_actually_executed = total_test_cases_actually_executed + tests_actually_executed;
		    total_testable_requirements_for_which_tests_are_executed = total_testable_requirements_for_which_tests_are_executed
		            + testable_requirements_for_which_tests_executed;
		    total_ftr=total_ftr+ftr;
		}

	    }
             
	    data.setTestdesign_planned(total_test_cases_planned_to_design);
	    data.setTestdesign_developed(total_test_cases_designed);
	    data.setTest_cases_passed(total_test_cases_passed);
	    data.setTest_cases_failed(total_test_cases_failed);
	    test_design_review_effort = performanceList.get(-(monthsDifference+1)).getTest_design_review_effort();
	    data.setTest_design_review_effort(test_design_review_effort);
	    int total_testable_requirements = (int) performanceList.get(-(monthsDifference+1)).getTotal_testable();
	    data.setTotal_testable_requirements(total_testable_requirements);
	    if(total_testable_requirements==0)
		data.setSrf("N/A");
	    else {
	    double srf=((double)total_ftr/total_testable_requirements)*100;
	    double srfun = Math.round(srf * 100.0) / 100.0;
	    data.setSrf(String.valueOf(srfun));
	    }
	    
	    data.setTest_cases_planned_for_execution(total_test_cases_planned_for_execution);
	    data.setTest_cases_actually_executed(total_test_cases_actually_executed);
	    int total_defects_in_test_design = performanceList.get(-(monthsDifference+1)).getTotal_test_design_defects();

	    data.setTotaltestdefects(total_defects_in_test_design);
	    if (total_test_cases_planned_for_execution == 0)
		data.setTestexecution("N/A");
	    else {
		double testsexec = (total_test_cases_actually_executed / (double)total_test_cases_planned_for_execution) * 100;
		double testsexecut = Math.round(testsexec * 100.0) / 100.0;
		data.setTestexecution(String.valueOf(testsexecut));
	    }
	    if (total_test_cases_designed == 0)
		data.setTestdesignquality("N/A");
	    else {
		double tdq = total_defects_in_test_design / (double)total_test_cases_designed;
		double tdquality = Math.round(tdq * 100.0) / 100.0;
		data.setTestdesignquality(String.valueOf(tdquality));
	    }
	    data.setTestable_requirements_executed(total_testable_requirements_for_which_tests_are_executed);
	    if (performanceList.get(-(monthsDifference+1)).getTotal_testable() == 0)
		data.setTestcoverage("N/A");
	    else {
		double testcov = ((double)total_testable_requirements_for_which_tests_are_executed
		        / performanceList.get(-(monthsDifference+1)).getTotal_testable()) * 100;
		double testcoverage = Math.round(testcov * 100.0) / 100.0;
		data.setTestcoverage(String.valueOf(testcoverage));
	    }
	    data.setFirst_time_right(total_ftr);
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
	    String month = (YearMonth.now().minusMonths(Math.abs(monthsDifference))).format(formatter);
	    data.setMonth(month);
	} catch (Exception e) {
	    es.writeException(e, p);
	}
	return CompletableFuture.completedFuture(data);
    }

}