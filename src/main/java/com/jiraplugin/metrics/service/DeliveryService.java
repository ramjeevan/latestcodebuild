package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffectivenessData;
@Component
@Service
public class DeliveryService{ 
	
	@Autowired
	IssueTypeThread t;
	

	public ArrayList<EffectivenessData> runThreadsOne(String args,@AuthenticationPrincipal AtlassianHostUser hostUser) throws Exception {   
		String p = args;
		ArrayList<EffectivenessData> data = new ArrayList<EffectivenessData>();
		
		CompletableFuture<EffectivenessData> t1 = t.runTasks(p,-1, hostUser);
		
		CompletableFuture<EffectivenessData> t2 = t.runTasks(p,-2, hostUser);
		
		CompletableFuture<EffectivenessData> t3 = t.runTasks(p,-3, hostUser);
		
		CompletableFuture<EffectivenessData> t4 = t.runTasks(p,-4, hostUser);		
		
		CompletableFuture.allOf(t1,t2,t3,t4).join();
		data.add(t1.get());
		data.add(t2.get());
		data.add(t3.get());
		data.add(t4.get());
	
		return data;
	}
	
	
}
