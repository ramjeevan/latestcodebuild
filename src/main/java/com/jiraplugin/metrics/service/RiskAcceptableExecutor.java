package com.jiraplugin.metrics.service;

import java.text.DecimalFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class RiskAcceptableExecutor {

	@Autowired
	JiraLookupService jiraLookupService;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Async
	public CompletableFuture<ArrayList<ArrayList<String>>> startThreads(AtlassianHostUser hostUser,
			String selectedProject, String promain) throws InterruptedException, ExecutionException {

		ArrayList<ArrayList<String>> riskAcceptableResult = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultArray;

		long start = System.currentTimeMillis();

		CompletableFuture<Double> page1 = jiraLookupService.findCount(hostUser, selectedProject, -1, promain);

		CompletableFuture<Double> page2 = jiraLookupService.findCount(hostUser, selectedProject, -2, promain);

		CompletableFuture<Double> page3 = jiraLookupService.findCount(hostUser, selectedProject, -3, promain);

		CompletableFuture<Double> page4 = jiraLookupService.findCount(hostUser, selectedProject, -4, promain);

		CompletableFuture.allOf(page1, page2, page3, page4).join();

		DecimalFormat df = new DecimalFormat("0.00");

		String[] resultString = { df.format(page1.get().doubleValue()), df.format(page2.get().doubleValue()),
				df.format(page3.get().doubleValue()), df.format(page4.get().doubleValue()) };

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {
			resultArray = new ArrayList<String>();
			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.add(months[j]);
			resultArray.add(resultString[j]);

			riskAcceptableResult.add(resultArray);
		}

		return CompletableFuture.completedFuture(riskAcceptableResult);

	}

}
