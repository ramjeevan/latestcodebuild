package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.fasterxml.jackson.core.JsonParseException;

@Service
public class WebHookHandler {

	@Autowired
	JiraCustomFieldMap jiraCustomFieldMap;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private ProjectService projectService;

	private static final String jiraBaseURL = "https://seneca-global.atlassian.net/rest/api/2/issue/";
	private static final String jiraBaseURLIssue = "https://seneca-global.atlassian.net/rest/api/2/issue";

	RestTemplate restTemplate = new RestTemplate();

	public String updateIssue(String json, AtlassianHostUser hostUser) throws JSONException, JsonParseException {

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		int RecurrenceFalg = 0;
		int RiskValueFlag = 0;
		int RiskOwnerFlag = 0;
		int ExposureFlag = 0;
		int RiskIdentifiedFalg = 0;

		SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-d");
		Date Currentdate = new Date();
		System.out.println(formatter.format(Currentdate));

		Map<String, String> projkeys = projectService.getProjectWebhook();

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);

		Map<String, String> customField = jiraCustomFieldMap.getJiraFieldList(hostUser);

		JSONObject receivedObject = new JSONObject(json);

		JSONObject changeLog = receivedObject.getJSONObject("changelog");

		JSONArray items = changeLog.getJSONArray("items");

		JSONObject issues = receivedObject.getJSONObject("issue");

		String Issuekey = issues.getString("key");

		JSONObject fields = issues.getJSONObject("fields");

		JSONObject getIssutypeObject = fields.getJSONObject("issuetype");

		String getIssueTypeName = getIssutypeObject.getString("name");

		JSONObject project = fields.getJSONObject("project");

		String projectName = project.getString("key");

		String createIssueJSON = null;

		String createJsonIssue = null;

		String newUrl = jiraBaseURL + Issuekey;

		String WebHookEvent = receivedObject.getString("issue_event_type_name");

		JSONObject user = receivedObject.getJSONObject("user");

		String UserName = user.getString("name");

		for (int i = 0; i < items.length(); i++) {

			JSONObject dataObject = items.getJSONObject(i);

			if (dataObject.getString("fieldId").contentEquals(customField.get("Risk Value"))
					|| dataObject.getString("fieldId").contentEquals(customField.get("Business Impact (BI)"))
					|| dataObject.getString("fieldId").contentEquals(customField.get("Risk Probability (RP)"))) {

				int result = 0;

				String rp = fields.getJSONObject(customField.get("Risk Probability (RP)")).getString("value");

				String bi = fields.getJSONObject(customField.get("Business Impact (BI)")).getString("value");

				result = Integer.parseInt(rp) * Integer.parseInt(bi);

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Risk Value") + "\" : " + result + "}}";

				RiskValueFlag = 1;

			} else if (dataObject.getString("fieldId").contentEquals(customField.get("Revised Risk Probability (RRP)"))
					|| dataObject.getString("fieldId").contentEquals(customField.get("Revised Business Impact (RBI)"))
					|| dataObject.getString("fieldId").contentEquals(customField.get("Residual Risk Value"))) {

				int result = 0;

				String rp = fields.getJSONObject(customField.get("Revised Risk Probability (RRP)")).getString("value");

				String bi = fields.getJSONObject(customField.get("Revised Business Impact (RBI)")).getString("value");

				result = Integer.parseInt(rp) * Integer.parseInt(bi);

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Residual Risk Value") + "\" : " + result
						+ "}}";

				RiskValueFlag = 1;

			} else if (dataObject.getString("toString").contentEquals("Risk Recurred")) {

				System.out.println(projkeys);

				String pkey = projkeys.get("New Project Design");
				String pkey2 = projectName;

				String rrd = customField.get("Reported Date");
				String rik = customField.get("Recurrence Issue Key");
				String cn = customField.get("Client Name");

				String rddate = fields.getString(customField.get("Risk Recurrence Date"));

				createJsonIssue = "{\"fields\": {\"project\": {\"key\": \"" + pkey + "\"},\"summary\": \"" + Issuekey
						+ "\",\"issuetype\": {\"name\": \"Reccurence\"},\"" + rrd + "\":\"" + rddate + "\",\"" + rik
						+ "\":\"" + Issuekey + "\", \"" + cn + "\":\"" + pkey2 + "\" }}";

				System.out.println(createJsonIssue);

				RecurrenceFalg = 1;

			} else if (WebHookEvent.contentEquals("issue_moved") && !UserName.contentEquals("admin")) {

				createIssueJSON = "{ \"fields\": { \"" + customField.get("Risk Owner") + "\" :{\"name\":\"" + UserName
						+ "\"}}}";

				RiskOwnerFlag = 1;

				break;

			} else if (dataObject.getString("toString").contentEquals("Risk Identified")) {

				String rd = customField.get("Reported Date");
				String rp = customField.get("Risk Probability (RP)");
				String il = customField.get("Parent Issue Key");
				String pkey = projkeys.get("New Project Design");
				String cn = customField.get("Client Name");
				String pkey2 = projectName;

				String rdd = fields.getString(rd);
				if (rdd.contentEquals("null")) {

					rdd = formatter.format(Currentdate);
				}
				JSONObject rpdj = fields.getJSONObject(rp);
				String rpd = rpdj.getString("value");

				createJsonIssue = "{\"fields\": {\"project\": {\"key\": \"" + pkey + "\"},\"summary\": \"" + Issuekey
						+ "\",\"issuetype\": {\"name\": \"Risk_Identified\"},\"" + rd + "\":\"" + rdd + "\",\"" + rp
						+ "\": {\"value\":\"" + rpd + "\"},\"" + il + "\" : \"" + Issuekey + "\", \"" + cn + "\":\""
						+ pkey2 + "\" }}";

				RiskIdentifiedFalg = 1;

				break;

			} else if (dataObject.getString("toString").contentEquals("Treated")) {

				String pkey = projkeys.get("New Project Design");
				String rp = fields.getJSONObject(customField.get("Risk Probability (RP)")).getString("value");
				String bi = fields.getJSONObject(customField.get("Business Impact (BI)")).getString("value");
				String rv = fields.getJSONObject(customField.get("Revised Risk Probability (RRP)")).getString("value");
				String rrv = fields.getJSONObject(customField.get("Revised Business Impact (RBI)")).getString("value");
				String rik = customField.get("Parent Issue Key");
				String date = customField.get("Reported Date");
				String cn = customField.get("Client Name");
				String dateString = fields.getString(customField.get("Reported Date"));
				String pkey2 = projectName;

				int rvResult = Integer.parseInt(rp) * Integer.parseInt(bi);

				int rrvResult = Integer.parseInt(rv) * Integer.parseInt(rrv);

				createJsonIssue = "{\"fields\": {\"project\": {\"key\": \"" + pkey + "\"},\"summary\": \"" + Issuekey
						+ "\",\"issuetype\": {\"name\": \"Exposure-Rating\"},\"" + customField.get("Risk Value") + "\":"
						+ rvResult + ",\"" + customField.get("Residual Risk Value") + "\": " + rrvResult + " ,\"" + rik
						+ "\" : \"" + Issuekey + "\" ,\"" + date + "\":\"" + dateString + "\", \"" + cn + "\":\""
						+ pkey2 + "\"}}";

				System.out.println(createJsonIssue);

				ExposureFlag = 1;
			}

		}

		headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);

		HttpEntity<String> requestEntity = new HttpEntity<String>(createIssueJSON, headers);

		HttpEntity<String> requestEntityIssue = new HttpEntity<String>(createJsonIssue, headers);

		JSONObject jsonObject = null;

		if (RecurrenceFalg == 1 || ExposureFlag == 1 || RiskIdentifiedFalg == 1) {

			String responseEntityIssue = restTemplate.postForObject(jiraBaseURLIssue, requestEntityIssue, String.class);
		}

		if (RiskValueFlag == 1 || RiskOwnerFlag == 1) {
			try {

				ResponseEntity<String> responseEntity = restClients.authenticatedAsHostActor().exchange(newUrl,
						HttpMethod.PUT, requestEntity, String.class);

				if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
					try {
						jsonObject = new JSONObject(responseEntity.getBody());
					} catch (JSONException e) {
						throw new RuntimeException("JSONException occurred");
					}
				}
			} catch (final HttpClientErrorException httpClientErrorException) {

			}

		}
		return null;

	}

}
