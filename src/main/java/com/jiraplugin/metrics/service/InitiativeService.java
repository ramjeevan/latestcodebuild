package com.jiraplugin.metrics.service;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.InitiativeData;
@Service
public class InitiativeService {
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired 
	JiraCustomFieldMap j;
	@Autowired 
	ErrorService es;
	@Async
	public CompletableFuture<ArrayList<InitiativeData>> getInitiatives(String p, String type,AtlassianHostUser hostUser) throws Exception{
		ArrayList<InitiativeData> data = new ArrayList<InitiativeData>();
		try {
		String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= 'Delivery Improvement Initiatives') AND (status ={type})";
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + type + "\"").toUri(), String.class);
		System.out.println(response);
		JSONObject response1 = new JSONObject(response);
		int count = response1.getInt("total");
		if(count != 0) {
			data = getParsedData(response1,hostUser);
		}
		}catch(Exception e) {
			es.writeException(e,p);
		}
		return CompletableFuture.completedFuture(data);
}
	
	@Async
	public CompletableFuture<ArrayList<InitiativeData>> getClosedData(String p, int sDate,@AuthenticationPrincipal AtlassianHostUser hostUser) throws Exception{
		ArrayList<InitiativeData> data = new ArrayList<InitiativeData>();
		try {
		String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= 'Delivery Improvement Initiatives') AND (status =closed) AND ('Issue Closed Date'>=startOfMonth({sDate}) AND 'Issue Closed Date'<=endOfMonth({sDate}))";
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + sDate + "\"", "\"" + sDate + "\"").toUri(), String.class);
		System.out.println(response);
		JSONObject response1 = new JSONObject(response);
		int count = response1.getInt("total");
		if(count != 0) {
			data = getParsedData(response1,hostUser);
		}
		}catch(Exception e) {
			es.writeException(e, p);
		}
		return CompletableFuture.completedFuture(data);
}
	
	
	public ArrayList<InitiativeData> getParsedData(JSONObject response,@AuthenticationPrincipal AtlassianHostUser hostUser) throws IOException, JSONException, ParseException{
		ArrayList<InitiativeData> data = new ArrayList<InitiativeData>();
		Map<String, String> fieldList = j.getJiraFieldList(hostUser);
		String action = fieldList.get("Delivery Improvement Initiative Action");
		String responsibility = fieldList.get("Responsibility");
		String initiative = fieldList.get("Initiative");
		String cDate = fieldList.get("Captured Date");
		String tDate = fieldList.get("Target Date");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
		JSONArray jsonArray = response.getJSONArray("issues");
		for(int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
			InitiativeData initiativeData = new InitiativeData();
			JSONObject fields = j.getJSONObject("fields");
			initiativeData.setAction(fields.get(action).toString());
			Date date = (Date)formatter.parse(fields.get(cDate).toString());
			String finalDate = newFormat.format(date);
			initiativeData.setCapturedDate(finalDate);
			initiativeData.setInitiative(fields.get(initiative).toString());
			initiativeData.setStatus(fields.getJSONObject("status").get("name").toString());
			JSONArray robject = fields.getJSONArray(responsibility);
			String resp = "";
			boolean flag = false;
			for(int k = 0; k < robject.length(); k++) {
			if(flag == true) {
				resp += ",";
			}
			JSONObject jo = robject.getJSONObject(k);
			resp += jo.get("displayName").toString();
			flag = true;
			}
			initiativeData.setResponsibility(resp);
			initiativeData.setTargetDate(newFormat.format((Date)formatter.parse(fields.get(tDate).toString())));
			data.add(initiativeData);
		}
		return data;
}
}