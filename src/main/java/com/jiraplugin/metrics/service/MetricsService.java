package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.JsonData;
import com.jiraplugin.metrics.model.Metrics;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.MetricsList;
import com.jiraplugin.metrics.model.TestCase;

@Service
public class MetricsService {
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired
	public ProjectService projectService;
	@Autowired
	public MonthsService monthsService;
	@Autowired
	private TestcaseMetricsService testcaseMetricsService;
	@Autowired
	public JiraCustomFieldMap jiraCustomfieldmaps;
	@Autowired
	DevEffectivenessService deveffService;
	public Metrics m = new Metrics();
	int review_defects;
	int testing_defects;
	int delivered_defects;
	int inprocess_defects;
	double revEffect = 0;
	double testEffect = 0;
	double dRE = 0;
	List<Object> mns = new ArrayList<Object>();

	MetricsList ml = new MetricsList();
	String ddf = "Delivery Defect";
	String td = "Testing Defect";
	String rd = "Review Defect";

	public MetricsData getCumulativeDefects(String project, String startDate, String endDate) throws Exception {
		MetricsData data = new MetricsData();
		String urlrd = "/rest/api/2/search?jql=project={p} AND issuetype={rd} AND created>={d} AND created<={e}";
		JsonData responserd = restClients.authenticatedAsHostActor()
				.getForObject(
						UriComponentsBuilder.fromUriString(urlrd).buildAndExpand("\"" + project + "\"",
								"\"" + rd + "\"", "\"" + startDate + "\"", "\"" + endDate + "\"").toUri(),
						JsonData.class);

		review_defects = responserd.getTotal();
		String urltd = "/rest/api/2/search?jql=project={p} AND issuetype={td} AND created>={d} AND created<={e}";
		JsonData responsetd = restClients.authenticatedAsHostActor()
				.getForObject(
						UriComponentsBuilder.fromUriString(urltd).buildAndExpand("\"" + project + "\"",
								"\"" + td + "\"", "\"" + startDate + "\"", "\"" + endDate + "\"").toUri(),
						JsonData.class);

		testing_defects = responsetd.getTotal();
		String urldd = "/rest/api/2/search?jql=project={p} AND issuetype={ddf} AND created>={d} AND created<={e}";

		JsonData responsedd = restClients.authenticatedAsHostActor()
				.getForObject(
						UriComponentsBuilder.fromUriString(urldd).buildAndExpand("\"" + project + "\"",
								"\"" + ddf + "\"", "\"" + startDate + "\"", "\"" + endDate + "\"").toUri(),
						JsonData.class);
		delivered_defects = responsedd.getTotal();
		inprocess_defects = review_defects + testing_defects;
		if (inprocess_defects == 0) {
			data.setRevef("N/A");
			data.setTesteff("N/A");
			data.setDreff("0");
			if (delivered_defects == 0)
				data.setDreff("N/A");
		} else {
			double reeffnss = (review_defects * 100.0) / inprocess_defects;
			revEffect = Math.round(reeffnss * 100.0) / 100.0;
			data.setRevef(String.valueOf(revEffect));

			double te = (testing_defects * 100.0) / inprocess_defects;
			testEffect = Math.round(te * 100.0) / 100.0;
			data.setTesteff(String.valueOf(testEffect));
			double dre = (inprocess_defects * 100.0) / (inprocess_defects + delivered_defects);
			dRE = Math.round(dre * 100.0) / 100.0;
			data.setDreff(String.valueOf(dRE));
		}

		data.setRevd(review_defects);

		data.setTesd(testing_defects);
		data.setDeld(delivered_defects);

		data.setInpd(inprocess_defects);

		return data;
	}

	public ArrayList<MetricsData> getMonthlyDefects(String p, AtlassianHostUser hostUser, int monthsDifference)
			throws Exception {
		ArrayList<MetricsData> data = new ArrayList<MetricsData>();

		// CompletableFuture<MetricsData> m0 = deveffService.getMonthlyDefectsData(p,0,
		// hostUser);

		CompletableFuture<MetricsData> m1 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 1, hostUser);

		CompletableFuture<MetricsData> m2 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 2, hostUser);

		CompletableFuture<MetricsData> m3 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 3, hostUser);

		CompletableFuture<MetricsData> m4 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 4, hostUser);

		CompletableFuture.allOf(m1, m2, m3, m4);
		// CompletableFuture.allOf(m1,m3);
		// data.add(m0.get());
		data.add(m1.get());
		data.add(m2.get());
		data.add(m3.get());
		data.add(m4.get());

		return data;
	}
	
	
	public ArrayList<MetricsData> getMonthlyDefectsAsync(String p, AtlassianHostUser hostUser, int monthsDifference)
			throws Exception {
		ArrayList<MetricsData> data = new ArrayList<MetricsData>();

		// CompletableFuture<MetricsData> m0 = deveffService.getMonthlyDefectsData(p,0,
		// hostUser);

		CompletableFuture<MetricsData> m1 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 1, hostUser);

		CompletableFuture<MetricsData> m2 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 2, hostUser);

		CompletableFuture<MetricsData> m3 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 3, hostUser);

		CompletableFuture<MetricsData> m4 = deveffService.getMonthlyDefectsData(p, -monthsDifference - 4, hostUser);
		
		CompletableFuture.allOf(m1, m2, m3, m4);
		/*CompletableFuture.allOf(m1,m2);*/
		/*CompletableFuture.allOf(m4);*/
		// data.add(m0.get());
		data.add(m1.get());
		data.add(m2.get());
		data.add(m3.get());
		data.add(m4.get());
		return data;
	}
	
	
	public ArrayList<TestCase> getMonthlyTestMetrics(String p, AtlassianHostUser hostUser,int monthsDifference, ArrayList<MetricsData> performanceList)
			throws Exception {  
			ArrayList<TestCase> testCase = new ArrayList<TestCase>();
		
			//CompletableFuture<TestCase> month_0 = testcaseMetricsService.getTestingMetrics(p,0, hostUser,performanceList);
			
			CompletableFuture<TestCase> month_1 = testcaseMetricsService.getTestingMetrics(p,-monthsDifference-1, hostUser,performanceList);
			
			CompletableFuture<TestCase> month_2 = testcaseMetricsService.getTestingMetrics(p,-monthsDifference-2, hostUser,performanceList);
			
			CompletableFuture<TestCase> month_3 = testcaseMetricsService.getTestingMetrics(p,-monthsDifference-3, hostUser,performanceList);
			
			CompletableFuture<TestCase> month_4 = testcaseMetricsService.getTestingMetrics(p,-monthsDifference-4, hostUser,performanceList);
			
			
		//CompletableFuture.allOf(month_0,month_1,month_2,month_3);
			CompletableFuture.allOf(month_1,month_2,month_3,month_4);
			//testCase.add(month_0.get());
			testCase.add(month_1.get());
			testCase.add(month_2.get());
			testCase.add(month_3.get());
			testCase.add(month_4.get());
			
			return testCase;
		}

}
