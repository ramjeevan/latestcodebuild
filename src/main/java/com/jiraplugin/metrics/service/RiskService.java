package com.jiraplugin.metrics.service;

import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskRangeData;
import com.jiraplugin.metrics.model.RiskRating;

@Service
public class RiskService {

	@Autowired
	private AtlassianHostRestClients restClients;

	public JSONArray riskOpenIssues(String pro) throws JSONException {

		String riskassess = "Risk Assessment";
		String sta = "Risk Identified";
		String inp = "Risk Treatment Action / Control";

		String treated = "Treated";

		String na = "Not Applicable";

		String urlString = "/rest/api/2/search?jql=project={pro} AND issuetype={riskassess} AND (status= {sta} OR status= {inp}) &startAt={startAt}&maxResults=100";

		JSONArray jsonArray = new JSONArray();
		int startAt = 0, total = 1;
		while (total > startAt) {
			String response = restClients.authenticatedAsHostActor().getForObject(
					UriComponentsBuilder.fromUriString(urlString).buildAndExpand("\"" + pro + "\"",
							"\"" + riskassess + "\"", "\"" + sta + "\"", "\"" + inp + "\"", startAt).toUri(),
					String.class);

			JSONObject jsonObj1 = new JSONObject(response);
			total = jsonObj1.getInt("total");
			JSONArray jsonArray1 = jsonObj1.getJSONArray("issues");
			int length = jsonArray1.length();
			for (int j = 0; j < length; j++) {
				JSONObject objects = jsonArray1.getJSONObject(j);
				jsonArray.put(objects);
			}
			startAt = startAt + length;
		}

		return jsonArray;
	}

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Async
	public CompletableFuture<ArrayList<ResultRangeData>> getProjectRangeData(String project, AtlassianHostUser hostUser,
			String promain) throws JSONException, java.text.ParseException {
		String rValue = "", residualRValue = "";

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		String url = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={project} AND issuetype='Exposure-Rating' AND 'Client Name' ~ {promain} AND 'Reported Date'>=startOfMonth(-4)&'Reported Date'!=startOfMonth(-1)&startAt=0&maxResults=100";
		String response = restTemplate.getForObject(UriComponentsBuilder.fromUriString(url)
				.buildAndExpand("\"" + project + "\"", "\"" + promain + "\"").toUri(), String.class);
		JSONObject jsonObj = new JSONObject(response);
		JSONArray jsonArray = jsonObj.getJSONArray("issues");
		int count = jsonObj.getInt("total");
		int maxResults = jsonObj.getInt("maxResults");
		if (count > maxResults) {
			url = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={project} AND issuetype='Exposure-Rating' AND 'Client Name' ~ {promain} AND 'Reported Date'>=startOfMonth(-4)&'Reported Date'!=startOfMonth(-1)&startAt=100&maxResults=100";
			String response1 = restTemplate.getForObject(UriComponentsBuilder.fromUriString(url)
					.buildAndExpand("\"" + project + "\"", "\"" + promain + "\"").toUri(), String.class);
			JSONObject jsonObj1 = new JSONObject(response1);
			JSONArray jsonArray1 = jsonObj1.getJSONArray("issues");
			for (int i = 0; i < jsonArray1.length(); i++) {
				JSONObject objects = jsonArray1.getJSONObject(i);
				jsonArray.put(objects);
			}
		}
		ArrayList<RiskRangeData> hdfc = new ArrayList<RiskRangeData>();
		RiskRating fielData = new RiskRating();
		for (int i = 0; i < jsonArray.length(); i++) {
			RiskRangeData data = new RiskRangeData();
			JSONObject issuesData = jsonArray.getJSONObject(i);
			JSONObject fieldsData = issuesData.getJSONObject("fields");
			Date monthandyear = new SimpleDateFormat("yyyy-MM")
					.parse(fieldsData.getString(fieldList.get("Reported Date")));
			String month = new SimpleDateFormat("MMM-yy").format(monthandyear);
			data.setCreated(month);
			int riskValue = 0, residualValue = 0;
			try {
				riskValue = fieldsData.getInt(fieldList.get("Risk Value"));
				data.setRiskValue(riskValue);
			} catch (Exception e) {
				data.setRiskValue(0);
			}
			try {
				residualValue = fieldsData.getInt(fieldList.get("Residual Risk Value"));
				data.setResidualRiskValue(residualValue);
			} catch (Exception e) {
				data.setResidualRiskValue(0);
			}
			System.out.println(data.getCreated());
			System.out.println(data.getResidualRiskValue());
			System.out.println(data.getRiskValue());
			System.out.println(i);
			System.out.println("ïiiiiiiiiiiiii*****************************************************************");
			hdfc.add(data);
		}

		fielData.setFields(hdfc);
		ArrayList<ResultRangeData> result = new ArrayList<ResultRangeData>();
		result = calAverage(fielData);
		return CompletableFuture.completedFuture(result);

	}

	public ArrayList<ResultRangeData> calAverage(RiskRating data) {
		ArrayList<ResultRangeData> result = new ArrayList<>();
		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {
			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;
		}
		float riskSum = 0, residualSum = 0, count = 0, avgRisk = 0, avgResidual = 0;
		String month;
		ArrayList<RiskRangeData> fieldsData = new ArrayList<RiskRangeData>();
		fieldsData = data.getFields();

		for (int j = 0; j < 4; j++) {
			ResultRangeData avgresult = new ResultRangeData();
			month = months[j];
			for (RiskRangeData i : fieldsData) {
				if (month.equals(i.getCreated())) {
					residualSum += i.getResidualRiskValue();
					riskSum += i.getRiskValue();
					count += 1;
				}
			}
			if (count > 0) {
				avgRisk = riskSum / count;
				avgResidual = residualSum / count;
			} else {
				avgRisk = 0;
				avgResidual = 0;
			}
			count = 0;
			residualSum = 0;
			riskSum = 0;
			avgresult.setMonth(month);
			avgresult.setAvg_risk(riskRating(Math.round(avgRisk)));
			avgresult.setAvg_residual(riskRating(Math.round(avgResidual)));
			avgresult.setAvg_residual_value(bucketValueRange(Math.round(avgResidual)));
			avgresult.setAvg_risk_value(bucketValueRange(Math.round(avgRisk)));
			result.add(avgresult);
		}
		System.out.println(result);
		return result;
	}

	public int riskRating(int value) {
		int rating = 0;
		if ((value < 1) || (value > 36)) {
			rating = 0;
		} else if ((value >= 1) && (value <= 4)) {
			rating = 1;
		} else if ((value >= 5) && (value <= 9)) {
			rating = 2;
		} else if ((value >= 10) && (value <= 17)) {
			rating = 3;
		} else if ((value >= 18) && (value <= 28)) {
			rating = 4;
		} else if ((value >= 29) && (value <= 36)) {
			rating = 5;
		}
		return rating;
	}

	public String bucketValueRange(int value) {
		String range = "";
		if ((value < 1) || (value > 36)) {
			range = "-";
		} else if ((value >= 1) && (value <= 4)) {
			range = "1-Low";
		} else if ((value >= 5) && (value <= 9)) {
			range = "2-Medium";
		} else if ((value >= 10) && (value <= 17)) {
			range = "3-High";
		} else if ((value >= 18) && (value <= 28)) {
			range = "4-Very High";
		} else if ((value >= 29) && (value <= 36)) {
			range = "5-Critical";
		}
		return range;
	}

}
