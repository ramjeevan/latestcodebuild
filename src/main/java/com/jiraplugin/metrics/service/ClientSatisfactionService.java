package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;

@Service
public class ClientSatisfactionService {

	public static Map<String, String> yearMap = new LinkedHashMap<String, String>();
	public static Map<String, String> yearMap1 = new LinkedHashMap<String, String>();

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private ClientSatisfactionThread cst;

	@Value("${UserName}")
	String userName;

	@Value("${PassWord}")
	String password;

	@Value("${Domian_Url}")
	String Domain_URL;

	@Value("${corrective_improvement_actions}")
	String Complaint_IssueType;

	@Value("${client_complaints}")
	String Category;

	@Value("${PeriodJanS}")
	String JanPS;

	@Value("${PeriodJanE}")
	String JanPE;

	@Value("${PeriodMayS}")
	String MayPS;

	@Value("${PeriodMayE}")
	String MayPE;

	@Value("${PeriodSepS}")
	String SepPS;

	@Value("${PeriodSepE}")
	String SepPE;

	public Map<String, ArrayList<ComplaintData>> getComplaint(AtlassianHostUser hostUser, String selectedProject)
			throws ParseException, Exception {

		Map<String, ArrayList<ComplaintData>> resultArray = new HashMap<String, ArrayList<ComplaintData>>();

		ArrayList<ArrayList<String>> callData = getFrontBackNumber();

		CompletableFuture<ArrayList<ComplaintData>> month_0 = cst.findCount(hostUser, selectedProject,
				callData.get(0).get(0).toString(), callData.get(0).get(1).toString());

		CompletableFuture<ArrayList<ComplaintData>> month_1 = cst.findCount(hostUser, selectedProject,
				callData.get(1).get(0).toString(), callData.get(1).get(1).toString());

		CompletableFuture<ArrayList<ComplaintData>> month_2 = cst.findCount(hostUser, selectedProject,
				callData.get(2).get(0).toString(), callData.get(2).get(1).toString());

		CompletableFuture<ArrayList<ComplaintData>> month_3 = cst.findCount(hostUser, selectedProject,
				callData.get(3).get(0).toString(), callData.get(3).get(1).toString());

		CompletableFuture.allOf(month_0, month_1, month_2, month_3).join();

		ArrayList<ArrayList<ComplaintData>> resultString = new ArrayList<ArrayList<ComplaintData>>();
		resultString.add(month_0.get());
		resultString.add(month_1.get());
		resultString.add(month_2.get());
		resultString.add(month_3.get());
		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");

		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;

	}

	ArrayList<ArrayList<String>> getFrontBackNumber() {

		Calendar cal;

		cal = Calendar.getInstance();

		int monthCount = cal.get(Calendar.MONTH);

		int yearNumber = cal.get(Calendar.YEAR);

		ArrayList<ArrayList<String>> CollectiveData = new ArrayList<ArrayList<String>>();
		ArrayList<String> resultData;

		if (monthCount >= 8 && monthCount <= 11) {

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + SepPS);
			resultData.add(yearNumber + SepPE);
			resultData.add("Sep-Dec(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + MayPS);

			resultData.add(yearNumber + MayPE);
			resultData.add("May-Aug(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + JanPS);
			resultData.add(yearNumber + JanPE);
			resultData.add("Jan-Apr(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add((yearNumber - 1) + SepPS);
			resultData.add((yearNumber - 1) + SepPE);
			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);

		} else if (monthCount >= 4 && monthCount <= 7) {

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + MayPS);
			resultData.add(yearNumber + MayPE);
			resultData.add("May-Aug(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + JanPS);
			resultData.add(yearNumber + JanPE);
			resultData.add("Jan-Apr(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add((yearNumber - 1) + SepPS);
			resultData.add((yearNumber - 1) + SepPE);
			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add((yearNumber - 1) + MayPS);
			resultData.add((yearNumber - 1) + MayPE);
			resultData.add("May-Aug(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);

		} else if (monthCount >= 0 && monthCount <= 3) {

			resultData = new ArrayList<String>();

			resultData.add(yearNumber + JanPS);

			resultData.add(yearNumber + JanPE);
			resultData.add("Jan-Apr(" + yearNumber + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add((yearNumber - 1) + SepPS);
			resultData.add((yearNumber - 1) + SepPE);
			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();

			resultData.add((yearNumber - 1) + MayPS);

			resultData.add((yearNumber - 1) + MayPE);
			resultData.add("May-Aug(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);

			resultData = new ArrayList<String>();
			resultData.add((yearNumber - 1) + JanPS);
			resultData.add((yearNumber - 1) + JanPE);
			resultData.add("Jan-Apr(" + (yearNumber - 1) + ")");
			CollectiveData.add(resultData);
		}

		return CollectiveData;
	}

	// Client Appreciations

	public Map<String, ArrayList<AppreciationData>> getAppreciation(AtlassianHostUser hostUser, String selectedProject)
			throws Exception, Exception {

		Map<String, ArrayList<AppreciationData>> resultArray = new HashMap<String, ArrayList<AppreciationData>>();

		ArrayList<ArrayList<String>> callData = getFrontBackNumber();

		CompletableFuture<ArrayList<AppreciationData>> month_0 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(0).get(0).toString(), callData.get(0).get(1).toString());

		CompletableFuture<ArrayList<AppreciationData>> month_1 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(1).get(0).toString(), callData.get(1).get(1).toString());

		CompletableFuture<ArrayList<AppreciationData>> month_2 = cst.findCountOfAppreciation(hostUser, selectedProject,
				callData.get(2).get(0).toString(), callData.get(2).get(1).toString());

		CompletableFuture<ArrayList<AppreciationData>> month_3 = cst.findCountOfAppreciation(hostUser, selectedProject,

				callData.get(3).get(0).toString(), callData.get(3).get(1).toString());

		CompletableFuture.allOf(month_0, month_1, month_2, month_3).join();

		ArrayList<ArrayList<AppreciationData>> resultString = new ArrayList<ArrayList<AppreciationData>>();

		resultString.add(month_0.get());
		resultString.add(month_1.get());
		resultString.add(month_2.get());
		resultString.add(month_3.get());

		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;
	}

	@Value("${ClientSatisfactionSingle_IssueType}")
	String ClientSatisfaction_IT;

	@Value("${Quarter_Start_Date}")
	String qsd;

	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

	ArrayList<ClientSatisfactionData> CSATdata;

	ArrayList<ArrayList<String>> dateYearLink;

	ArrayList<String> dateLink;

	public CompletableFuture<Map<String, ClientSatisfactionData>> findCountClientSatisfactionSingleCall(
			AtlassianHostUser hostUser, String sce) throws InterruptedException, Exception, ParseException {

		Calendar cal;

		DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");

		RestTemplate restTemplate = new RestTemplate();

		cal = Calendar.getInstance();

		int monthCount = cal.get(Calendar.MONTH);

		int yearNumber = cal.get(Calendar.YEAR);

		System.out.println("Start of Code Service..!!! " + df.format(cal.getTime()));

		ArrayList<String> resultData = new ArrayList<String>();

		dateYearLink = new ArrayList<ArrayList<String>>();

		dateLink = new ArrayList<String>();

		if (monthCount >= 0 && monthCount <= 3) {

			resultData.add("Jan-Apr(" + yearNumber + ")");
			dateLink.add("Q1(Jan-Apr)");
			dateLink.add(String.valueOf(yearNumber));
			dateYearLink.add(dateLink);

			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
			dateLink.add("Q3(Sep-Dec)");
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);

			resultData.add("May-Aug(" + (yearNumber - 1) + ")");
			dateLink.add("Q2(May-Aug)");
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);

			resultData.add("Jan-Apr(" + (yearNumber - 1) + ")");
			dateLink.add("Q1(Jan-Apr)");
			dateLink.add(String.valueOf(yearNumber - 1));
			dateYearLink.add(dateLink);

			String[] dateLink1 = { "Q1(Jan-Apr)", "Q3(Sep-Dec)", "Q2(May-Aug)", "Q1(Jan-Apr)" };

			for (int i = 0; i < 4; i++) {
				dateLink = new ArrayList<String>();
				dateLink.add(dateLink1[i]);

				if (i == 0) {
					dateLink.add(String.valueOf(yearNumber));
				} else {
					dateLink.add(String.valueOf(yearNumber - 1));
				}
				dateYearLink.add(dateLink);
			}

		} else if (monthCount >= 4 && monthCount <= 7) {

			resultData.add("May-Aug(" + yearNumber + ")");
			resultData.add("Jan-Apr(" + (yearNumber) + ")");
			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
			resultData.add("May-Aug(" + (yearNumber - 1) + ")");

			String[] dateLink1 = { "Q2(May-Aug)", "Q1(Jan-Apr)", "Q3(Sep-Dec)", "Q2(May-Aug)" };

			for (int i = 0; i < 4; i++) {
				dateLink = new ArrayList<String>();
				dateLink.add(dateLink1[i]);

				if (i >= 2) {
					dateLink.add(String.valueOf(yearNumber - 1));
				} else {
					dateLink.add(String.valueOf(yearNumber));
				}
				dateYearLink.add(dateLink);
			}

		} else if (monthCount >= 8 && monthCount <= 11) {

			resultData.add("Sep-Dec(" + yearNumber + ")");
			resultData.add("May-Aug(" + (yearNumber) + ")");
			resultData.add("Jan-Apr(" + (yearNumber) + ")");
			resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");

			String[] dateLink1 = { "Q3(Sep-Dec)", "Q2(May-Aug)", "Q1(Jan-Apr)", "Q3(Sep-Dec)" };

			for (int i = 0; i < 4; i++) {
				dateLink = new ArrayList<String>();
				dateLink.add(dateLink1[i]);
				if (i == 3) {
					dateLink.add(String.valueOf(yearNumber - 1));
				} else {
					dateLink.add(String.valueOf(yearNumber));
				}
				dateYearLink.add(dateLink);
			}

		}

		Map<String, ClientSatisfactionData> resultArray = new HashMap<String, ClientSatisfactionData>();

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName + "@senecaglobal.com", password));

		String urlString = Domain_URL;
		String urlPara = "/rest/api/2/search?jql=project={pro} AND issuetype={csat} AND ({qsd} = 'Q1(Jan-Apr)' OR {qsd} = 'Q2(May-Aug)' OR {qsd} = 'Q3(Sep-Dec)') AND 'Year' ~ {YearNumber} & startAt= {startAtCount} & maxResults=100";
		String domain = urlString + urlPara;

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		int initialTotal = 0;
		int count = 1;
		int startAtCount = 0;

		while (initialTotal < count) {

			String response2 = restClients.authenticatedAs(hostUser)
					.getForObject(
							UriComponentsBuilder.fromUriString(urlPara)
									.buildAndExpand("\"" + sce + "\"", "\"" + ClientSatisfaction_IT + "\"",
											"\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + qsd + "\"",
											"\"" + yearNumber + "\"", "\"" + startAtCount + "\"")
									.toUri(),
							String.class);

			JSONObject response = new JSONObject(response2);
			count = response.getInt("total");
			int issueMaxResult = response.getInt("maxResults");
			System.out.println(count + issueMaxResult);

			parseMethod2(hostUser, response);

			initialTotal = initialTotal + issueMaxResult;

		}

		for (int i = 0; i < 4; i++) {

			int flag = 0;
			int lastElement = 0;

			for (int j = 0; j < CSATdata.size(); j++) {
				lastElement++;

				try {

					if (dateYearLink.get(i).get(0).equals(CSATdata.get(j).getQuarter_Start_Date())
							&& dateYearLink.get(i).get(1).equals(CSATdata.get(j).getYear())) {

						flag = 1;

						resultArray.put(resultData.get(i), CSATdata.get(j));

						break;
					}

				} catch (Exception e) {
					System.out.println(e);

				}

			}
			if (flag == 0) {

				clientSatisfactionData = new ClientSatisfactionData();

				clientSatisfactionData.setCSAT_Q6("-");
				CSATdata.add(clientSatisfactionData);
				resultArray.put(resultData.get(i), CSATdata.get(lastElement));

			}

		}

		cal = Calendar.getInstance();

		System.out.println("End of Code Service..!!! " + df.format(cal.getTime()));

		return CompletableFuture.completedFuture(resultArray);
	}

	@Value("${CSAT_Value_Q1}")
	String CSAT_Q1;
	@Value("${CSAT_Value_Q2}")
	String CSAT_Q2;
	@Value("${CSAT_Value_Q3}")
	String CSAT_Q3;
	@Value("${CSAT_Value_Q4}")
	String CSAT_Q4;
	@Value("${CSAT_Value_Q5}")
	String CSAT_Q5;
	@Value("${CSAT_Value_Q6}")
	String CSAT_Q6;
	@Value("${CSAT_Value_Q7}")
	String CSAT_Q7;
	@Value("${CSAT_Value_Q8}")
	String CSAT_Q8;

	ClientSatisfactionData clientSatisfactionData;

	void parseMethod2(AtlassianHostUser hostUser, JSONObject response) throws Exception, ParseException {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		CSATdata = new ArrayList<ClientSatisfactionData>();
		String cn = fieldList.get("Client Name");
		String sd = fieldList.get("Sent Date");
		String rd = fieldList.get("Received Date");
		String qsd = fieldList.get("Quarter");
		String qed = fieldList.get("Year");
		String qa1 = fieldList.get(CSAT_Q1);
		String qa2 = fieldList.get(CSAT_Q2);
		String qa3 = fieldList.get(CSAT_Q3);
		String qa4 = fieldList.get(CSAT_Q4);
		String qa5 = fieldList.get(CSAT_Q5);
		String qa6 = fieldList.get(CSAT_Q6);
		String qa7 = fieldList.get(CSAT_Q7);
		String qa8 = fieldList.get(CSAT_Q8);

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			clientSatisfactionData = new ClientSatisfactionData();
			JSONObject fields = j.getJSONObject("fields");

			clientSatisfactionData.setClient_Name(fields.get(cn).toString());

			Date date1 = formatter.parse(fields.get(sd).toString());
			String finalDate1 = newFormat.format(date1);
			clientSatisfactionData.setSent_Date(finalDate1);

			Date date2 = formatter.parse(fields.get(rd).toString());
			String finalDate2 = newFormat.format(date2);
			clientSatisfactionData.setReceived_Date(finalDate2);

			/*
			 * Date date3 = formatter.parse(fields.get(qsd).toString()); String finalDate3 =
			 * newFormat.format(date3);
			 * clientSatisfactionData.setQuarter_Start_Date(finalDate3);
			 */
			clientSatisfactionData.setQuarter_Start_Date(fields.getJSONObject(qsd).getString("value").toString());

			/*
			 * Date date4 = formatter.parse(fields.get(qed).toString()); String finalDate4 =
			 * newFormat.format(date4);
			 * clientSatisfactionData.setQuarter_End_Date(finalDate4);
			 */

			clientSatisfactionData.setYear(fields.get(qed).toString());

			clientSatisfactionData.setCSAT_Q1(fields.getJSONObject(qa1).getString("value").toString());
			clientSatisfactionData.setCSAT_Q2(fields.getJSONObject(qa2).getString("value").toString());
			clientSatisfactionData.setCSAT_Q3(fields.getJSONObject(qa3).getString("value").toString());
			clientSatisfactionData.setCSAT_Q4(fields.getJSONObject(qa4).getString("value").toString());
			clientSatisfactionData.setCSAT_Q5(fields.getJSONObject(qa5).getString("value").toString());
			clientSatisfactionData.setCSAT_Q6(fields.getJSONObject(qa6).getString("value").toString());
			clientSatisfactionData.setCSAT_Q7(fields.get(qa7).toString());
			clientSatisfactionData.setCSAT_Q8(fields.get(qa8).toString());
			
				if (clientSatisfactionData.getCSAT_Q1().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q1("-");
							} else if (clientSatisfactionData.getCSAT_Q2().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q2("-");
							} else if (clientSatisfactionData.getCSAT_Q3().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q3("-");
							} else if (clientSatisfactionData.getCSAT_Q4().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q4("-");
							} else if (clientSatisfactionData.getCSAT_Q5().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q5("-");
							} else if (clientSatisfactionData.getCSAT_Q6().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q6("-");
							} else if (clientSatisfactionData.getCSAT_Q7().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q7("-");
							} else if (clientSatisfactionData.getCSAT_Q8().contentEquals("null")) {

								clientSatisfactionData.setCSAT_Q8("-");
							}

			

			CSATdata.add(clientSatisfactionData);

		}

	}

	public Map<String, ArrayList<ClientSatisfactionFA>> getCSFA(AtlassianHostUser hostUser, String selectedProject)
			throws Exception, Exception {

		Map<String, ArrayList<ClientSatisfactionFA>> resultArray = new HashMap<String, ArrayList<ClientSatisfactionFA>>();

		ArrayList<ArrayList<String>> callData = getFrontBackNumber();

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month_0 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(0).get(0).toString(), callData.get(0).get(1).toString());

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month_1 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(1).get(0).toString(), callData.get(1).get(1).toString());

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month_2 = cst.findCountCSFA(hostUser, selectedProject,
				callData.get(2).get(0).toString(), callData.get(2).get(1).toString());

		CompletableFuture<ArrayList<ClientSatisfactionFA>> month_3 = cst.findCountCSFA(hostUser, selectedProject,

				callData.get(3).get(0).toString(), callData.get(3).get(1).toString());

		CompletableFuture.allOf(month_0, month_1, month_2, month_3).join();

		ArrayList<ArrayList<ClientSatisfactionFA>> resultString = new ArrayList<ArrayList<ClientSatisfactionFA>>();

		resultString.add(month_0.get());
		resultString.add(month_1.get());
		resultString.add(month_2.get());
		resultString.add(month_3.get());

		for (int i = 0; i < 4; i++) {

			resultArray.put(callData.get(i).get(2), resultString.get(i));
		}

		return resultArray;

	}
	public Map<String, CIDIData> getCidiMethod(AtlassianHostUser hostUser, String selectedProject)
			throws Exception, ParseException, Exception {

		Map<String, CIDIData> resultArray = new HashMap<String, CIDIData>();

		CompletableFuture<CIDIData> month_0 = cst.findCIDICount(hostUser, selectedProject, -1);

		CompletableFuture<CIDIData> month_1 = cst.findCIDICount(hostUser, selectedProject, -2);

		CompletableFuture<CIDIData> month_2 = cst.findCIDICount(hostUser, selectedProject, -3);

		CompletableFuture<CIDIData> month_3 = cst.findCIDICount(hostUser, selectedProject, -4);

		CompletableFuture.allOf(month_0, month_1, month_2, month_3).join();

		ArrayList<CIDIData> resultString = new ArrayList<CIDIData>();

		resultString.add(month_0.get());
		resultString.add(month_1.get());
		resultString.add(month_2.get());
		resultString.add(month_3.get());

		String months[] = new String[4];
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		for (int i = 1, j = 0; i <= 4; i++, j++) {

			String month = (YearMonth.now().minusMonths(i)).format(formatter);
			months[j] = month;

			resultArray.put(months[j], resultString.get(j));

		}

		return resultArray;
	}
}
