package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CumulativeData;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.EffectivenessData;

@Service
public class DEMonthsThread {
	@Autowired DeffectThread t;
	public ArrayList<DEffectivenessData> runMonthsThreads(String p,@AuthenticationPrincipal AtlassianHostUser hostUser) throws Exception {   
		ArrayList<DEffectivenessData> data = new ArrayList<DEffectivenessData>();
		CompletableFuture<DEffectivenessData> t1 = t.getDefectsCount(p,-1, hostUser);
		
		CompletableFuture<DEffectivenessData> t2 = t.getDefectsCount(p,-2, hostUser);
		
		CompletableFuture<DEffectivenessData> t3 = t.getDefectsCount(p,-3, hostUser);
		
		CompletableFuture<DEffectivenessData> t4 = t.getDefectsCount(p,-4, hostUser);
		
		
		CompletableFuture.allOf(t1,t2,t3,t4).join();
		data.add(t1.get());
		data.add(t2.get());
		data.add(t3.get());
		data.add(t4.get());
		
		return data;
	}
}
