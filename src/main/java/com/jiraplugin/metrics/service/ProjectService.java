package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.FieldCustomID;
import com.jiraplugin.metrics.model.ProjectData;
import com.jiraplugin.metrics.model.ProjectsData;

@Service
public class ProjectService {

	public static int FiledCutFlag = 0;
	public static Map<String, String> customFieldIDMap = new LinkedHashMap<String, String>();


	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	private AtlassianHostRestClients restClients;

	@Autowired
	private JiraCustomFieldMap jiraCust;

	public ArrayList<String> getProjects(AtlassianHostUser hostUser) {

		ProjectsData[] response = restClients.authenticatedAs(hostUser).getForObject("/rest/api/2/project",
				ProjectsData[].class);

		System.out.println(response);
                ArrayList<String> list =new ArrayList<String>();
		List<String> plist = new ArrayList<String>();
		for (int i = 0; i < response.length; i++) {

			plist = Arrays.asList(response[i].getName());
			list.addAll(plist);
		}

		list.remove("COMMON RISKS (DELIVERY)");
		Collections.sort(plist);
		System.out.println("service" + list);

	

		return list;

	}

	public Map<String, String> getProjectsKeys() {

		ProjectsData[] response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		System.out.println("service" + projectkeysMap);
		return projectkeysMap;

	}
	
	public Map<String, String> getProjectsKeysAsync(AtlassianHostUser hostUser) {

		ProjectsData[] response = restClients.authenticatedAs(hostUser).getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		System.out.println("service" + projectkeysMap);
		return projectkeysMap;

	}
	

	@Async
	public CompletableFuture<Integer> getFieldKeys(AtlassianHostUser hostUser) {

		String FieldJsonURL = "/rest/api/2/field";

		FieldCustomID[] Result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,
				FieldCustomID[].class);

		for (int i = 0; i < Result.length; i++) {

			customFieldIDMap.put(Result[i].getName(), Result[i].getKey());

		}

		FiledCutFlag = 1;

		return CompletableFuture.completedFuture(Integer.valueOf(1));

	}

	public Map<String, String> getProjectWebhook() {

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		ProjectsData[] response = restTemplate.getForObject("https://seneca-global.atlassian.net/rest/api/2/project",
				ProjectsData[].class);

		Map<String, String> projectkeysMap = new LinkedHashMap<String, String>();

		for (int i = 0; i < response.length; i++) {

			projectkeysMap.put(response[i].getName(), response[i].getKey());

		}
		System.out.println("service" + projectkeysMap);
		return projectkeysMap;

	}

	public Map<String, Integer> getProjectsIds() {

		ProjectsData[] response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project",
				ProjectsData[].class);

		Map<String, Integer> projectIdsMap = new LinkedHashMap<String, Integer>();

		for (int i = 0; i < response.length; i++) {

			projectIdsMap.put(response[i].getName(), Integer.parseInt(response[i].getId()));

		}
		System.out.println("service" + projectIdsMap);
		return projectIdsMap;

	}

	public Map<String, String> getProjectModes() throws JSONException {

		Map<String, String> modes = new LinkedHashMap<String, String>();
		String type;
		String response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project", String.class);
		JSONArray response1 = new JSONArray(response);
		for (int i = 0; i < response1.length(); i++) {
			JSONObject jobj = response1.getJSONObject(i);
			String id = jobj.getString("id");
			try {
				JSONObject pct = jobj.getJSONObject("projectCategory");
				type = pct.getString("name");
			} catch (Exception e) {
				type = "none";
			}
			modes.put(id, type);
		}
		return modes;
	}

	/* @Scheduled(fixedRate = 5000) */
	public Map<Integer, Integer> getBoardIds() throws JSONException {
		String response = restClients.authenticatedAsHostActor().getForObject("/rest/agile/1.0/board", String.class);
		JSONObject jobj = new JSONObject(response);
		JSONArray jarr = jobj.getJSONArray("values");
		Integer projectId;
		Map<Integer, Integer> projectBoardIds = new LinkedHashMap<Integer, Integer>();

		for (int i = 0; i < jarr.length(); i++) {
			JSONObject j = jarr.getJSONObject(i);
			Integer id = j.getInt("id");

			try {
				JSONObject location = j.getJSONObject("location");
				projectId = location.getInt("projectId");
			} catch (Exception e) {
				projectId = 0;
			}
			if (projectBoardIds.containsKey(projectId) == false) {
				projectBoardIds.put(projectId, id);
			}
		}
		return projectBoardIds;
	}

	public Map<Integer, ProjectData> getProjectData() throws JSONException {
		String response = restClients.authenticatedAsHostActor().getForObject("/rest/api/2/project/", String.class);
		JSONArray jarr = new JSONArray(response);
		Map<Integer, ProjectData> projectData = new LinkedHashMap<Integer, ProjectData>();
		for (int i = 0; i < jarr.length(); i++) {
			ProjectData pd = new ProjectData();
			JSONObject j = jarr.getJSONObject(i);
			Integer id = j.getInt("id");
			pd.setName(j.getString("name"));
			JSONObject category = new JSONObject();
			try {
				category = j.getJSONObject("projectCategory");
				pd.setCategory(category.getString("name"));
			} catch (Exception e) {
				pd.setCategory("-");
			}
			if (projectData.containsKey(id) == false) {
				projectData.put(id, pd);
			}
		}
		return projectData;
	}

}
