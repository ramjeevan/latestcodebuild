package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.SelectChoice;

@Controller
public class MonthlyServiceMetrics {

	@Autowired
	CriticalDeliveryRisk criticalDeliveryRisk;

	@Async
	public CompletableFuture<String> fieldModelSeclection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, String projectChosen)
			throws IOException, InterruptedException, ExecutionException {

		Map<String, String> mapFieldsMain = new HashMap<String, String>();
		// String selectedProject = null;
		try {

			mapFieldsMain = criticalDeliveryRisk.riskOpenIssuesMethod(projectChosen, hostUser);
		} catch (Exception e) {
			System.out.println(e);
		}

		model.addAttribute("atpInfo", mapFieldsMain);

		return CompletableFuture.completedFuture(null);

	}

	@Autowired
	ClientSatisfaction clientsat;

	@Async
	public CompletableFuture<String> clientSatisfactionControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, String projectChosen)
			throws IOException, JSONException, ParseException {

	
		int errorFlag = 0;
		String selectedQuarter = "Sep-Dec(2017)";
		ArrayList<String> mainQuarterList = clientsat.getYearQuarters();
		if (projectChosen != null && selectedQuarter != null) {
			Map<String, String> dataString = null, overalCSAT = null;
			try {
				dataString = clientsat.getCSAT(projectChosen, selectedQuarter, hostUser);
				overalCSAT = clientsat.getOveralCSAT(projectChosen, selectedQuarter, hostUser);
				System.out.println(" inspected");
				System.out.println(ReflectionToStringBuilder.toString(overalCSAT));
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;

			}

			model.addAttribute("project_name", projectChosen);
			model.addAttribute("client_name", dataString.get("cn"));
			model.addAttribute("sent_date", dataString.get("sd"));
			model.addAttribute("received_date", dataString.get("rd"));
			model.addAttribute("quarterPeriod", selectedQuarter);
			model.addAttribute("ab1", dataString.get("a1"));
			model.addAttribute("ab2", dataString.get("a2"));
			model.addAttribute("ab3", dataString.get("a3"));
			model.addAttribute("ab4", dataString.get("a4"));

			model.addAttribute("ab5", dataString.get("a5"));
			model.addAttribute("ab6", dataString.get("a6"));
			model.addAttribute("ab7", dataString.get("a7"));
			model.addAttribute("ab8", dataString.get("a8"));

			model.addAttribute("qua0", overalCSAT.get(clientsat.collView.get(0)));
			model.addAttribute("qua1", overalCSAT.get(clientsat.collView.get(1)));
			model.addAttribute("qua2", overalCSAT.get(clientsat.collView.get(2)));
			model.addAttribute("qua3", overalCSAT.get(clientsat.collView.get(3)));

			model.addAttribute("date0", clientsat.collView.get(0));
			model.addAttribute("date1", clientsat.collView.get(1));
			model.addAttribute("date2", clientsat.collView.get(2));
			model.addAttribute("date3", clientsat.collView.get(3));
			System.out.println("yeah started");
			System.out.println(ReflectionToStringBuilder.toString(clientsat.collView.get(0)));
			System.out.println(ReflectionToStringBuilder.toString(clientsat.collView.get(1)));
			System.out.println(ReflectionToStringBuilder.toString(clientsat.collView.get(2)));
			System.out.println(ReflectionToStringBuilder.toString(clientsat.collView.get(3)));
			JSONArray CSAT = new JSONArray();

			for (int i = 0; i < 4; i++) {
				JSONArray j = new JSONArray();
				j.put(clientsat.collView.get(i));
				if ((overalCSAT.get(clientsat.collView.get(i)).equals("-"))) {
					j.put(0);
					j.put("N/A");
				} else if((overalCSAT.get(clientsat.collView.get(i))).equals("1 - Very Dissatisfied")){
					j.put(1);
					j.put("1");
				}
				else if((overalCSAT.get(clientsat.collView.get(i))).equals("2")){
					j.put(2);
					j.put("2");
				}
				else if((overalCSAT.get(clientsat.collView.get(i))).equals("3 - Satisfied")){
					j.put(3);
					j.put("3");
				}
				else if((overalCSAT.get(clientsat.collView.get(i))).equals("4")){
					j.put(4);
					j.put("4");
				}
				else {
					j.put(5);
					j.put("5");
				}
				CSAT.put(j);
			}

			System.out.println("--------------------------"+CSAT);
			model.addAttribute("CSAT", CSAT);
			
		}

		return CompletableFuture.completedFuture(null);

	}

	@Autowired
	private ProjectService projectService;

	@Autowired
	private RiskService rs;

	@Async
	public CompletableFuture<String> deliveryRiskControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model, String projectChosen)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {

		// String project = mychoice.getProjectname();

		String snapProject = "New Project Design";
		Map<String, String> projkeys = projectService.getProjectsKeysAsync(hostUser);
		String pkey2 = projkeys.get(projectChosen);

		int errorFlag = 0;

		CompletableFuture<ArrayList<ResultRangeData>> exposureRatingResults = rs.getProjectRangeData(snapProject,
				hostUser, pkey2);

		ArrayList<ResultRangeData> ratingResults = exposureRatingResults.get();
		JSONArray jsonRatingResults = new JSONArray(ratingResults);

		JSONArray riskRatingLine = new JSONArray();


		for (int i = 0; i < jsonRatingResults.length(); i++) {
			JSONArray j = new JSONArray();
			j.put(((JSONObject) jsonRatingResults.get(i)).getString("month"));
			j.put(((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk"));
			if((((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk"))==0){ 
					j.put("N/A"); }
			else j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk")));
			j.put(((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual"));
			if((((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual"))==0){ 
				j.put("N/A"); }
		else j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual")));
			
			riskRatingLine.put(j);
		}
		model.addAttribute("riskRatingLine", riskRatingLine);

		model.addAttribute("project_title", projectChosen);
		model.addAttribute("err", exposureRatingResults.get());

		return CompletableFuture.completedFuture(null);

	}

}
