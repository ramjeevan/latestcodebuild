package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ClientSatisfactionData;

@Service
public class ClientSatisfactionDateService {

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Value("${ClientSatisfactionSingle_IssueType}")
	String clientSatisfactionIT;

	@Value("${Quarter_Start_Date}")
	String qsd;

	@Value("${Client_SAT}")
	String clientSatisfactionFilterUrl;

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	public Map<String, ClientSatisfactionData> getClientRating(AtlassianHostUser hostUser, String projectChosen,
			Map<String, String> userDateRange) {

		ArrayList<ArrayList<String>> dateYearLink = getClientQuarter(userDateRange);

		String actualClientUrl = searchIdentifiedUrl.concat(clientSatisfactionFilterUrl);

		ArrayList<ClientSatisfactionData> CSATdata = new ArrayList<ClientSatisfactionData>();

		ClientSatisfactionData clientSatisfactionData;

		Map<String, ClientSatisfactionData> resultArray = new LinkedHashMap<String, ClientSatisfactionData>();

		Map<String, ClientSatisfactionData> actualResultArray = new HashMap<String, ClientSatisfactionData>();

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		int initialTotal = 0;
		int count = 1;
		int startAtCount = 0;

		while (initialTotal < count) {

			String response2 = restClients.authenticatedAs(hostUser)
					.getForObject(UriComponentsBuilder.fromUriString(actualClientUrl)
							.buildAndExpand("\"" + projectChosen + "\"", "\"" + clientSatisfactionIT + "\"",
									"\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + startAtCount + "\"")
							.toUri(), String.class);

			JSONObject response = new JSONObject(response2);
			count = response.getInt("total");
			int issueMaxResult = response.getInt("maxResults");
			System.out.println(count + issueMaxResult);

			try {
				CSATdata = parseMethod2(hostUser, response);
			} catch (ParseException e) {

				e.printStackTrace();
			} catch (Exception e) {

				e.printStackTrace();
			}

			initialTotal = initialTotal + issueMaxResult;

		}

		for (int i = dateYearLink.size() - 1; i >= 0; i--) {

			int flag = 0;
			int lastElement = 0;

			for (int j = 0; j < CSATdata.size(); j++) {
				lastElement++;

				try {

					if (dateYearLink.get(i).get(0).equals(CSATdata.get(j).getQuarter_Start_Date())
							&& dateYearLink.get(i).get(1).equals(CSATdata.get(j).getYear())) {

						flag = 1;

						resultArray.put(dateYearLink.get(i).get(0) + "-" + dateYearLink.get(i).get(1), CSATdata.get(j));

						break;
					}

				} catch (Exception e) {
					System.out.println(e);

				}

			}
			if (flag == 0) {

				clientSatisfactionData = new ClientSatisfactionData();

				clientSatisfactionData.setCSAT_Q6("-");
				CSATdata.add(clientSatisfactionData);
				resultArray.put(dateYearLink.get(i).get(0) + "-" + dateYearLink.get(i).get(1),
						CSATdata.get(lastElement));

			}

		}

		/*
		 * for(int i = 0; i < dateYearLink.size(); i++) {
		 *
		 * }
		 */
		return resultArray;
	}

	public ArrayList<ArrayList<String>> getClientQuarter(Map<String, String> userDateRange)

	{

		ArrayList<ArrayList<String>> mainQuarters = new ArrayList<>();
		ArrayList<String> quarters;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat dfYY = new SimpleDateFormat("yyyy");

		Map<Integer, String> quaStr = new LinkedHashMap<>();
		quaStr.put(1, "Jan-Apr");
		quaStr.put(2, "May-Aug");
		quaStr.put(3, "Sep-Dec");

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(df.parse(userDateRange.get("d1")));

			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(df.parse(userDateRange.get("d2")));

			while (cal1.getTime().after(cal.getTime()) || cal1.getTime().equals(cal.getTime())) {
				int month = cal.get(Calendar.MONTH) + 1;

				int quarter = month % 4 == 0 ? (month / 4) : (month / 4) + 1;

				quarters = new ArrayList<>();

				quarters.add("Q" + quarter + "(" + quaStr.get(quarter) + ")"); // Q2(May-Aug)
				quarters.add(dfYY.format(cal.getTime()));

				mainQuarters.add(quarters);

				cal.add(Calendar.MONTH, 4);
			}

			System.out.println();

		} catch (Exception e) {

		}

		return mainQuarters;
	}

	@Value("${CSAT_Value_Q1}")
	String CSAT_Q1;
	@Value("${CSAT_Value_Q2}")
	String CSAT_Q2;
	@Value("${CSAT_Value_Q3}")
	String CSAT_Q3;
	@Value("${CSAT_Value_Q4}")
	String CSAT_Q4;
	@Value("${CSAT_Value_Q5}")
	String CSAT_Q5;
	@Value("${CSAT_Value_Q6}")
	String CSAT_Q6;
	@Value("${CSAT_Value_Q7}")
	String CSAT_Q7;
	@Value("${CSAT_Value_Q8}")
	String CSAT_Q8;

	ArrayList<ClientSatisfactionData> parseMethod2(AtlassianHostUser hostUser, JSONObject response)
			throws Exception, ParseException {
		ClientSatisfactionData clientSatisfactionData;
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		ArrayList<ClientSatisfactionData> CSATdata = new ArrayList<>();
		String cn = fieldList.get("Client Name");
		String sd = fieldList.get("Sent Date");
		String rd = fieldList.get("Received Date");
		String quarterValue = fieldList.get("Quarter");
		String qed = fieldList.get("Year");
		String qa1 = fieldList.get(CSAT_Q1);
		String qa2 = fieldList.get(CSAT_Q2);
		String qa3 = fieldList.get(CSAT_Q3);
		String qa4 = fieldList.get(CSAT_Q4);
		String qa5 = fieldList.get(CSAT_Q5);
		String qa6 = fieldList.get(CSAT_Q6);
		String qa7 = fieldList.get(CSAT_Q7);
		String qa8 = fieldList.get(CSAT_Q8);

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			clientSatisfactionData = new ClientSatisfactionData();
			JSONObject fields = j.getJSONObject("fields");

			clientSatisfactionData.setClient_Name(fields.get(cn).toString());

			Date date1 = formatter.parse(fields.get(sd).toString());
			String finalDate1 = newFormat.format(date1);
			clientSatisfactionData.setSent_Date(finalDate1);

			Date date2 = formatter.parse(fields.get(rd).toString());
			String finalDate2 = newFormat.format(date2);
			clientSatisfactionData.setReceived_Date(finalDate2);
			clientSatisfactionData.setQuarter_Start_Date(fields.getJSONObject(quarterValue).getString("value"));
			clientSatisfactionData.setYear(fields.get(qed).toString());
			try {
				clientSatisfactionData.setCSAT_Q1(fields.getJSONObject(qa1).getString("value"));
			} catch (Exception e) {

				clientSatisfactionData.setCSAT_Q1("null");
			}
			try {
				clientSatisfactionData.setCSAT_Q2(fields.getJSONObject(qa2).getString("value"));

			} catch (Exception e) {
				clientSatisfactionData.setCSAT_Q2("null");
			}
			try {

				clientSatisfactionData.setCSAT_Q3(fields.getJSONObject(qa3).getString("value"));

			} catch (Exception e) {
				clientSatisfactionData.setCSAT_Q3("null");
			}
			try {

				clientSatisfactionData.setCSAT_Q4(fields.getJSONObject(qa4).getString("value"));

			} catch (Exception e) {
				clientSatisfactionData.setCSAT_Q4("null");
			}
			try {

				clientSatisfactionData.setCSAT_Q5(fields.getJSONObject(qa5).getString("value"));

			} catch (Exception e) {
				clientSatisfactionData.setCSAT_Q5("null");
			}
			try {

				clientSatisfactionData.setCSAT_Q6(fields.getJSONObject(qa6).getString("value"));

			} catch (Exception e) {
				clientSatisfactionData.setCSAT_Q6("null");
			}

			clientSatisfactionData.setCSAT_Q7(fields.get(qa7).toString());
			clientSatisfactionData.setCSAT_Q8(fields.get(qa8).toString());

			String respondentSkipped = "Respondent skipped this question";

			if (clientSatisfactionData.getCSAT_Q1().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q1(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q2().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q2(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q3().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q3(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q4().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q4(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q5().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q5(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q6().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q6(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q7().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q7(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q8().contentEquals("null")) {
				clientSatisfactionData.setCSAT_Q8(respondentSkipped);
			}

			CSATdata.add(clientSatisfactionData);

		}
		return CSATdata;
	}

}
