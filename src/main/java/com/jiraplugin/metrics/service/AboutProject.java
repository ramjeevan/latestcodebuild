package com.jiraplugin.metrics.service;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class AboutProject {

	@Autowired
	private AtlassianHostRestClients restClients;

	public JSONObject getFieldKeys(String sce, AtlassianHostUser hostUser) throws JSONException {


		String aboutProject = "About The Project";
		String urlString = "/rest/api/2/search?jql=project={pro} AND issuetype={aboutProject}";

		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder
				.fromUriString(urlString).buildAndExpand("\"" + sce + "\"", "\"" + aboutProject + "\"").toUri(),
				String.class);

		JSONObject jsonObj = new JSONObject(response);

		return jsonObj;

	}

}
