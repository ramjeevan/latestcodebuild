package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskManagementData;

@Service
public class RiskManagementService {

	@Value("${search_Identified_Url}")
	String searchIdentifiedUrl;

	@Value("${Domian_Url}")
	String domianUrl;

	@Value("${Snap_Shot_Project}")
	String snapProject;

	@Autowired
	ProjectService projectService;

	@Autowired
	RiskManagementDateRangeService riskDateRangeService;

	public RiskManagementData getRiskRangeData(AtlassianHostUser hostUser, String projectChosen,
			Map<String, String> userDateRange) {

		Map<String, String> projkeys = projectService.getProjectsKeys();

		String pkey2 = projkeys.get(projectChosen);

		CompletableFuture<Map<String, String>> acceptableLevelResults = riskDateRangeService
				.getAcceptablePercentage(hostUser, snapProject, pkey2, userDateRange);

		CompletableFuture<Map<String, String>> criticalDeliveryIdentified = riskDateRangeService
				.getCriticalDeliveryIntified(hostUser, snapProject, pkey2, userDateRange);

		CompletableFuture<Map<String, String>> riskOccurrenceResults = riskDateRangeService
				.getRiskOccurenceDetails(hostUser, pkey2, userDateRange, projectChosen);

		CompletableFuture<ArrayList<ResultRangeData>> exposureRatingResults = riskDateRangeService
				.getExposureRatingValues(hostUser, snapProject, pkey2, userDateRange);

		CompletableFuture
				.allOf(acceptableLevelResults, criticalDeliveryIdentified, riskOccurrenceResults, exposureRatingResults)
				.join();

		RiskManagementData riskManageData = new RiskManagementData();

		try {
			riskManageData.setCriticalRiskCount(criticalDeliveryIdentified.get());
			riskManageData.setRiskAcceptablePercentage(acceptableLevelResults.get());
			riskManageData.setRiskOccurenceCount(riskOccurrenceResults.get());
			riskManageData.setExposureValues(exposureRatingResults.get());
		} catch (Exception e) {
			System.out.println(e);
		}

		return riskManageData;
	}

}
