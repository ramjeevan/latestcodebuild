package com.jiraplugin.metrics.service;
import java.text.ParseException;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.IoData;
import com.jiraplugin.metrics.model.NcData;



@Service
public class NCandIOService {
	@Autowired
	 JiraCustomFieldMap jiraCustomfieldmaps;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Async
	public CompletableFuture<NcData> getNCS(int id,AtlassianHostUser hostUser) throws Exception{
		
			
			String urlString = "/rest/api/2/issue/{id}";
			
			String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(urlString).buildAndExpand(id).toUri(), String.class);
			Map<String,String> customFieldIDMap=jiraCustomfieldmaps.getJiraFieldList(hostUser);
			String NC_Description=customFieldIDMap.get("NC Description");
			String process=customFieldIDMap.get("Applicable Process Title");
			String Appl_Process_Req=customFieldIDMap.get("Applicable Process Requirement");
			String severity=customFieldIDMap.get("NC Severity");
			String evidence=customFieldIDMap.get("Evidence");
			String cause=customFieldIDMap.get("Cause of NC");
			String correction=customFieldIDMap.get("Correction");
			String responsibility_correc=customFieldIDMap.get("Responsibility (Correction)");
			String planned_Closure_Date_correc=customFieldIDMap.get("Planned Closure Date for Correction");
			String actualclosuredatecorrec=customFieldIDMap.get("Actual Closure Date for Correction");
			String corrective_action=customFieldIDMap.get("Corrective Action");
			String resp_corr_action=customFieldIDMap.get("Responsibility (Corrective Action)");
			String planned_Closure_Date_Corr_Action=customFieldIDMap.get("Planned Closure Date for Corrective Action");
			String actual_Closure_Date_Corr_Action=customFieldIDMap.get("Actual Closure Date for Corrective Action");
			String verification_Details=customFieldIDMap.get("Verification Details");
			String verified_by=customFieldIDMap.get("Verified by");
			String verification_Date=customFieldIDMap.get("Verification Date");
			String closure_Remarks=customFieldIDMap.get("Closure Remarks");
			JSONObject jsonObj = new JSONObject(response);
			JSONObject  fieldsData=jsonObj.getJSONObject("fields");
			NcData ncd=new NcData();
			ncd.setDescription(fieldsData.getString(NC_Description));
			ncd.setProcess(fieldsData.getString(process));
		   ncd.setProcessRequirement(fieldsData.getString(Appl_Process_Req));
		
		   JSONObject sev=fieldsData.getJSONObject(severity);
				   ncd.setSeverity( sev.getString("value"));
		ncd.setEvidence(fieldsData.getString(evidence));
		if(!fieldsData.isNull(cause))
			ncd.setCause(fieldsData.getString(cause));
		if(!fieldsData.isNull(correction))
		ncd.setCorrection(fieldsData.getString(correction));

		JSONArray jsonArray = fieldsData.getJSONArray(responsibility_correc);

		String resp = "";
		boolean flag = false;
		for (int k = 0; k < jsonArray.length(); k++) {
			if (flag == true) {
				resp += ",";
			}
			JSONObject jo = jsonArray.getJSONObject(k);
			resp += jo.getString("displayName");
			flag = true;
		}
		ncd.setCorrectionResponsibility(resp);
		if(!fieldsData.isNull(planned_Closure_Date_correc))		
		ncd.setCorrectionPlannedDate(fieldsData.getString(planned_Closure_Date_correc));
		if(!fieldsData.isNull(actualclosuredatecorrec))
		ncd.setCorrectionActualDate(fieldsData.getString(actualclosuredatecorrec));
		if(!fieldsData.isNull(corrective_action))
		ncd.setCorrectiveAction(fieldsData.getString(corrective_action));
		JSONArray jsonArr = fieldsData.getJSONArray(responsibility_correc);
		String respons = "";
		boolean flag1 = false;
		for (int k = 0; k < jsonArr.length(); k++) {
			if (flag1 == true) {
				respons += ",";
			}
			JSONObject jo = jsonArr.getJSONObject(k);
			respons += jo.getString("displayName");
			flag1 = true;
		}
		ncd.setActionResponsibility(respons);
		if(!fieldsData.isNull(planned_Closure_Date_Corr_Action))
	ncd.setActionPlannedDate(fieldsData.getString(planned_Closure_Date_Corr_Action));
		if(!fieldsData.isNull(actual_Closure_Date_Corr_Action))
	ncd.setActionActualDate(fieldsData.getString(actual_Closure_Date_Corr_Action));
		if(!fieldsData.isNull(verification_Details))
	ncd.setVerificaitonDetails(fieldsData.getString(verification_Details));
		if(!fieldsData.isNull(verified_by))
	ncd.setVerifiedBy(fieldsData.getString(verified_by));
		if(!fieldsData.isNull(verification_Date))
	ncd.setVerifiedDate(fieldsData.getString(verification_Date));
	if(!fieldsData.isNull(closure_Remarks))		
	ncd.setClosureRemark(fieldsData.getString(closure_Remarks));
			
			

	System.out.println("nc thread");
		return CompletableFuture.completedFuture(ncd);
		
		
		
	}
	@Async
	public CompletableFuture<IoData> getIOS(int id,AtlassianHostUser hostUser) throws Exception{

		
		String urlString = "/rest/api/2/issue/{id}";
		
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(urlString).buildAndExpand(id).toUri(), String.class);
		Map<String,String> customFieldIDMap=jiraCustomfieldmaps.getJiraFieldList(hostUser);
		String process_Improvement=customFieldIDMap.get("Process Improvement");
		String action_Plan=customFieldIDMap.get("Action Plan");
		String responsibility_io=customFieldIDMap.get("Responsibility (Improvement Opportunities)");
		String target_Date_io=customFieldIDMap.get("Target Date (Improvement Opportunities)");
		String actual_Date_io= customFieldIDMap.get("Actual Date (Improvement Opportunities)");
		String remarks_io=customFieldIDMap.get("Remarks (Improvement Opportunities)");
		JSONObject jsonObj = new JSONObject(response);
		JSONObject  fieldsData=jsonObj.getJSONObject("fields");
		IoData iod=new IoData();
		iod.setProcessImprovements(fieldsData.getString(process_Improvement));
		if(!fieldsData.isNull(action_Plan))
		iod.setActionPlan(fieldsData.getString(action_Plan));
		if(!fieldsData.isNull(responsibility_io))
		iod.setPiresponsibility(fieldsData.getString(responsibility_io));
		if(!fieldsData.isNull(target_Date_io))
		iod.setTargetDate(fieldsData.getString(target_Date_io));
		if(!fieldsData.isNull(actual_Date_io))
		iod.setActualDate(fieldsData.getString(actual_Date_io));
		if(!fieldsData.isNull(remarks_io))
		iod.setRemarks(fieldsData.getString(remarks_io));
		
		System.out.println(" io thread");
		return CompletableFuture.completedFuture(iod);
	}
	}
