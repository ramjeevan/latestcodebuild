package com.jiraplugin.metrics.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.JsonData;

@Service
public class CriticalDeliveryRiskAsyn {

	@Autowired
	private AtlassianHostRestClients restClients;

	@Async
	public CompletableFuture<Integer> findCount(AtlassianHostUser hostUser, int dce, int start, String riskUrl,
			String promain) throws InterruptedException {

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		int result;

		/*
		 * JsonData responseEntity =
		 * restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder
		 * .fromUriString(riskUrl).buildAndExpand("\"" + start + "\"", "\"" + dce +
		 * "\"").toUri(), JsonData.class);
		 */

		JsonData responseEntity = restTemplate.getForObject(
				UriComponentsBuilder.fromUriString(riskUrl)
						.buildAndExpand("\"" + start + "\"", "\"" + dce + "\"", "\"" + promain + "\"").toUri(),
				JsonData.class);

		result = responseEntity.getTotal();

		return CompletableFuture.completedFuture(result);

	}

	@Async
	public CompletableFuture<Integer> findCountClient(AtlassianHostUser hostUser, int dce, String riskUrl)
			throws InterruptedException {

		int result;

		JsonData responseEntity = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder
				.fromUriString(riskUrl).buildAndExpand("\"" + dce + "\"", "\"" + dce + "\"").toUri(), JsonData.class);

		result = responseEntity.getTotal();

		return CompletableFuture.completedFuture(result);

	}

}
