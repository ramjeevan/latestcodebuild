package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.ComplaintData;
import com.jiraplugin.metrics.model.InitiativeData;


@Service
public class ComplaintsService {
	 @Value("${client_complaints}")  String client_complaints;
	 @Value("${corrective_improvement_actions}") String type;
	@Autowired JiraCustomFieldMap j;
	@Autowired ErrorService es;
	@Autowired
	private AtlassianHostRestClients restClients;
	public ArrayList<ComplaintData> getComplaintData(AtlassianHostUser hostUser,String p) throws IOException{
	ArrayList<ComplaintData> data = new ArrayList<ComplaintData>();
	try {
	String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {type}) AND ('Category (Corr./Improv Actions)'= {client_complaints}) AND (status = 'Open' OR status = 'In Progress')";
	String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + type + "\"","\"" + client_complaints + "\"").toUri(), String.class);
	JSONObject response1 = new JSONObject(response);
	int count = response1.getInt("total");
	if(count != 0) {
		data = getParsedData(response1,hostUser);
	}
	}catch(Exception e) {
		es.writeException(e,p);
	}
	return data;
	}
	public ArrayList<ComplaintData> getClosedData(AtlassianHostUser hostUser,String p,int sDate,int eDate) throws IOException{
		ArrayList<ComplaintData> data = new ArrayList<ComplaintData>();
		try {
		String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {type}) AND ('Category (Corr./Improv Actions)'= {client_complaints}) AND (status ='Closed') AND ('Issue Closed Date' >= startOfMonth({sDate}) AND 'Issue Closed Date' <= endOfMonth({eDate}))" ;
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + type + "\"","\"" + client_complaints + "\"", sDate, eDate).toUri(), String.class);
		JSONObject response1 = new JSONObject(response);
		int count = response1.getInt("total");
		if(count != 0) {
			data = getParsedData(response1,hostUser);
		}
		}catch(Exception e) {
			es.writeException(e,p);
		}
		return data;
		}
	
	
	public ArrayList<ComplaintData> getParsedData(JSONObject response,@AuthenticationPrincipal AtlassianHostUser hostUser) throws IOException, JSONException, ParseException{
		Map<String, String> fieldList = j.getJiraFieldList(hostUser);
		ArrayList<ComplaintData> data = new ArrayList<ComplaintData>();
		String rDate = fieldList.get("Reported Date");
		String description = fieldList.get("Issue description");
		String action = fieldList.get("Action");
		String rFrom = fieldList.get("If Client Complaint, Received From");
		JSONArray jsonArray = response.getJSONArray("issues");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
		for(int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
			ComplaintData complaintData = new ComplaintData();
			JSONObject fields = j.getJSONObject("fields");
			Date date = (Date)formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			complaintData.setrDate(finalDate);
			complaintData.setDescription(fields.get(description).toString());
			complaintData.setAction(fields.get(action).toString());
			String recFrom;
			try{
			 recFrom = fields.getString(rFrom).toString();
			}
			catch(Exception e){
			recFrom = "-";
			}
			complaintData.setrFrom(recFrom);
			String obj = fields.get("status").toString();
			JSONObject sta = new JSONObject(obj);
			complaintData.setStatus(sta.get("name").toString());
			data.add(complaintData);
		}
	return data;
}
}
