package com.jiraplugin.metrics.service;

import java.text.DecimalFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.MetricsData;

@Service
public class DevEffectivenessService {

	@Autowired
	private DefectsService defectsService;
	@Autowired
	private MonthlyIssues monthlyIssues;
    @Autowired
    ErrorService es;
	@Async
	public CompletableFuture<MetricsData> getMonthlyDefectsData(String project, int i,  AtlassianHostUser hostUser) throws Exception {
		MetricsData data = new MetricsData();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		String month = (YearMonth.now().minusMonths(Math.abs(i))).format(formatter);
		data.setMonth(month);
	try{
	/*	CompletableFuture<Integer> rdefect = defectsService.defectsCount(p, i, "Review Defect", hostUser);

		CompletableFuture<Integer> tdefect = defectsService.defectsCount(p, i, "Testing Defect", hostUser);

		CompletableFuture<Integer> ddefect = defectsService.defectsCount(p, i, "Delivery Defect", hostUser);*/

		/*CompletableFuture.allOf(rdefect, tdefect, ddefect);
		Integer rdc = rdefect.get();

		Integer tdc = tdefect.get();

		Integer dd = ddefect.get();*/
		
		//CompletableFuture<PerformanceMetrics> defects=defectsService.defectsCount(p, i, hostUser);
	    CompletableFuture<List<Double>> review_effort1=monthlyIssues.getMonthlyIssues(project,i,0,hostUser);
		CompletableFuture<List<Double>> review_effort2=monthlyIssues.getMonthlyIssues(project,i,50,hostUser);
		CompletableFuture<List<Double>> review_effort3=monthlyIssues.getMonthlyIssues(project,i,100,hostUser);
		CompletableFuture<List<Double>> review_effort4=monthlyIssues.getMonthlyIssues(project,i,150,hostUser);
		CompletableFuture<DEffectivenessData> defects=defectsService.defectsCount(project, i, hostUser);
		CompletableFuture.allOf(review_effort1, review_effort2, review_effort3,review_effort4,defects);
		double total_review_effort=review_effort1.get().get(0)+review_effort2.get().get(0)+review_effort3.get().get(0)+review_effort4.get().get(0);
		double review_effort=review_effort1.get().get(1)+review_effort2.get().get(1)+review_effort3.get().get(1)+review_effort4.get().get(1);
		double test_design_review_effort=review_effort1.get().get(2)+review_effort2.get().get(2)+review_effort3.get().get(2)+review_effort4.get().get(2);
		double total_testable=review_effort1.get().get(3)+review_effort2.get().get(3)+review_effort3.get().get(3)+review_effort4.get().get(3);
		double review_efficiency=0;
		double revEffect = 0;
		double reviewEffective_adc=0;
		double testEffect=0;
		double dRE=0;
		DEffectivenessData performanceMetrics=defects.get();
		int reviewdefects_count=performanceMetrics.getRd_adc_count();
		int total_review_defects_count=performanceMetrics.getrTCount();
		int testingdefects_count=performanceMetrics.gettTCount();
		int total_test_design_defects_count=performanceMetrics.getDesign_count();
		int deliverydefects_count=performanceMetrics.getdTCount();
		int total_inprocess_defects=total_review_defects_count+testingdefects_count;
		/*PerformanceMetrics pm=defects.get();
		int dd=pm.getDelivery_defects();
		int rdc=pm.getReview_defects();
		int tdc=pm.getTesting_defects();*/
		int inprocess_defects = reviewdefects_count + testingdefects_count;
		if(inprocess_defects!=0) {
			double reeffnss = ( total_review_defects_count * 100.0) / total_inprocess_defects;
			revEffect = Math.round(reeffnss * 100.0) / 100.0;
			data.setRevef(String.valueOf(revEffect));
			double reviewEffectiveness_adc = ( reviewdefects_count * 100.0) / inprocess_defects;
			reviewEffective_adc = Math.round(reviewEffectiveness_adc * 100.0) / 100.0;
			data.setReview_effectiveness_adc(String.valueOf(reviewEffective_adc));
			double te = ( testingdefects_count * 100.0) / total_inprocess_defects;
			testEffect = Math.round(te * 100.0) / 100.0;
			data.setTesteff(String.valueOf(testEffect));
			double dre = ( total_inprocess_defects * 100.0) / ( total_inprocess_defects + deliverydefects_count);
			dRE = Math.round(dre * 100.0) / 100.0;
			data.setDreff(String.valueOf(dRE));
		}
		else {
		data.setRevef("N/A");
		data.setReview_effectiveness_adc("N/A");
		data.setTesteff("N/A");
		data.setDreff("0");
		data.setReview_efficiency("N/A");
		if(deliverydefects_count==0)
			data.setDreff("N/A");
		}
		if(total_review_effort==0)
			data.setTotal_review_efficiency("N/A");
		else if(total_review_effort==0 && total_review_defects_count==0 )
			data.setTotal_review_efficiency("N/A");
		else
		{
			 review_efficiency = total_review_defects_count/total_review_effort;
			
		data.setTotal_review_efficiency(String.format("%.2f", review_efficiency));
		}
		if(review_effort==0)
			data.setReview_efficiency("N/A");
		else if(review_effort==0 && reviewdefects_count==0 )
			data.setReview_efficiency("N/A");
		else
		{
			 review_efficiency = reviewdefects_count/review_effort;
			
		data.setReview_efficiency(String.format("%.2f", review_efficiency));
		}
		data.setRevd(reviewdefects_count);
		data.setTesd(testingdefects_count);
		data.setDeld(deliverydefects_count);
		data.setTotal_inprocess_defects(total_inprocess_defects);
     data.setTotal_review_defects(total_review_defects_count);
		data.setInpd(inprocess_defects);
		data.setReview_effort(String.valueOf(review_effort));
		
		data.setTotal_review_effort(String.valueOf(total_review_effort));
		data.setTest_design_review_effort(test_design_review_effort);
	        data.setTotal_test_design_defects(total_test_design_defects_count);
	        data.setTotal_testable(total_testable);
		data.setD(performanceMetrics);
	} 
		catch (Exception e) {
		    es.writeException(e, project);
		}
	
			System.out.println(ReflectionToStringBuilder.toString(data));
			System.out.println("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii");
		
		return CompletableFuture.completedFuture(data);
		
	}
}