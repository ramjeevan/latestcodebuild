package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;
import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class MonthlyIssues {
    @Autowired
    private AtlassianHostRestClients restClients;
    @Autowired
    ProjectService jcfm;
    @Autowired
    ErrorService es;
    @Value("${new_requirement}")
    String new_requirement;
    @Value("${change_request}")
    String change_request;
    @Value("${task}")
    String task;
    @Value("${analysis_review_effort}")
    String analysis_review_effort_field;
    @Value("${design_review_effort}")
    String design_review_effort_field;
    @Value("${code_review_effort}")
    String code_review_effort_field;
    @Value("${unit_test_design_review_effort}")
    String unit_test_design_review_effort_field;
    @Value("${review_effort_of_test_cases_written_by_developer}")
    String review_effort_of_test_cases_written_by_developer_field;
    @Value("${review_effort_of_test_cases_written_by_QA}")
    String review_effort_of_test_cases_written_by_QA_field;
    @Value("${is_it_testable}")
    String is_it_testable;

    /*
     * @Value("${review_defect}") String review_defect_nwf;
     * 
     * @Value("${analysis_design_review_defect}") String
     * analysis_design_review_defect_nwf;
     * 
     * @Value("${unit_testing_defect}") String unit_testing_defect_nwf;
     * 
     * @Value("${test_case_review_defect}") String test_case_review_defect_nwf;
     */
    @Async
    public CompletableFuture<List<Double>> getMonthlyIssues(String project, int i, int startAt,
            AtlassianHostUser hostUser) throws Exception {

	double total_analysis_review_effort = 0;
	double total_design_review_effort = 0;
	double total_code_review_effort = 0;
	double total_unit_test_design_review_effort = 0;
	double total_review_effort_of_test_cases_written_by_developer = 0;
	double total_review_effort_of_test_cases_written_by_QA = 0;
	double total_review_effort = 0;
	int total_testable=0;
	double review_effort = 0;
	List<Double> review_effort_list = new ArrayList<Double>(); // total_review_effort,review_effort,testing_review_effort
	String url = "/rest/api/2/search?jql=project={p} AND ( issuetype={new_requirement} OR issuetype={change_request} OR issuetype={task} ) AND created>=startOfMonth({i}) AND created<=endOfMonth({i})&startAt={startAt}&maxResults=50";
	String response = restClients.authenticatedAs(hostUser)
	        .getForObject(
	                UriComponentsBuilder.fromUriString(url)
	                        .buildAndExpand("\"" + project + "\"", "\"" + new_requirement + "\"",
	                                "\"" + change_request + "\"", "\"" + task + "\"", i, i, startAt)
	                        .toUri(),
	                String.class);
	Map<String, String> map = jcfm.customFieldIDMap;
	JSONObject jsonObject = new JSONObject(response);

	try {

	    JSONArray jsonArray = jsonObject.getJSONArray("issues");
	    if (jsonArray != null && jsonArray.length() > 0) {

		for (int j = 0; j < jsonArray.length(); j++) {
		    String are = null;
		    String dre = null;
		    String cre = null;
		    String utdre = null;
		    String re_testcases_developer = null;
		    String re_testcases_QA = null;
		    int testable_count=0;
		    double analysis_review_effort = 0;

		    double design_review_effort = 0;

		    double code_review_effort = 0;
		    double unit_test_design_review_effort = 0;
		    double review_effort_of_test_cases_written_by_developer = 0;
		    double review_effort_of_test_cases_written_by_QA = 0;
		    JSONObject issuesData = jsonArray.getJSONObject(j);

		    JSONObject fieldsData = issuesData.getJSONObject("fields");
		     if(fieldsData.has((map.get(analysis_review_effort_field)))) {
		    are = fieldsData.get(map.get(analysis_review_effort_field)).toString();
		    if (!are.equals("null") && !are.isEmpty())
			analysis_review_effort = Double.parseDouble(are);
		     }
		    // if(fieldsData.has((map.get(design_review_effort_field)))) {
		    dre = fieldsData.get(map.get(design_review_effort_field)).toString();
		    if (!dre.equals("null") && !dre.isEmpty())
			design_review_effort = Double.parseDouble(dre);
		    // }
		    // if(fieldsData.has((map.get(code_review_effort_field)))) {
		    cre = fieldsData.get(map.get(code_review_effort_field)).toString();
		    if (!cre.equals("null") && !cre.isEmpty())
			code_review_effort = Double.parseDouble(cre);
		    // }
		    // if(fieldsData.has((map.get(unit_test_design_review_effort_field)))) {
		    utdre = fieldsData.get(map.get(unit_test_design_review_effort_field)).toString();
		    if (!utdre.equals("null") && !utdre.isEmpty())
			unit_test_design_review_effort = Double.parseDouble(utdre);
		    // }
		    re_testcases_developer = fieldsData.get(map.get(unit_test_design_review_effort_field)).toString();
		    if (!re_testcases_developer.equals("null") && !re_testcases_developer.isEmpty())
			review_effort_of_test_cases_written_by_developer = Double.parseDouble(re_testcases_developer);
		    re_testcases_QA = fieldsData.get(map.get(unit_test_design_review_effort_field)).toString();
		    if (!re_testcases_QA.equals("null") && !re_testcases_QA.isEmpty())
			review_effort_of_test_cases_written_by_QA = Double.parseDouble(re_testcases_QA);
		    JSONObject testable_obj = fieldsData.getJSONObject(map.get(is_it_testable));
		     String testable= testable_obj.getString("value");
		     if(testable.equals("Yes"))
			 testable_count++;
		    total_analysis_review_effort = total_analysis_review_effort + analysis_review_effort;
		    total_design_review_effort = total_design_review_effort + design_review_effort;
		    total_code_review_effort = total_code_review_effort + code_review_effort;
		    total_unit_test_design_review_effort = total_unit_test_design_review_effort
		            + unit_test_design_review_effort;
		    total_review_effort_of_test_cases_written_by_developer=total_review_effort_of_test_cases_written_by_developer+review_effort_of_test_cases_written_by_developer;
		    total_review_effort_of_test_cases_written_by_QA=total_review_effort_of_test_cases_written_by_QA+review_effort_of_test_cases_written_by_QA;
		    total_testable=total_testable+testable_count;
		}
	    }

	    total_review_effort = total_analysis_review_effort + total_design_review_effort + total_code_review_effort
	            + total_unit_test_design_review_effort+total_review_effort_of_test_cases_written_by_developer+total_review_effort_of_test_cases_written_by_QA;
	    review_effort = total_analysis_review_effort + total_design_review_effort + total_code_review_effort+total_unit_test_design_review_effort;
	   
	    review_effort_list.add(total_review_effort);
	    review_effort_list.add(review_effort);
	    review_effort_list.add(total_unit_test_design_review_effort);
	    review_effort_list.add((double) total_testable);
	} catch (Exception e) {
	    es.writeException(e, project);
	}
	return CompletableFuture.completedFuture(review_effort_list);
    }
}