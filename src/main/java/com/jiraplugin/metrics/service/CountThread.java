package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.util.concurrent.CompletableFuture;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
@Component
@Service
public class CountThread {

	@Autowired
	private AtlassianHostRestClients restClients;
	
	@Async
	public CompletableFuture<Integer> getValues(String p,String name, int sDate, @AuthenticationPrincipal AtlassianHostUser hostUser) throws JSONException, ParseException {
		String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {name}) AND ('Reported Date'>=startOfMonth({sDate}) AND 'Reported Date'<=endOfMonth({eDate}))&startAt=0&maxResults=100";
		String response = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\""+ name + "\"",sDate,sDate).toUri(), String.class);
		JSONObject response1 = new JSONObject(response);
		int total = response1.getInt("total");
		return CompletableFuture.completedFuture(total);
	}
}
