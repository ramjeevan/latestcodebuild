package com.jiraplugin.metrics.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class VelocityService {
	@Autowired
	private AtlassianHostRestClients restClients;
	
	public
	Map<Integer, String> boardTypes = new LinkedHashMap<Integer, String>();
	
	public Map<String, Integer> getProjectsData(AtlassianHostUser hostUser) {
		String FieldJsonURL = "/rest/api/2/project";
		String result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,String.class);
		JSONArray jsonObj = new JSONArray(result);
		Map<String,Integer> customFieldIDMap = new LinkedHashMap<String, Integer>();
		for (int i = 0; i < jsonObj.length(); i++) {
			JSONObject obj = jsonObj.getJSONObject(i);
			customFieldIDMap.put(obj.getString("key"), obj.getInt("id"));
		}
		return customFieldIDMap;

	}
	
	public Map<Integer,Integer> getBoardId(AtlassianHostUser hostUser ) {
		String FieldJsonURL = "/rest/agile/1.0/board";
		String result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,String.class);
		JSONObject response = new JSONObject(result);
		JSONArray jsonArray = response.getJSONArray("values");
		Map<Integer, Integer> boardData = new LinkedHashMap<Integer, Integer>();
		for(int i = 0; i < jsonArray.length(); i++) {
			JSONObject o = jsonArray.getJSONObject(i);
			try {
			JSONObject location = o.getJSONObject("location");
			boardTypes.put(o.getInt("id"), o.getString("type"));
			boardData.put(location.getInt("projectId"),o.getInt("id"));		
		}catch(Exception e) {
			System.out.println("No Project Id");
		}
		}
		return boardData; 
	}
	
	
	
}
