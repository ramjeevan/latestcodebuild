package com.jiraplugin.metrics.service;

import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.FieldCustomID;

@Service
public class JiraCustomFieldMap {

	private AtlassianHostRestClients restClients;

	public Map<String, String> customFieldIDMap = new LinkedHashMap<String, String>();

	@Autowired
	public JiraCustomFieldMap(AtlassianHostRestClients theRestClients) {
		this.restClients = theRestClients;
	}

	// Returns the list of fields in the jira instance with field keys
	public Map<String, String> getJiraFieldList(AtlassianHostUser hostUser) {

		String FieldJsonURL = "/rest/api/2/field";

		FieldCustomID[] Result = restClients.authenticatedAs(hostUser).getForObject(FieldJsonURL,
				FieldCustomID[].class);

		for (int i = 0; i < Result.length; i++) {

			customFieldIDMap.put(Result[i].getName(), Result[i].getKey());

		}

		return customFieldIDMap;

	}

}
