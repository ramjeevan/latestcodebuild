package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.CreativeIdea;
import com.jiraplugin.metrics.model.DEffectivenessData;


@Service
public class DefectsService {
	@Autowired
	private AtlassianHostRestClients restClients;
	@Value("${review_defect}")  String review_defect_nwf;
	@Value("${testing_defect}")  String testing_defect_nwf;
	@Value("${delivery_defect}")  String delivery_defect_nwf;
	@Value("${analysis_design_review_defect}")  String analysis_design_review_defect_nwf;
	@Value("${unit_testing_defect}")  String unit_testing_defect_nwf;
	@Value("${test_case_review_defect}")  String test_case_review_defect_nwf;
	 @Value("${delivery_defect}")
	    String delivery_defect;
	    @Value("${corrective_improvement_actions}")
	    String corrective_improvement_actions;
	    @Value("${reported_date}")
	    String reported_date;
	    @Value("${category_corrective_improvement_actions}")
	    String category_corrective_improvement_actions;
	    @Value("${issue_description}")
	    String issue_description;
	    @Value("${defect_type}")
	    String defect_type;
	    @Value("${priority}")
	    String priority;
	    @Value("${root_cause}")
	    String root_cause;
	    @Value("${action}")
	    String action;
	    @Value("${action_type}")
	    String action_type;
	    @Value("${responsibility}")
	    String responsibility;
	    @Value("${actual_date}")
	    String actual_date;
	    @Value("${target_date}")
	    String target_date;
	int rl = 0,rh = 0,rm = 0,tl = 0,tm = 0,th = 0,dh = 0,dl = 0,dm = 0;
	
	@Autowired ProjectService p;
	Map<String, String> map = p.customFieldIDMap;
	public CreativeIdea parseCreativeIdea(JSONObject j) throws ParseException {
		
		JSONObject fieldsData = j.getJSONObject("fields");
		CreativeIdea cia = new CreativeIdea();
		JSONObject iss_type = fieldsData.getJSONObject("issuetype");
		String issuetype = iss_type.getString("name");
		String repdate = fieldsData.getString(map.get(reported_date));
		Date rep = new SimpleDateFormat("yyyy-MM-dd").parse(repdate);
		String repdt = new SimpleDateFormat("dd-MMM-yyyy").format(rep);
		cia.setDate(repdt);
		cia.setStatus(fieldsData.getJSONObject("status").getString("name"));
		    cia.setCategory("Delivery Defect");
		cia.setDescription(fieldsData.getString(map.get(issue_description)));
		JSONObject prio = fieldsData.getJSONObject(map.get("Complexity"));
		cia.setPriority(prio.getString("value"));
		
		try {
		cia.setAction(fieldsData.getString(map.get(action)));
		}catch(Exception e) {
			cia.setAction("-");
		}
		try {
			cia.setRoot_cause(fieldsData.getString(map.get(root_cause)));
			}catch(Exception e) {
				cia.setRoot_cause("-");
			}
		if (issuetype.equals(delivery_defect))
		    cia.setAction_type("Corrective Action");
		else {
		    JSONObject action_type_object = fieldsData.getJSONObject(map.get(action_type));
		    cia.setAction_type(action_type_object.getString("value"));
		}
		try {
		JSONArray robject = fieldsData.getJSONArray(map.get(responsibility));
		String resp = "";
		boolean flag = false;
		for (int k = 0; k < robject.length(); k++) {
		    if (flag == true) {
			resp += ",";
		    }
		    JSONObject jo = robject.getJSONObject(k);
		    resp += jo.getString("displayName");
		    flag = true;
		}
		cia.setResponsibility(resp);
		}
		catch(Exception e) {
			cia.setResponsibility("-");
		}
		try {
		String tgd = fieldsData.getString(map.get(target_date));
		if (!tgd.equals("null") && !tgd.isEmpty()) {
		Date tdate = new SimpleDateFormat("yyyy-MM-dd").parse(tgd);
		String td = new SimpleDateFormat("dd-MMM-yyyy").format(tdate);
		cia.setTarget_date(td);
		}
		}catch(Exception e) {
			cia.setTarget_date("-");
		}
		String acd = fieldsData.get(map.get(actual_date)).toString();
	//	String acd = fieldsData.getString(map.get(actual_date));
		if (!acd.equals("null") && !acd.isEmpty()) {
		    Date ad = new SimpleDateFormat("yyyy-MM-dd").parse(acd);
		    String adt = new SimpleDateFormat("dd-MMM-yyyy").format(ad);
		    cia.setActual_date(adt);
		}
		else
		    cia.setActual_date("-"); 
		// cia.setRemarks(fieldsData.getString(customfields.get("Remarks (Corr./Improv.
		// Actions)")));
		return cia;
	    }
		
	public void testingData(String severity) {
		if(severity.equals("High")) {
			th++;
		}else if(severity.equals("Low")) {
			tl++;
		}else if(severity.equals("Medium")) {
			tm++;
		}
	}
	public void reviewData(String severity) {
		if(severity.equals("High")) {
			rh++;
		}else if(severity.equals("Low")) {
			rl++;
		}else if(severity.equals("Medium")) {
			rm++;
		}
	}
	public void deliveryData(String severity) {
		if(severity.equals("High")) {
			dh++;
		}else if(severity.equals("Low")) {
			dl++;
		}else if(severity.equals("Medium")) {
			dm++;
		}
	}
	@Async
	public CompletableFuture<DEffectivenessData> defectsCount(String project, int i,  AtlassianHostUser hostUser) throws Exception {
		
		
		/*String review_defect="Review Defect";
		String testing_defect="Testing Defect";
		String delivery_defect="Delivery Defect";
		//String review_defect_nwf="Review Defect (NWF)";
		String testing_defect_nwf="Testing Defect (NWF)";
		String delivery_defect_nwf="Delivery Defect (NWF)";
		//String analysis_design_review_defect="Analysis and Design Review Defect";
		String analysis_design_review_defect_nwf="Analysis and Design Review Defect (NWF)";
		//String unit_testing_defect="Unit Testing Defect";
		String unit_testing_defect_nwf="Unit Testing Defect (NWF)";
		//String test_case_review_defect="Test Case Review Defect";
		String test_case_review_defect_nwf="Test Case Review Defect (NWF)";*/
		ArrayList<CreativeIdea> ciData = new ArrayList<CreativeIdea>();
		String url = "/rest/api/2/search?jql=project={p} AND ( issuetype={defectname} OR issuetype={defectname} OR issuetype={defectname}  OR issuetype={defectname} OR issuetype={defectname} OR issuetype={defectname}) AND 'Reported date'>=startOfMonth({i}) AND 'Reported date'<=endOfMonth({i})&startAt=0&maxResults=100";
		//String url = "/rest/api/2/search?jql=project={p} AND issuetype in ({defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}, {defectname}) AND 'Reported date'>=startOfMonth({i}) AND 'Reported date'<=endOfMonth({i})&startAt=0&maxResults=100";
	
		String response = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder.fromUriString(url)
								.buildAndExpand("\"" + project + "\"",  "\"" + review_defect_nwf + "\"","\"" + analysis_design_review_defect_nwf + "\"","\"" + unit_testing_defect_nwf + "\"","\"" + test_case_review_defect_nwf + "\"","\"" + testing_defect_nwf + "\"","\"" + delivery_defect_nwf + "\"", i,i).toUri(),
						String.class);
		JSONObject jsonObject = new JSONObject(response);
		JSONArray jsonArray = jsonObject.getJSONArray("issues");
		int count = jsonObject.getInt("total");
		int maxResults = Integer.parseInt(jsonObject.get("maxResults").toString());
		if (count > maxResults) {
			url = "/rest/api/2/search?jql=project={project} AND (issuetype={defectname} OR issuetype={defectname} OR issuetype={defectname}  OR issuetype={defectname} OR issuetype={defectname} OR issuetype={defectname}) AND 'Reported date'>=startOfMonth({i}) AND 'Reported date'<=endOfMonth({i})&startAt=100&maxResults=100";
			String response1 = restClients.authenticatedAs(hostUser).getForObject(
					UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + project + "\"",  "\"" + review_defect_nwf + "\"","\"" + analysis_design_review_defect_nwf + "\"","\"" + unit_testing_defect_nwf + "\"","\"" + test_case_review_defect_nwf + "\"","\"" + testing_defect_nwf + "\"","\"" + delivery_defect_nwf + "\"", i,i).toUri(),
					String.class);
			JSONObject jsonObj1 = new JSONObject(response1);
			JSONArray jsonArray1 = jsonObj1.getJSONArray("issues");
			for (int j = 0; j < jsonArray1.length(); j++) {
				JSONObject objects = jsonArray1.getJSONObject(j);
				jsonArray.put(objects);
			}
		}
		int reviewdefects_count=0;
		int review_defects_adc=0;
		int unit_testing_defects_count=0;
		int total_review_defects_count=0;
		int total_test_design_defects_count=0;
		int testingdefects_count=0;
		int deliverydefects_count = 0;
		int test_case_review_defect_count=0;
		for (int j = 0; j < jsonArray.length(); j++) {
			
			JSONObject issuesData = jsonArray.getJSONObject(j);
			JSONObject fieldsData = issuesData.getJSONObject("fields");
			
			JSONObject issuetype_object=fieldsData.getJSONObject("issuetype");
			String issue_type=issuetype_object.getString("name");
			String type = "";
			try {
			JSONObject severity_object=fieldsData.getJSONObject("customfield_10591");
			type=severity_object.get("value").toString();
			System.out.println(type);
			}catch(Exception e) {
				type = "none";
				System.out.println(type);
			}
			if(issue_type.equals(review_defect_nwf) || issue_type.equals(analysis_design_review_defect_nwf)) {
				reviewdefects_count++;
				reviewData(type);
			}
			else if(issue_type.equals(unit_testing_defect_nwf) ) {
			    unit_testing_defects_count++;
				reviewData(type);
			}
			else if(issue_type.equals(test_case_review_defect_nwf) ) {
				test_case_review_defect_count++;
				reviewData(type);
			}
			else if(issue_type.equals(testing_defect_nwf)) {
				testingdefects_count++;
				testingData(type);
			}
			else if(issue_type.equals(delivery_defect_nwf)) {
			deliverydefects_count++;
			ciData.add(parseCreativeIdea(issuesData));
			deliveryData(type);
			}
		}
		total_review_defects_count=reviewdefects_count+test_case_review_defect_count+unit_testing_defects_count;
		review_defects_adc=reviewdefects_count+unit_testing_defects_count;
		total_test_design_defects_count=unit_testing_defects_count+test_case_review_defect_count;
		DEffectivenessData data = new DEffectivenessData();
		data.setDesign_count(total_test_design_defects_count);
		data.setdHCount(dh);
		data.setdLCount(dl);
		data.setdMCount(dm);
		data.setdTCount(deliverydefects_count);
		data.setiHCount(rh + th);
		data.setiLCount(rl + tl);
		data.setiMCount(rm + tm);
		data.setiTCount(total_review_defects_count + testingdefects_count);
		data.setRd_adc_count(review_defects_adc);
		data.setrHCount(rh);
		data.setrLCount(rl);
		data.setrMCount(rm);
		data.setrTCount(total_review_defects_count);
		data.settHCount(th);
		data.settLCount(tl);
		data.settMCount(tm);
		data.settTCount(testingdefects_count);
		data.setDeliveryCausalData(ciData);
		/*
		List<Integer> performanceMetrics=new ArrayList<Integer>();
		performanceMetrics.add(review_defects_adc);
		performanceMetrics.add(total_review_defects_count);
		performanceMetrics.add(testingdefects_count);
		performanceMetrics.add(total_test_design_defects_count);
		performanceMetrics.add(deliverydefects_count);*/
		/*PerformanceMetrics pm=new PerformanceMetrics();
		pm.setDelivery_defects(deliverydefects_count);
		pm.setReview_defects(reviewdefects_count);
		pm.setTesting_defects(testingdefects_count);*/
		rl = 0;
		rh = 0;
		rm = 0;
		tl = 0;
		tm = 0;
		th = 0;
		dh = 0;
		dl = 0;
		dm = 0;
		return CompletableFuture.completedFuture(data);
	}
	
}