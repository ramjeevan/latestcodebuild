package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;

@Service
public class ClientSatisfactionThread {

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	@Value("${UserName}")
	String userName;

	@Value("${PassWord}")
	String password;

	@Value("${Domian_Url}")
	String Domain_URL;

	@Value("${corrective_improvement_actions}")
	String Complaint_IssueType;

	@Value("${client_complaints}")
	String Category;

	@Value("${PeriodJanS}")
	String JanPS;

	@Value("${PeriodJanE}")
	String JanPE;

	@Value("${PeriodMayS}")
	String MayPS;

	@Value("${PeriodMayE}")
	String MayPE;

	@Value("${PeriodSepS}")
	String SepPS;

	@Value("${PeriodSepE}")
	String SepPE;

	@Async
	public CompletableFuture<ArrayList<ComplaintData>> findCount(AtlassianHostUser hostUser, String sce, String back,
			String front) throws InterruptedException, Exception, ParseException {
		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Category (CI/DI)' = {Category} AND 'Reported Date' >= {back} AND 'Reported Date' <= {front}";

		String response1 = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder.fromUriString(urlPara)
								.buildAndExpand("\"" + sce + "\"", "\"" + Complaint_IssueType + "\"",
										"\"" + Category + "\"", "\"" + back + "\"", "\"" + front + "\"")
								.toUri(),
						String.class);
		JSONObject response = new JSONObject(response1);
		int count = response.getInt("total");
		System.out.println(count);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);
		ArrayList<ComplaintData> data = new ArrayList<ComplaintData>();
		String rDate = fieldList.get("Reported Date");
		String description = fieldList.get("Issue description");
		String action = fieldList.get("Action");
		String rFrom = fieldList.get("If Client Complaint, Received From");
		JSONArray jsonArray = response.getJSONArray("issues");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
			ComplaintData complaintData = new ComplaintData();
			JSONObject fields = j.getJSONObject("fields");
			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			complaintData.setrDate(finalDate);
			complaintData.setDescription(fields.get(description).toString());
			complaintData.setAction(fields.get(action).toString());
			String recFrom;
			try {
				recFrom = fields.getString(rFrom).toString();
			} catch (Exception e) {
				recFrom = "-";
			}
			complaintData.setrFrom(recFrom);
			String obj = fields.get("status").toString();
			JSONObject sta = new JSONObject(obj);
			complaintData.setStatus(sta.get("name").toString());
			data.add(complaintData);
		}
		return CompletableFuture.completedFuture(data);
	}

	@Value("${client_appreciation}")
	String Client_Appreciation_IT;

	@Async
	public CompletableFuture<ArrayList<AppreciationData>> findCountOfAppreciation(AtlassianHostUser hostUser,
			String sce, String back, String front) throws InterruptedException, Exception, ParseException {

		RestTemplate restTemplate = new RestTemplate();

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(userName + "@senecaglobal.com", password));

		String urlString = Domain_URL;
		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Client_Appreciation_IT} AND 'Date Received' >= {back} AND 'Date Received' <= {front}";
		String domain = urlString + urlPara;

		String response2 = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(domain).buildAndExpand("\"" + sce + "\"",
						"\"" + Client_Appreciation_IT + "\"", "\"" + back + "\"", "\"" + front + "\"").toUri(),
				String.class);

		JSONObject response = new JSONObject(response2);
		int count = response.getInt("total");
		System.out.println(count);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);
		ArrayList<AppreciationData> data = new ArrayList<AppreciationData>();
		String rDate = fieldList.get("Date Received");
		String rFrom = fieldList.get("Received From");
		String context = fieldList.get("Context");
		String rFor = fieldList.get("Received For");
		String details = fieldList.get("Appreciation Details");

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			AppreciationData appreciationData = new AppreciationData();
			JSONObject fields = j.getJSONObject("fields");
			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			appreciationData.setdReceived(finalDate);
			appreciationData.setrFrom(fields.get(rFrom).toString());
			appreciationData.setContext(fields.get(context).toString());
			appreciationData.setrFor(fields.get(rFor).toString());
			appreciationData.setDetails(fields.get(details).toString());
			data.add(appreciationData);
		}

		return CompletableFuture.completedFuture(data);
	}

	@Value("${csfa_Category}")
	String CSFACategory;

	ClientSatisfactionFA clientSatisfactionFA;

	@Async
	public CompletableFuture<ArrayList<ClientSatisfactionFA>> findCountCSFA(AtlassianHostUser hostUser, String sce,
			String back, String front) throws Exception, Exception {

		clientSatisfactionFA = new ClientSatisfactionFA();

		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Category (CI/DI)' = {Category} AND 'Reported Date' >= {back} AND 'Reported Date' <= {front}";

		String response1 = restClients.authenticatedAs(hostUser).getForObject(
				UriComponentsBuilder.fromUriString(urlPara)
						.buildAndExpand("\"" + sce + "\"", "\"" + Complaint_IssueType + "\"",
								"\"" + CSFACategory + "\"", "\"" + back + "\"", "\"" + front + "\"")
						.toUri(),
				String.class);

		ArrayList<ClientSatisfactionFA> data = new ArrayList<ClientSatisfactionFA>();

		JSONObject response = new JSONObject(response1);
		int count = response.getInt("total");
		System.out.println(count);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);
		String rDate = fieldList.get("Reported Date");
		String categoryFiled = fieldList.get("Category (CI/DI)");
		String description = fieldList.get("Issue description");
		String rFrom = fieldList.get("If Client Complaint, Received From");
		String severity = fieldList.get("Severity of the Issue");
		String rootCause = fieldList.get("Root Cause");
		String defectType = fieldList.get("Defect Type");
		String actionType = fieldList.get("Action Type");
		String action = fieldList.get("Action");
		String reponsibility = fieldList.get("Responsibility");
		String actualDate = fieldList.get("Actual Date");
		String targetDate = fieldList.get("Target Date");
		String remarks = fieldList.get("Remarks");

		JSONArray jsonArray = response.getJSONArray("issues");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
		for (int i = 0; i < jsonArray.length(); i++) {

			clientSatisfactionFA = new ClientSatisfactionFA();

			JSONObject j = jsonArray.getJSONObject(i);

			JSONObject fields = j.getJSONObject("fields");

			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			clientSatisfactionFA.setReported_Date(finalDate);

			clientSatisfactionFA.setCategory(fields.getJSONObject(categoryFiled).get("value").toString());
			clientSatisfactionFA.setIssue_Description(fields.get(description).toString());
			clientSatisfactionFA.setReceived_From(fields.get(rFrom).toString());
			clientSatisfactionFA.setSeverity(fields.getJSONObject(severity).getString("value").toString());

			clientSatisfactionFA.setRoot_Cause(fields.get(rootCause).toString());
			clientSatisfactionFA.setDefect_Type(fields.getJSONObject(defectType).getString("value").toString());
			clientSatisfactionFA.setAction_Type(fields.get(actionType).toString());
			clientSatisfactionFA.setAction(fields.get(action).toString());
			String resvalue = "";

			try {
				JSONArray resArray = fields.getJSONArray(reponsibility);

				int length = resArray.length();
				for (int h = 0; h < length; h++) {
					if (h != 0 && length > 1) {

						resvalue = resvalue + ", ";
					}
					resvalue = resvalue + resArray.getJSONObject(h).get("displayName").toString();

				}
			} catch (Exception e) {

				clientSatisfactionFA.setResponsibility("-");

				System.out.println(e);
			}

			clientSatisfactionFA.setResponsibility(resvalue);

			clientSatisfactionFA.setActual_Date(fields.get(actualDate).toString());
			clientSatisfactionFA.setTarget_Date(fields.get(targetDate).toString());
			clientSatisfactionFA.setRemarks(fields.get(remarks).toString());

			data.add(clientSatisfactionFA);

		}

		return CompletableFuture.completedFuture(data);
	}

	@Value("${client_complaints}")
	String cC_S;

	@Value("${csfa_Category}")
	String cSFA_S;
	@Value("${Project_Delivery_PI}")
	String pDPI_S;
	@Value("${Process_Complaince_Issue}")
	String pCI_S;

	@Value("${Information_Security}")
	String iSW_S;

	@Value("${Information_Security_Event}")
	String iSE_S;

	@Value("${Risk_Event}")
	String rE_S;

	@Value("${Delivery_Improvement}")
	String dI_S;

	@Value("${Creative_Idea}")
	String cI_S;

	CIDIData cidiAspect(String response1, AtlassianHostUser hostUser) throws Exception, Exception {

		ArrayList<ClientSatisfactionFA> ccData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> csfaData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> pdpiData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> pciData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> iswData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> iseData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> reData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> diData = new ArrayList<ClientSatisfactionFA>();
		ArrayList<ClientSatisfactionFA> ciData = new ArrayList<ClientSatisfactionFA>();
		CIDIData cidiData = new CIDIData();
		JSONObject response = new JSONObject(response1);
		int count = response.getInt("total");
		System.out.println(count);
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);
		String rDate = fieldList.get("Reported Date");
		String categoryFiled = fieldList.get("Category (CI/DI)");
		String description = fieldList.get("Issue description");
		String rFrom = fieldList.get("If Client Complaint, Received From");
		String severity = fieldList.get("Severity of the Issue");
		String rootCause = fieldList.get("Root Cause");
		String defectType = fieldList.get("Defect Type");
		String actionType = fieldList.get("Action Type");
		String action = fieldList.get("Action");
		String reponsibility = fieldList.get("Responsibility");
		String actualDate = fieldList.get("Actual Date");
		String targetDate = fieldList.get("Target Date");
		String remarks = fieldList.get("Remarks");

		JSONArray jsonArray = response.getJSONArray("issues");
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
		for (int i = 0; i < jsonArray.length(); i++) {

			clientSatisfactionFA = new ClientSatisfactionFA();

			JSONObject j = jsonArray.getJSONObject(i);

			JSONObject fields = j.getJSONObject("fields");

			Date date = formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);

			clientSatisfactionFA.setReported_Date(finalDate);

			clientSatisfactionFA.setCategory(fields.getJSONObject(categoryFiled).get("value").toString());
			clientSatisfactionFA.setIssue_Description(fields.get(description).toString());
			clientSatisfactionFA.setReceived_From(fields.get(rFrom).toString());
			clientSatisfactionFA.setSeverity(fields.getJSONObject(severity).getString("value").toString());

			clientSatisfactionFA.setRoot_Cause(fields.get(rootCause).toString());
			clientSatisfactionFA.setDefect_Type(fields.getJSONObject(defectType).getString("value").toString());
			clientSatisfactionFA.setAction_Type(fields.get(actionType).toString());
			clientSatisfactionFA.setAction(fields.get(action).toString());
			String resvalue = "";

			try {
				JSONArray resArray = fields.getJSONArray(reponsibility);

				int length = resArray.length();
				for (int h = 0; h < length; h++) {
					if (h != 0 && length > 1) {

						resvalue = resvalue + ", ";
					}
					resvalue = resvalue + resArray.getJSONObject(h).get("displayName").toString();

				}
			} catch (Exception e) {

				clientSatisfactionFA.setResponsibility("-");

				System.out.println(e);
			}

			clientSatisfactionFA.setResponsibility(resvalue);

			clientSatisfactionFA.setActual_Date(fields.get(actualDate).toString());
			clientSatisfactionFA.setTarget_Date(fields.get(targetDate).toString());
			clientSatisfactionFA.setRemarks(fields.get(remarks).toString());

			if (clientSatisfactionFA.getAction().contentEquals("null")) {
				clientSatisfactionFA.setAction("-");
			}
			if (clientSatisfactionFA.getAction_Type().contentEquals("null")) {
				clientSatisfactionFA.setAction_Type("-");
			}
			if (clientSatisfactionFA.getActual_Date().contentEquals("null")) {
				clientSatisfactionFA.setActual_Date("-");
			}
			if (clientSatisfactionFA.getCategory().contentEquals("null")) {
				clientSatisfactionFA.setCategory("-");
			}
			if (clientSatisfactionFA.getDefect_Type().contentEquals("null")) {
				clientSatisfactionFA.setDefect_Type("-");
			}
			if (clientSatisfactionFA.getIssue_Description().contentEquals("null")) {
				clientSatisfactionFA.setIssue_Description("-");
			}
			if (clientSatisfactionFA.getReceived_From().contentEquals("null")) {
				clientSatisfactionFA.setReceived_From("-");
			}
			if (clientSatisfactionFA.getReported_Date().contentEquals("null")) {
				clientSatisfactionFA.setReported_Date("-");
			}
			if (clientSatisfactionFA.getResponsibility().contentEquals("null")) {
				clientSatisfactionFA.setResponsibility("-");
			}

			String category = fields.getJSONObject(categoryFiled).get("value").toString();

			if (category.equals(cC_S)) {
				ccData.add(clientSatisfactionFA);

			} else if (category.equals(cSFA_S)) {
				csfaData.add(clientSatisfactionFA);

			} else if (category.equals(pDPI_S)) {

				pdpiData.add(clientSatisfactionFA);

			} else if (category.equals(pCI_S)) {

				pciData.add(clientSatisfactionFA);

			} else if (category.equals(iSW_S)) {

				iswData.add(clientSatisfactionFA);

			} else if (category.equals(iSE_S)) {

				iseData.add(clientSatisfactionFA);

			} else if (category.equals(rE_S)) {

				reData.add(clientSatisfactionFA);

			} else if (category.equals(dI_S)) {

				diData.add(clientSatisfactionFA);

			} else if (category.equals(cI_S)) {

				ciData.add(clientSatisfactionFA);

			}

		}

		cidiData.setcCData(ccData);
		cidiData.setcIData(ciData);
		cidiData.setcSFAData(csfaData);
		cidiData.setdIData(diData);
		cidiData.setiSEData(iseData);
		cidiData.setiSWData(iswData);
		cidiData.setpCIData(pciData);
		cidiData.setpDPIData(pdpiData);
		cidiData.setrEData(reData);

		return cidiData;

	}

	@Async
	public CompletableFuture<CIDIData> findCIDICount(AtlassianHostUser hostUser, String sce, int num) throws Exception {

		String urlPara = "/rest/api/2/search?jql=project={sce} AND issuetype= {Complaint_IssueType} AND 'Reported Date' >= startOfMonth({num}) AND 'Reported Date' <= endOfMonth({num})";

		String response1 = restClients.authenticatedAs(hostUser)
				.getForObject(
						UriComponentsBuilder
								.fromUriString(urlPara).buildAndExpand("\"" + sce + "\"",
										"\"" + Complaint_IssueType + "\"", "\"" + num + "\"", "\"" + num + "\"")
								.toUri(),
						String.class);

		try {
			String response1A = restClients.authenticatedAsHostActor().getForObject("/wiki/rest/api/space/",
					String.class);
			RestTemplate restTemplate = new RestTemplate();
			restTemplate.getInterceptors()
					.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

			String response2 = restTemplate.getForObject("https://seneca-global.atlassian.net/wiki/rest/api/space/",
					String.class);
			System.out.println(response2);
		} catch (Exception e) {
			System.out.println(e);
		}

		CIDIData data = cidiAspect(response1, hostUser);

		return CompletableFuture.completedFuture(data);
	}

}
