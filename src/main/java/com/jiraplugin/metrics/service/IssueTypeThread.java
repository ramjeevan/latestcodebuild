package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.EffectivenessData;

@Component
@Service
public class IssueTypeThread {
	@Value("${story}") String story;
	@Value("${task}") String task;
	@Value("${new_requirement}") String new_requirement;
	@Value("${change_request}") String change_request;
	@Value("${production_issue_support}") String production_issue_support;
	@Value("${improvement}") String improvement;
	@Autowired
	CountThread t;
	
	@Async
	public CompletableFuture<EffectivenessData> runTasks(String p, int sDate,@AuthenticationPrincipal AtlassianHostUser hostUser) throws JSONException, ParseException, InterruptedException, ExecutionException {
		EffectivenessData data = new EffectivenessData();
		long start = System.currentTimeMillis();
		CompletableFuture<Integer> t0 = t.getValues(p,story, sDate, hostUser);
		
		CompletableFuture<Integer> t1 = t.getValues(p,task, sDate, hostUser);
		
		CompletableFuture<Integer> t2 = t.getValues(p,new_requirement, sDate, hostUser);
		
		CompletableFuture<Integer> t3 = t.getValues(p,change_request, sDate, hostUser);
		
		CompletableFuture<Integer> t4 = t.getValues(p,production_issue_support, sDate, hostUser);	
		
		CompletableFuture<Integer> t5 = t.getValues(p,improvement, sDate, hostUser);
	
		
		CompletableFuture.allOf(t0,t1,t2,t3,t4, t5).join();
		
		data.setsCount(t0.get());
		data.settCount(t1.get());
		data.setNrCount(t2.get());
		data.setCrCount(t3.get());
		data.setPisCount(t4.get());
		data.setiCount(t5.get());
		
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		String m = (YearMonth.now().minusMonths(Math.abs(sDate))).format(formatter);
		data.setMonth(m);
		return CompletableFuture.completedFuture(data);
	}
}
