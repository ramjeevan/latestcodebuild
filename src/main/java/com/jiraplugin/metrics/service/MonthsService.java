package com.jiraplugin.metrics.service;

import java.time.LocalDate;
import java.time.Period;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class MonthsService {
	public List<String> getMonths() {
		List<String> months = new ArrayList<String>();
		DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM ");
		YearMonth thisMonth = YearMonth.now();
		String thismon = thisMonth.format(monthYearFormatter);
		months.add(thismon);
		for (int i = 1; i <= 4; i++) {
			YearMonth p = thisMonth.minusMonths(i);
			months.add(p.format(monthYearFormatter));
		}
		System.out.println("string month " + thismon);
		System.out.println("months list " + months);
		return months;
	}

	public List<String> getMonthAndYear() {
		List<String> monthYear = new ArrayList<String>();
		DateTimeFormatter monthYearFormatter = DateTimeFormatter.ofPattern("MMMM-yy");
		YearMonth thisMonthYear = YearMonth.now();
		String thismon = thisMonthYear.format(monthYearFormatter);
		monthYear.add(thismon);
		for (int i = 1; i <= 4; i++) {
			YearMonth p = thisMonthYear.minusMonths(i);
			monthYear.add(p.format(monthYearFormatter));
		}
		System.out.println("string month year " + thisMonthYear);
		System.out.println("months year list " + monthYear);
		return monthYear;
	}
	public List<String> getCycles(){
		List<String> cycles=new ArrayList<String>();
		cycles.add("Cycle-1");
		cycles.add("Cycle-2");
		cycles.add("Cycle-3");
		cycles.add("External");
		
		return cycles;
		
	}
	//get 2017-18 format
	public List<String> getFinancialyears(){
		List<String> financialyear=new ArrayList<String>();
		Calendar c = Calendar.getInstance(); 
		int year  = c.get(Calendar.YEAR);
		int nextyear=year+1;
		for(int i=0;i<=5;i++){
			financialyear.add((year-i)+"-"+(nextyear-i));
		}
		System.out.println(financialyear);
		return financialyear;
	}
	public int getMonthsDifference(Date selectedDate){
		LocalDate requiredDate = selectedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		LocalDate currentDate = LocalDate.now();
		
          int diffYear = currentDate.getYear()-requiredDate.getYear();
          int diffMonth = diffYear * 12 +  currentDate.getMonthValue() - requiredDate.getMonthValue();
		System.out.println("Number of months in between" + diffMonth + "y"+diffYear);
		return diffMonth;

		
	}

	
}