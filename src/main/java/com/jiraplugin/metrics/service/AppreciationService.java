package com.jiraplugin.metrics.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;


@Service
public class AppreciationService {
	@Value("${client_appreciation}")  String appreciation;
	@Autowired JiraCustomFieldMap j;
	@Autowired ErrorService es;
	@Autowired private AtlassianHostRestClients restClients;
	/*@Cacheable("Appreciation Data")*/
	public ArrayList<AppreciationData> getAppreciationData(AtlassianHostUser hostUser,String p) throws IOException{
	ArrayList<AppreciationData> data = new ArrayList<AppreciationData>();
	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); 
	SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");
	try {
	String url = "/rest/api/2/search?jql=project={p} AND  (issuetype= {appreciation})AND ('Date Received'>=startOfMonth(-4))";
	String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder.fromUriString(url).buildAndExpand("\"" + p + "\"", "\"" + appreciation + "\"").toUri(), String.class);
	JSONObject response1 = new JSONObject(response);
	int count = response1.getInt("total");  
	if(count != 0) {
		Map<String, String> fieldList = j.getJiraFieldList(hostUser);
		String rDate = fieldList.get("Date Received");
		String rFrom = fieldList.get("Received From");
		String context = fieldList.get("Context");
		String rFor = fieldList.get("Received For");
		String details = fieldList.get("Appreciation Details");
		JSONArray jsonArray = response1.getJSONArray("issues");
		for(int i = 0; i < jsonArray.length(); i++) {
			JSONObject j = jsonArray.getJSONObject(i);
			AppreciationData appreciationData = new AppreciationData();
			JSONObject fields = j.getJSONObject("fields");
			Date date = (Date)formatter.parse(fields.get(rDate).toString());
			String finalDate = newFormat.format(date);
			appreciationData.setdReceived(finalDate);
			appreciationData.setrFrom(fields.get(rFrom).toString());
			appreciationData.setContext(fields.get(context).toString());
			appreciationData.setrFor(fields.get(rFor).toString());
			appreciationData.setDetails(fields.get(details).toString());
			data.add(appreciationData);
		}
	}
	}catch(Exception e) {
		es.writeException(e, p);
	}
	return data;
}
}
