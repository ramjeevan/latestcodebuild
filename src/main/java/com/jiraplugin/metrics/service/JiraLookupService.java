package com.jiraplugin.metrics.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.JsonData;

@Service
public class JiraLookupService {

	@Autowired
	private AtlassianHostRestClients restClients;

	String issueType = "Exposure-Rating";

	String riskValue = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={p} AND issuetype= {issueType} AND 'Residual Risk Value'>=1 AND 'Residual Risk Value'<=4 AND 'Reported Date' >= startOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Client Name' ~ {promain}";
	String riskIndentified = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project={p} AND issuetype= {issueType} AND 'Reported Date' >= startOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Client Name' ~ {promain}";

	@Async
	public CompletableFuture<Double> findCount(AtlassianHostUser hostUser, String sce, int dce, String promain)

	{

		RestTemplate restTemplate = new RestTemplate();

		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor("ramjeevan.tadi@senecaglobal.com", "hyd@1234"));

		Map<String, Integer> riskAcceptableMap = new LinkedHashMap<String, Integer>();

		String p;
		String urlString = "";
		String urlName = "";
		int date;
		Double DRTAL;

		p = sce;
		date = dce;
		for (int i = 0; i < 2; i++) {

			if (i == 0) {
				urlString = riskValue;
				urlName = "riskValue";
			} else if (i == 1) {
				urlString = riskIndentified;
				urlName = "riskIndentified";
			}

			JsonData responseEntity = restTemplate.getForObject(UriComponentsBuilder.fromUriString(urlString)
					.buildAndExpand("\"" + p + "\"", "\"" + issueType + "\"", "\"" + date + "\"", "\"" + date + "\"",
							"\"" + promain + "\"")
					.toUri(), JsonData.class);

			riskAcceptableMap.put(urlName, responseEntity.getTotal());

		}

		DRTAL = (riskAcceptableMap.get("riskValue").doubleValue()
				/ riskAcceptableMap.get("riskIndentified").doubleValue()) * 100;
		if (DRTAL.isNaN()) {
			DRTAL = 0.0;
		}

		return CompletableFuture.completedFuture(DRTAL);
	}

}
