package com.jiraplugin.metrics.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;

@Service
public class ClientSatisfaction {

	@Autowired
	private JiraCustomFieldMap jiraFieldList;

	@Autowired
	private AtlassianHostRestClients restClients;

	public static Map<String, String> yearMap = new LinkedHashMap<String, String>();
	public static Map<String, String> yearMap1 = new LinkedHashMap<String, String>();

	public ArrayList<String> collView = new ArrayList<String>();

	public Map<String, String> getCSAT(String sce, String selectedQuarter, AtlassianHostUser hostUser)
			throws JSONException {

		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		String pro = sce;

		Map<String, String> mapFields = new LinkedHashMap<String, String>();
		Map<String, String> mapFieldsMain = new LinkedHashMap<String, String>();

		String csat = "Client Satisfaction";
		String qsd = "Quarter Start Date";
		String quater = selectedQuarter;
		String date = yearMap.get(quater);

		String urlString = "/rest/api/2/search?jql=project={pro} AND issuetype={csat} AND {qsd} = {date} ";

		String response = restClients.authenticatedAs(hostUser)
				.getForObject(UriComponentsBuilder.fromUriString(urlString)
						.buildAndExpand("\"" + pro + "\"", "\"" + csat + "\"", "\"" + qsd + "\"", "\"" + date + "\"")
						.toUri(), String.class);

		JSONObject jsonObj = new JSONObject(response);

		JSONArray jsonArray = jsonObj.getJSONArray("issues");

		try {

			JSONObject dataObject = jsonArray.getJSONObject(0);

			JSONObject real = dataObject.getJSONObject("fields");

			mapFieldsMain.put("cn", real.getString(fieldList.get("Client Name")));
			mapFieldsMain.put("sd", real.getString(fieldList.get("Sent Date")));
			mapFieldsMain.put("rd", real.getString(fieldList.get("Received Date")));
			mapFieldsMain.put("qsd", real.getString(fieldList.get("Quarter Start Date")));
			mapFieldsMain.put("qed", real.getString(fieldList.get("Quarter End Date")));

			mapFieldsMain.put("a1", real.getJSONObject(fieldList.get(
					"1. How satisfied are you with the quality of software deliverables in terms of meeting scope and requirements?"))
					.getString("value"));
			mapFieldsMain.put("a2", real.getJSONObject(fieldList.get(
					"2. How satisfied are you with the implementation of delivery process and project management for on-time and reliable delivery?"))
					.getString("value"));
			mapFieldsMain.put("a3", real.getJSONObject(fieldList.get(
					"3. How do you rate the communication (oral and written), collaboration, and issue resolution by project team?"))
					.getString("value"));
			mapFieldsMain.put("a4",
					real.getJSONObject(fieldList.get(
							"4. How do you rate the application domain and technology competency of the project team?"))
							.getString("value"));
			mapFieldsMain.put("a5", real.getJSONObject(fieldList.get(
					"5. How do you rate the ideas and solutions to problems that were proactively offered by the project team?"))
					.getString("value"));
			mapFieldsMain.put("a6", real.getJSONObject(fieldList.get(
					"6. How satisfied are you with the “overall value add to business” from the deliverables and services provided by Seneca Global?"))
					.getString("value"));

			mapFieldsMain.put("a7", real
					.getString(fieldList.get("7. What do you consider as strength(s) of Seneca Global project team?")));
			mapFieldsMain.put("a8", real.getString(
					fieldList.get("8. What are the areas in which the Seneca Global project team needs to improve?")));

		} catch (Exception e) {

			System.out.println(e);

		}

		return mapFieldsMain;

	}

	public ArrayList<String> getYearQuarters() {

		DateFormat dateFormat = new SimpleDateFormat("YYYY");

		ArrayList<String> year = new ArrayList<String>();

		Calendar cal = Calendar.getInstance();
		String str1 = null;

		int monthValue = cal.get(Calendar.MONTH); // jan = 0, feb = 1, march =2,
		// april = 3, may =5

		for (int i = 0; i < 3; i++) {
			int counter = 9;

			for (int j = 3; j >= 1; j--) {

				if (i == 0) {

					cal.add(Calendar.YEAR, 0);
					if (j == 3 && monthValue == 11) {
						str1 = "Sep-Dec" + "(" + cal.get(Calendar.YEAR) + ")";
						year.add(str1);
						yearMap.put(str1, cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01");
					} else if (j == 2 && (monthValue > 7 && monthValue <= 11)) {
						str1 = "May-Aug" + "(" + cal.get(Calendar.YEAR) + ")";
						year.add(str1);
						yearMap.put(str1, cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01");
					} else if (j == 1 && (monthValue > 3 && monthValue <= 11)) {
						str1 = "Jan-Apr" + "(" + cal.get(Calendar.YEAR) + ")";
						year.add(str1);
						yearMap.put(str1, cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01");
					}
					// str1 = "Q" + j + "-" + cal.get(Calendar.YEAR);

					yearMap1.put(cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01", str1);
					counter = counter - 4;

				} else {

					if (j == 3) {
						str1 = "Sep-Dec" + "(" + cal.get(Calendar.YEAR) + ")";
					} else if (j == 2) {
						str1 = "May-Aug" + "(" + cal.get(Calendar.YEAR) + ")";
					} else if (j == 1) {
						str1 = "Jan-Apr" + "(" + cal.get(Calendar.YEAR) + ")";
					}

					// str1 = "Q" + j + "-" + cal.get(Calendar.YEAR);
					year.add(str1);
					yearMap.put(str1, cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01");
					yearMap1.put(cal.get(Calendar.YEAR) + "-" + "0" + counter + "-01", str1);
					counter = counter - 4;
				}

			}
			cal.add(Calendar.YEAR, -1);
		}

		return year;

	}

	public Map<String, String> getOveralCSAT(String sce, String selectedQuarter, AtlassianHostUser hostUser)
			throws JSONException, ParseException {

		Map<String, String> OveralRatingMap = new LinkedHashMap<String, String>();
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);
		String urlString = "/rest/api/2/search?jql=project={pro} AND issuetype={csat}";
		String csat = "Client Satisfaction";
		String pro = sce;
		String response = restClients.authenticatedAs(hostUser).getForObject(UriComponentsBuilder
				.fromUriString(urlString).buildAndExpand("\"" + pro + "\"", "\"" + csat + "\"").toUri(), String.class);
		Map<String, String> mapFields = new LinkedHashMap<String, String>();
		Map<String, String> mapFieldsMain = new LinkedHashMap<String, String>();
		Map<String, String> yearMapFields = new LinkedHashMap<String, String>();
		try {
			JSONObject jsonObj = new JSONObject(response);

			JSONArray jsonArray = jsonObj.getJSONArray("issues");

			for (int j = 0; j < jsonArray.length(); j++) {

				JSONObject dataObject = jsonArray.getJSONObject(j);

				JSONObject real = dataObject.getJSONObject("fields");

				mapFieldsMain.put(real.getString(fieldList.get("Quarter Start Date")), real.getJSONObject(fieldList.get(
						"6. How satisfied are you with the “overall value add to business” from the deliverables and services provided by Seneca Global?"))
						.getString("value"));
			}
			String quater = selectedQuarter;
			String date = yearMap.get(quater);

			ArrayList<String> Year1 = new ArrayList<String>();
			ArrayList<String> str = new ArrayList<String>();
			Year1 = this.getYearQuarters();
			int index = Year1.indexOf(yearMap1.get(date));
			String dash = "";

			collView.clear();
			yearMapFields.put(selectedQuarter, mapFieldsMain.get(yearMap.get(selectedQuarter)));
			  
			collView.add(selectedQuarter); 
			for (int i = 0; i < 3; i++) {

				index++;
				if (index >= Year1.size()) {
					collView.add("-");
					yearMapFields.put("-", "-");
				} else if (index <= Year1.size()) {
					str.add(Year1.get(index));
					collView.add(str.get(i));
					if (mapFieldsMain.get(yearMap.get(str.get(i))) == null) {
						dash = "-";
					} else
						dash = mapFieldsMain.get(yearMap.get(str.get(i)));
					yearMapFields.put(str.get(i), dash);
				}

			}
		} catch (Exception e) {
			System.out.println(e);

		} finally {
			return yearMapFields;
		}

	}

}
