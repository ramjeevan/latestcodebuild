package com.jiraplugin.metrics.service;

import java.text.ParseException;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.DefectTypeData;
import com.jiraplugin.metrics.model.EffectivenessData;

@Service
public class DeffectThread {
	@Value("${review_defect}") String rDefect;
	@Value("${delivery_defect}") String dDefect;
	@Value("${testing_defect}") String tDefect;
	@Autowired 
	DefectTypeThread t;
	@Async
	public CompletableFuture<DEffectivenessData> getDefectsCount(String p, int sDate,@AuthenticationPrincipal AtlassianHostUser hostUser) throws JSONException, ParseException, InterruptedException, ExecutionException {
		DEffectivenessData data = new DEffectivenessData();
		CompletableFuture<DefectTypeData> t0 = t.getTypeCount(p,"Review Defect (NWF)", sDate, hostUser);
		
		CompletableFuture<DefectTypeData> t1 = t.getTypeCount(p,"Delivery Defect (NWF)", sDate, hostUser);
		
		CompletableFuture<DefectTypeData> t2 = t.getTypeCount(p,"Testing Defect (NWF)", sDate, hostUser);
	
		
		CompletableFuture.allOf(t0,t1,t2).join();
		
		data.setrHCount(t0.get().gethCount());
		data.setrLCount(t0.get().getlCount());
		data.setrMCount(t0.get().getmCount());
		data.setrTCount(t0.get().gettCount());
		
		data.setdHCount(t1.get().gethCount());
		data.setdLCount(t1.get().getlCount());
		data.setdMCount(t1.get().getmCount());
		data.setdTCount(t1.get().gettCount());
		
		data.settHCount(t2.get().gethCount());
		data.settLCount(t2.get().getlCount());
		data.settMCount(t2.get().getmCount());
		data.settTCount(t2.get().gettCount());
		
		data.setiHCount(data.getrHCount() + data.gettHCount());
		data.setiLCount(data.getrLCount() + data.gettLCount());
		data.setiMCount(data.getrMCount() + data.gettMCount());
		data.setiTCount(data.getrTCount() + data.gettTCount());
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMM-yy");
		String m = (YearMonth.now().minusMonths(Math.abs(sDate))).format(formatter);
		/*data.setMonth(m);*/
		return CompletableFuture.completedFuture(data);
	}
	
	

}
