
package com.jiraplugin.metrics;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import javax.servlet.ServletException;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.jiraplugin.metrics.model.AppreciationData;
import com.jiraplugin.metrics.model.CIDIData;
import com.jiraplugin.metrics.model.ClientSatisfactionData;
import com.jiraplugin.metrics.model.ClientSatisfactionFA;
import com.jiraplugin.metrics.model.ComplaintData;
import com.jiraplugin.metrics.model.CreativeIdea;
import com.jiraplugin.metrics.model.CumulativeData;
import com.jiraplugin.metrics.model.DEffectivenessData;
import com.jiraplugin.metrics.model.EffectivenessData;
import com.jiraplugin.metrics.model.InitiativeData;
import com.jiraplugin.metrics.model.MetricsData;
import com.jiraplugin.metrics.model.ProjectData;
import com.jiraplugin.metrics.model.ResultRangeData;
import com.jiraplugin.metrics.model.RiskManagementData;
import com.jiraplugin.metrics.model.SelectChoice;
import com.jiraplugin.metrics.model.TestCase;
import com.jiraplugin.metrics.service.AppreciationService;
import com.jiraplugin.metrics.service.CasualAnalysis;
import com.jiraplugin.metrics.service.ClientSatisfaction;
import com.jiraplugin.metrics.service.ClientSatisfactionDateService;
import com.jiraplugin.metrics.service.ClientSatisfactionService;
import com.jiraplugin.metrics.service.ComplaintsService;
import com.jiraplugin.metrics.service.CriticalDeliveryRisk;
import com.jiraplugin.metrics.service.DEMonthsThread;
import com.jiraplugin.metrics.service.DeliveryService;
import com.jiraplugin.metrics.service.IntiativeThreadService;
import com.jiraplugin.metrics.service.JiraCustomFieldMap;
import com.jiraplugin.metrics.service.MetricsService;
import com.jiraplugin.metrics.service.MonthlyServiceMetrics;
import com.jiraplugin.metrics.service.MonthsService;
import com.jiraplugin.metrics.service.ProjectService;
import com.jiraplugin.metrics.service.RiskAcceptableExecutor;
import com.jiraplugin.metrics.service.RiskManagementService;
import com.jiraplugin.metrics.service.RiskService;
import com.jiraplugin.metrics.service.SprintService;
import com.jiraplugin.metrics.service.VelocityService;
import com.jiraplugin.metrics.service.WebHookHandler;

@Controller
@EnableJpaRepositories
public class UserController {
	@Value("${UserName}")
	String userName;
	@Value("${ClientSatisfactionSingle_IssueType}")
	String ClientSatisfaction_IT;

	@Value("${Quarter_Start_Date}")
	String qsd;
	@Value("${PassWord}")
	String password;
	@Value("${Domian_Url}")
	String Domain_URL;
	String projectChosen = null;
	Date selectedDate;
	@Autowired
	private AtlassianHostRestClients restClients;
	@Autowired
	private JiraCustomFieldMap jiraFieldList;
	ArrayList<String> projectList;
	@Autowired
	private ProjectService projectService;
	@Autowired
	private MetricsService metricsService;

	@Autowired
	ClientSatisfaction clientsat;
	@Autowired
	private CasualAnalysis casualAnalysis;

	@Autowired
	ProjectService j;
	@Autowired
	VelocityService v;
	@Autowired
	ClientSatisfactionService CliSatSer;

	@Autowired
	RiskManagementService riskManagementService;

	@GetMapping(value = "/clientSatisfactionDateProjectList")
	public String riskManagementDateRangeProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) {

		Map<String, String> result = new LinkedHashMap<>();
		result.put("target", "@{/clientSatisfactionDateRangeControl(jwt=${atlassianConnectToken})}");

		String sDate = "";
		String eDate = "";
		model.addAttribute("res", result);
		model.addAttribute("project_title", projectChosen);
		model.addAttribute("section_title", "Client Satisfaction");
		model.addAttribute("sDate", sDate);
		model.addAttribute("eDate", eDate);

		return "dateRangePage";
	}

	@GetMapping(value = "/riskManagmentDateRangeProjectList")
	public String riskManagmentDateRangeProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) {

		Map<String, String> result = new LinkedHashMap<>();
		result.put("target", "@{/riskManagementDateRangeControl(jwt=${atlassianConnectToken})}");

		String sDate = "";
		String eDate = "";
		model.addAttribute("res", result);
		model.addAttribute("project_title", projectChosen);
		model.addAttribute("section_title", "Risk Management");
		model.addAttribute("sDate", sDate);
		model.addAttribute("eDate", eDate);

		return "dateRangePage";
	}

	@GetMapping(value = "/riskManagementDateRangeControl")
	public String riskManagementDateRangeControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String dateObj, Model model) {

		Map<String, String> userDateRange = new LinkedHashMap<>();

		String[] parts = dateObj.split("~");
		String part1 = parts[0];
		String part2 = parts[1];

		userDateRange.put("d1", part1);
		userDateRange.put("d2", part2);

		RiskManagementData result = riskManagementService.getRiskRangeData(hostUser, projectChosen, userDateRange);

		model.addAttribute("res", result);

		return "riskManagementDateRangeView";
	}

	@Autowired
	ClientSatisfactionDateService clientSatisfactionService;

	@GetMapping(value = "/clientSatisfactionDateRangeControl")
	public String clientSatisfactionDateRangeControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@RequestParam("startdate") String dateObj, Model model) {

		Map<String, String> userDateRange = new LinkedHashMap<>();

		String[] parts = dateObj.split("~");
		String part1 = parts[0];
		String part2 = parts[1];

		userDateRange.put("d1", part1);
		userDateRange.put("d2", part2);

		Map<String, ClientSatisfactionData> result = clientSatisfactionService.getClientRating(hostUser, projectChosen,
				userDateRange);

		model.addAttribute("csat", result);

		return "clientSatisfactionDateRangeView";
	}

	@RequestMapping(value = "/cidiControl", method = RequestMethod.GET, produces = "application/json")
	public String cidiControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		Map<String, CIDIData> result = new HashMap<String, CIDIData>();

		try {
			result = CliSatSer.getCidiMethod(hostUser, projectChosen);

		} catch (Exception e) {
			System.out.println(e);

		}

		model.addAttribute("CIDI_Data", result);

		CIDIData s = result.get("Jul-18");

		return "cidiView";
	}

	DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat newFormat = new SimpleDateFormat("dd-MMM-yyyy");

	@RequestMapping(value = "/clientComplaint", method = RequestMethod.GET)
	public String clientComplaint(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		Map<String, ArrayList<ComplaintData>> result = new HashMap<String, ArrayList<ComplaintData>>();
		try {
			result = CliSatSer.getComplaint(hostUser, projectChosen);

		} catch (Exception e) {
			System.out.println(e);
		}
		for (Map.Entry<String, ArrayList<ComplaintData>> entry : result.entrySet()) {
			System.out.println(entry.getKey());
			for (int i = 0; i < entry.getValue().size(); i++) {
				System.out.println(ReflectionToStringBuilder.toString(entry.getValue().get(i)));
			}

		}
		model.addAttribute("data", result);
		return "complaints";
	}

	@RequestMapping(value = "/clientAppreciation", method = RequestMethod.GET)
	public String clientAppreciation(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		Map<String, ArrayList<AppreciationData>> result = new HashMap<String, ArrayList<AppreciationData>>();
		try {
			result = CliSatSer.getAppreciation(hostUser, projectChosen);

		} catch (Exception e) {
			System.out.println(e);
		}
		for (Map.Entry<String, ArrayList<AppreciationData>> entry : result.entrySet()) {
			System.out.println(entry.getKey());
			for (int i = 0; i < entry.getValue().size(); i++) {
				System.out.println(ReflectionToStringBuilder.toString(entry.getValue().get(i)));
			}

		}
		model.addAttribute("data", result);
		return "appreciationReport";
	}

	@RequestMapping(value = "/velocityProjectList", method = RequestMethod.GET)
	public String velocityProjectList(@ModelAttribute("mychoice") SelectChoice mychoice, Model model,
			@AuthenticationPrincipal AtlassianHostUser hostUser) throws IOException {

		isProjectSelected(mychoice);
		if (projectChosen != null) {
			String domain = hostUser.getHost().getBaseUrl();
			model.addAttribute("domain", domain);
			Map<String, String> keyData = j.getProjectsKeys();
			Map<String, Integer> projectId = v.getProjectsData(hostUser);
			Map<Integer, Integer> boardId = v.getBoardId(hostUser);
			Map<Integer, String> boardTypes = v.boardTypes;
			String key = keyData.get(projectChosen);
			Integer pid = projectId.get(key);
			Integer bid = boardId.get(pid);
			String btype = boardTypes.get(bid);
			model.addAttribute("key", key);
			model.addAttribute("pid", pid);
			model.addAttribute("bid", bid);
			model.addAttribute("btype", btype);
		}
		return "velocityProjectList";
	}

	public void isProjectSelected(SelectChoice mychoice) {
		try {
			if (projectChosen == null) {
				projectChosen = mychoice.getProjectname();
				selectedDate = mychoice.getSelectedDate();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	@RequestMapping(value = "/ReviewsMonthlyMetrics", method = RequestMethod.GET)
	public String metricsReportForReviews(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {

		isProjectSelected(mychoice);
		if (projectChosen != null) {
			int errorFlag = 0;
			ArrayList<MetricsData> performanceList = null;
			model.addAttribute("project_title", projectChosen);
			try {
				if (selectedDate != null)
					monthsDifference = monthsService.getMonthsDifference(selectedDate);
				performanceList = metricsService.getMonthlyDefects(projectChosen, hostUser, monthsDifference);

			} catch (Exception e) {

				System.out.println(e);

				errorFlag = 1;

			}

			model.addAttribute("proj", projectChosen);
			model.addAttribute("result", performanceList);

			if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "DEVELOPMENT EFFECTIVENESS");
				model.addAttribute("continue", "/monthlymetrics");
				model.addAttribute("back", "/velocityProjectList");
				return "exceptionErrorView";
			} else {

				return "ReviewsMonthlyMetrics";

			}
		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "/ReviewsMonthlyMetrics");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "/ReviewsMonthlyMetrics");
			return "landingPage";
		}

	}

	@Autowired
	MonthlyServiceMetrics msm;

	@RequestMapping(value = "/monthlyMetricsData", method = RequestMethod.GET)
	public String metricsData(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, InterruptedException, ExecutionException {
		int crh = 0, crm = 0, crl = 0, crt = 0, cth = 0, ctl = 0, ctm = 0, ctt = 0, cih = 0, cim = 0, cil = 0, cit = 0,
				cdh = 0, cdm = 0, cdl = 0, cdt = 0;
		isProjectSelected(mychoice);
		if (projectChosen != null) {
			int errorFlag = 0;
			ArrayList<MetricsData> listdata = new ArrayList<MetricsData>();
			try {
				listdata = metricsService.getMonthlyDefectsAsync(projectChosen, hostUser, monthsDifference);

				for (MetricsData m : listdata) {
					crh = crh + m.getD().getrHCount();
					crm = crm + m.getD().getrMCount();
					crl = crl + m.getD().getrLCount();
					crt = crt + m.getD().getrTCount();
					cth = cth + m.getD().gettHCount();
					ctl = ctl + m.getD().gettLCount();
					ctm = ctm + m.getD().gettMCount();
					ctt = ctt + m.getD().gettTCount();
					cih = cih + m.getD().getiHCount();
					cil = cil + m.getD().getiLCount();
					cim = cim + m.getD().getiMCount();
					cit = cit + m.getD().getiTCount();
					cdh = cdh + m.getD().getdHCount();
					cdl = cdl + m.getD().getdLCount();
					cdm = cdm + m.getD().getdMCount();
					cdt = cdt + m.getD().getdTCount();
					System.out.println(ReflectionToStringBuilder.toString(m));
					System.out
							.println("wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww");
				}
			} catch (Exception e) {

				e.printStackTrace();
				errorFlag = 1;

			}
			model.addAttribute("result", listdata);
			model.addAttribute("crh", crh);
			model.addAttribute("crl", crl);
			model.addAttribute("crm", crm);
			model.addAttribute("crt", crt);
			model.addAttribute("cth", cth);
			model.addAttribute("ctm", ctm);
			model.addAttribute("ctl", ctl);
			model.addAttribute("ctt", ctt);
			model.addAttribute("cih", cih);
			model.addAttribute("cil", cil);
			model.addAttribute("cim", cim);
			model.addAttribute("cit", cit);
			model.addAttribute("cdl", cdl);
			model.addAttribute("cdm", cdm);
			model.addAttribute("cdh", cdh);
			model.addAttribute("cdt", cdt);
			JSONArray jsonData = new JSONArray(listdata);

			JSONArray DRE = new JSONArray();
			try {
				for (int i = 0; i < jsonData.length(); i++) {
					JSONArray j = new JSONArray();
					j.put(((JSONObject) jsonData.get(i)).getString("month"));
					if (((JSONObject) jsonData.get(i)).getString("dreff") == "N/A") {
						j.put(0);
						j.put("N/A");
					} else {
						j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("dreff")));
						j.put(((JSONObject) jsonData.get(i)).getString("dreff"));
					}
					DRE.put(j);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			model.addAttribute("DRE", DRE);

			JSONArray defectsBar = new JSONArray();

			for (int i = 0; i < jsonData.length(); i++) {
				JSONArray j = new JSONArray();
				j.put(((JSONObject) jsonData.get(i)).getString("month"));
				j.put(((JSONObject) jsonData.get(i)).getInt("deld"));
				j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("deld")));
				j.put(((JSONObject) jsonData.get(i)).getInt("tesd"));
				j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("tesd")));
				j.put(((JSONObject) jsonData.get(i)).getInt("total_review_defects"));
				j.put(Integer.toString(((JSONObject) jsonData.get(i)).getInt("total_review_defects")));
				defectsBar.put(j);
			}
			model.addAttribute("defectsBar", defectsBar);

			JSONArray LineEff = new JSONArray();

			for (int i = 0; i < jsonData.length(); i++) {
				JSONArray j = new JSONArray();
				j.put(((JSONObject) jsonData.get(i)).getString("month"));
				if (((JSONObject) jsonData.get(i)).getString("revef") == "N/A") {
					j.put(0);
					j.put("N/A");
				} else {
					j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("revef")));
					j.put(((JSONObject) jsonData.get(i)).getString("revef"));
				}
				LineEff.put(j);
			}

			model.addAttribute("LineEff", LineEff);

			JSONArray TestLineEff = new JSONArray();
			JSONArray head = new JSONArray();
			head.put("Month");
			head.put("Testing Effectiveness(%)");
			TestLineEff.put(head);

			for (int i = 0; i < jsonData.length(); i++) {
				JSONArray j = new JSONArray();
				j.put(((JSONObject) jsonData.get(i)).getString("month"));
				if (((JSONObject) jsonData.get(i)).getString("testeff") == "N/A") {
					j.put(0);
				} else {
					j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("testeff")));
				}
				TestLineEff.put(j);
			}
			model.addAttribute("TestLineEff", TestLineEff);
		}

		return "MonthlyMetricsData";

	}

	@RequestMapping(value = "/monthlymetrics", method = RequestMethod.GET)
	public String metricsReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		model.addAttribute("project_title", projectChosen);
		return "MonthlyMetricsTable";
	}

	int monthsDifference = 0;

	// CAUSAL ANALYSIS

	@Autowired
	private MonthsService monthsService;

	@RequestMapping(value = "/cia", method = RequestMethod.GET)
	public String cia(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, InterruptedException, ExecutionException, Throwable {
		isProjectSelected(mychoice);

		if (projectChosen != null) {
			int errorFlag = 0;

			ArrayList<CreativeIdea> list = null;

			try {
				if (selectedDate != null) {
					monthsDifference = monthsService.getMonthsDifference(selectedDate);
				}
				// project = mychoice.getProjectname();
				list = casualAnalysis.getcasualanalysis(hostUser, projectChosen, monthsDifference);
				model.addAttribute("project_title", projectChosen);
				if (list.isEmpty()) {
					model.addAttribute("selectedIssueType", "CAUSAL ANALYSIS");
					model.addAttribute("project_title", projectChosen);
					model.addAttribute("continue", "/improvementProjectList");
					model.addAttribute("back", "/ccResult");
					return "errorView";
				}
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;

			}

			model.addAttribute("proj", projectChosen);
			model.addAttribute("result", list);

			if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "CAUSAL ANALYSIS");
				model.addAttribute("continue", "/improvementProjectList");
				model.addAttribute("back", "/ccResult");
				return "exceptionErrorView";
			} else {
				return "CasualAnalysis";
			}
		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "cia");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "cia");
			return "landingPage";
		}

	}

	ArrayList<MetricsData> performanceList;

	@RequestMapping(value = "/tests", method = RequestMethod.GET)
	public String testmetrics(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException {
		isProjectSelected(mychoice);
		ArrayList<TestCase> test_list = null;
		try {
			if (selectedDate != null) {
				monthsDifference = monthsService.getMonthsDifference(selectedDate);
			}
			performanceList = metricsService.getMonthlyDefects(projectChosen, hostUser, monthsDifference);
			test_list = metricsService.getMonthlyTestMetrics(projectChosen, hostUser, monthsDifference,
					performanceList);

		} catch (Exception e) {
			System.out.println(e);
		}
		model.addAttribute("result", test_list);

		JSONArray jsonData = new JSONArray(test_list);
		JSONArray SRF = new JSONArray();
		for (int i = 0; i < jsonData.length(); i++) {
			JSONArray j = new JSONArray();
			j.put(((JSONObject) jsonData.get(i)).getString("month"));
			if (((JSONObject) jsonData.get(i)).getString("srf") == "N/A") {
				j.put(0);
				j.put("N/A");
			} else {
				j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("srf")));
				j.put(((JSONObject) jsonData.get(i)).getString("srf"));
			}
			SRF.put(j);
		}

		System.out.println(SRF);
		model.addAttribute("srfData", SRF);
		return "TestsMetrics";

	}

	@RequestMapping(value = "/riskOpenIssuesControl", method = RequestMethod.GET)
	public String riskMitigateSelection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException {
		isProjectSelected(mychoice);
		if (projectChosen != null) {
			// String selectedProject = null;
			Map<Integer, List<String>> mapFieldsMain = new LinkedHashMap<Integer, List<String>>();
			int errorFlag = 0;

			try {
				// selectedProject = mychoice.getProjectname();

				mapFieldsMain = criticalDeliveryRisk.riskOpenItems(projectChosen, hostUser);

				model.addAttribute("maprisk", mapFieldsMain);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;
			}
			model.addAttribute("project_title", projectChosen);
			if (mapFieldsMain.isEmpty()) {

				model.addAttribute("selectedIssueType", "RISK ASSESSMENT AND TREATMENT PLAN - DELIVERY");
				model.addAttribute("continue", "/deliveryRiskControl");
				model.addAttribute("back", "/defectsReport");
				return "errorView";

			} else if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "RISK ASSESSMENT AND TREATMENT PLAN - DELIVERY");
				model.addAttribute("continue", "/deliveryRiskControl");
				model.addAttribute("back", "/defectsReport");
				return "exceptionErrorView";
			} else {

				return "RiskOpenIssuesView";
			}
		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			return "landingPage";
		}
	}

	public ArrayList<String> collectProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) {

		CompletableFuture<Integer> fieldcust = projectService.getFieldKeys(hostUser);
		CompletableFuture.allOf(fieldcust);
		ArrayList<String> projects = projectService.getProjects(hostUser);
		model.addAttribute("projectlist", projects);
		return projects;

		//

	}

	@GetMapping(value = "/landingPage")
	public String fieldlandingModelSeclection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, InterruptedException, ExecutionException {
		projectChosen = null;
		selectedDate = null;
		ArrayList<String> projects = collectProjectList(hostUser, mychoice, model);
		model.addAttribute("pageNavigator", "aboutProject");
		model.addAttribute("projectlist", projects);
		return "landingPage";
	}

	// About The Project

	@RequestMapping(value = "/aboutProjectInfo", method = RequestMethod.GET)
	public String fieldModelSeclection(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);

		Map<String, String> mapFieldsMain = new HashMap<String, String>();

		try {

			mapFieldsMain = criticalDeliveryRisk.riskOpenIssuesMethod(projectChosen, hostUser);
		} catch (Exception e) {
			System.out.println(e);
		}

		model.addAttribute("project_title", projectChosen);
		model.addAttribute("atpInfo", mapFieldsMain);
		System.out.println("hi");
		return "aboutProjectViews";

	}

	@RequestMapping(value = "/clientSatisfactionProjecList", method = RequestMethod.GET)
	public String clientSatisfactionProjecList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws IOException {
		int errorFlag = 0;

		String selectedProject = null, selectedQuarter = null;

		Map<String, String> dataString = null, overalCSAT = null;
		try {
			selectedQuarter = mychoice.getQuarter();
			dataString = clientsat.getCSAT(projectChosen, selectedQuarter, hostUser);
			overalCSAT = clientsat.getOveralCSAT(projectChosen, selectedQuarter, hostUser);

		} catch (Exception e) {
			System.out.println(e);
			errorFlag = 1;

		}

		model.addAttribute("project_title", projectChosen);
		model.addAttribute("client_name", dataString.get("cn"));
		model.addAttribute("sent_date", dataString.get("sd"));
		model.addAttribute("received_date", dataString.get("rd"));
		model.addAttribute("quarterPeriod", selectedQuarter);
		model.addAttribute("ab1", dataString.get("a1"));
		model.addAttribute("ab2", dataString.get("a2"));
		model.addAttribute("ab3", dataString.get("a3"));
		model.addAttribute("ab4", dataString.get("a4"));

		model.addAttribute("ab5", dataString.get("a5"));
		model.addAttribute("ab6", dataString.get("a6"));
		model.addAttribute("ab7", dataString.get("a7"));
		model.addAttribute("ab8", dataString.get("a8"));

		model.addAttribute("qua0", overalCSAT.get(clientsat.collView.get(0)));
		model.addAttribute("qua1", overalCSAT.get(clientsat.collView.get(1)));
		model.addAttribute("qua2", overalCSAT.get(clientsat.collView.get(2)));

		model.addAttribute("date0", clientsat.collView.get(0));
		model.addAttribute("date1", clientsat.collView.get(1));
		model.addAttribute("date2", clientsat.collView.get(2));

		if (errorFlag == 1) {
			model.addAttribute("selectedIssueType", "CLIENT SATISFACTION");
			model.addAttribute("continue", "/appreciationResult");
			model.addAttribute("back", "/clientSatisfactionControl");
			return "exceptionErrorView";
		} else {
			model.addAttribute("continue", "/appreciationResult");
			model.addAttribute("back", "/clientSatisfactionControl");
			return "clientSatisfactionView";
		}

	}

	@RequestMapping(value = "/clientSatisfactionControl", method = RequestMethod.GET)
	public String clientSatisfactionControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {
		try {
			isProjectSelected(mychoice);
			ArrayList<String> dateLink = new ArrayList<String>();
			ArrayList<ArrayList<String>> dateYearLink = new ArrayList<ArrayList<String>>();
			Calendar cal;
			ArrayList<ClientSatisfactionData> CSATdata = new ArrayList<ClientSatisfactionData>();
			DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
			ClientSatisfactionData clientSatisfactionData = new ClientSatisfactionData();
			RestTemplate restTemplate = new RestTemplate();

			cal = Calendar.getInstance();

			int monthCount = cal.get(Calendar.MONTH);

			int yearNumber = cal.get(Calendar.YEAR);

			System.out.println("Start of Code Service..!!! " + df.format(cal.getTime()));

			ArrayList<String> resultData = new ArrayList<String>();

			if (monthCount >= 0 && monthCount <= 3) {

				resultData.add("Jan-Apr(" + yearNumber + ")");
				dateLink.add("Q1(Jan-Apr)");
				dateLink.add(String.valueOf(yearNumber));
				dateYearLink.add(dateLink);

				resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
				dateLink.add("Q3(Sep-Dec)");
				dateLink.add(String.valueOf(yearNumber - 1));
				dateYearLink.add(dateLink);

				resultData.add("May-Aug(" + (yearNumber - 1) + ")");
				dateLink.add("Q2(May-Aug)");
				dateLink.add(String.valueOf(yearNumber - 1));
				dateYearLink.add(dateLink);

				resultData.add("Jan-Apr(" + (yearNumber - 1) + ")");
				dateLink.add("Q1(Jan-Apr)");
				dateLink.add(String.valueOf(yearNumber - 1));
				dateYearLink.add(dateLink);

				String[] dateLink1 = { "Q1(Jan-Apr)", "Q3(Sep-Dec)", "Q2(May-Aug)", "Q1(Jan-Apr)" };

				for (int i = 0; i < 4; i++) {
					dateLink = new ArrayList<String>();
					dateLink.add(dateLink1[i]);

					if (i == 0) {
						dateLink.add(String.valueOf(yearNumber));
					} else {
						dateLink.add(String.valueOf(yearNumber - 1));
					}
					dateYearLink.add(dateLink);
				}

			} else if (monthCount >= 4 && monthCount <= 7) {

				resultData.add("May-Aug(" + yearNumber + ")");
				resultData.add("Jan-Apr(" + (yearNumber) + ")");
				resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");
				resultData.add("May-Aug(" + (yearNumber - 1) + ")");

				String[] dateLink1 = { "Q2(May-Aug)", "Q1(Jan-Apr)", "Q3(Sep-Dec)", "Q2(May-Aug)" };

				for (int i = 0; i < 4; i++) {
					dateLink = new ArrayList<String>();
					dateLink.add(dateLink1[i]);

					if (i >= 2) {
						dateLink.add(String.valueOf(yearNumber - 1));
					} else {
						dateLink.add(String.valueOf(yearNumber));
					}
					dateYearLink.add(dateLink);
				}

			} else if (monthCount >= 8 && monthCount <= 11) {

				resultData.add("Sep-Dec(" + yearNumber + ")");
				resultData.add("May-Aug(" + (yearNumber) + ")");
				resultData.add("Jan-Apr(" + (yearNumber) + ")");
				resultData.add("Sep-Dec(" + (yearNumber - 1) + ")");

				String[] dateLink1 = { "Q3(Sep-Dec)", "Q2(May-Aug)", "Q1(Jan-Apr)", "Q3(Sep-Dec)" };

				for (int i = 0; i < 4; i++) {
					dateLink = new ArrayList<String>();
					dateLink.add(dateLink1[i]);
					if (i == 3) {
						dateLink.add(String.valueOf(yearNumber - 1));
					} else {
						dateLink.add(String.valueOf(yearNumber));
					}
					dateYearLink.add(dateLink);
				}

			}

			Map<String, ClientSatisfactionData> resultArray = new HashMap<String, ClientSatisfactionData>();

			restTemplate.getInterceptors()
					.add(new BasicAuthorizationInterceptor(userName + "@senecaglobal.com", password));

			String urlString = Domain_URL;
			String urlPara = "/rest/api/2/search?jql=project={pro} AND issuetype={csat} AND ({qsd} = 'Q1(Jan-Apr)' OR {qsd} = 'Q2(May-Aug)' OR {qsd} = 'Q3(Sep-Dec)') AND 'Year' ~ {YearNumber} & startAt= {startAtCount} & maxResults=100";
			String domain = urlString + urlPara;

			Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

			int initialTotal = 0;
			int count = 1;
			int startAtCount = 0;

			while (initialTotal < count) {

				String response2 = restClients.authenticatedAs(hostUser)
						.getForObject(UriComponentsBuilder.fromUriString(urlPara)
								.buildAndExpand("\"" + projectChosen + "\"", "\"" + ClientSatisfaction_IT + "\"",
										"\"" + qsd + "\"", "\"" + qsd + "\"", "\"" + qsd + "\"",
										"\"" + yearNumber + "\"", "\"" + startAtCount + "\"")
								.toUri(), String.class);

				JSONObject response = new JSONObject(response2);
				count = response.getInt("total");
				int issueMaxResult = response.getInt("maxResults");
				System.out.println(count + issueMaxResult);

				CSATdata = parseMethod2(hostUser, response);

				initialTotal = initialTotal + issueMaxResult;

			}

			for (int i = 0; i < 4; i++) {

				int flag = 0;
				int lastElement = 0;

				for (int j = 0; j < CSATdata.size(); j++) {
					lastElement++;

					try {

						if (dateYearLink.get(i).get(0).equals(CSATdata.get(j).getQuarter_Start_Date())
								&& dateYearLink.get(i).get(1).equals(CSATdata.get(j).getYear())) {

							flag = 1;

							resultArray.put(resultData.get(i), CSATdata.get(j));

							break;
						}

					} catch (Exception e) {
						System.out.println(e);

					}

				}
				if (flag == 0) {

					clientSatisfactionData = new ClientSatisfactionData();

					clientSatisfactionData.setCSAT_Q6("-");
					CSATdata.add(clientSatisfactionData);
					resultArray.put(resultData.get(i), CSATdata.get(lastElement));

				}

			}

			cal = Calendar.getInstance();

			System.out.println("End of Code Service..!!! " + df.format(cal.getTime()));

			JSONArray CSAT = new JSONArray();
			for (Map.Entry<String, ClientSatisfactionData> entry : resultArray.entrySet()) {
				JSONArray j = new JSONArray();
				j.put(entry.getKey());
				String val = entry.getValue().getCSAT_Q6();
				if (val == "-") {
					j.put(0);
					j.put("N/A");
					j.put("N/A");
				} else if (val.equals("1 - Very Dissatisfied")) {
					j.put(1);
					j.put("1");
					j.put("1");
				} else if (val.equals("2")) {
					j.put(2);
					j.put("2");
					j.put("2");
				} else if (val.equals("3 - Satisfied")) {
					j.put(3);
					j.put("3");
					j.put("3");
				} else if (val.equals("4")) {
					j.put(4);
					j.put("4");
					j.put("4");
				} else {
					j.put(5);
					j.put("5");
					j.put("5");
				}
				CSAT.put(j);
			}
			model.addAttribute("CSAT", CSAT);
			model.addAttribute("csat", resultArray);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "clientSatisfactionView";
	}

	@Value("${CSAT_Value_Q1}")
	String CSAT_Q1;
	@Value("${CSAT_Value_Q2}")
	String CSAT_Q2;
	@Value("${CSAT_Value_Q3}")
	String CSAT_Q3;
	@Value("${CSAT_Value_Q4}")
	String CSAT_Q4;
	@Value("${CSAT_Value_Q5}")
	String CSAT_Q5;
	@Value("${CSAT_Value_Q6}")
	String CSAT_Q6;
	@Value("${CSAT_Value_Q7}")
	String CSAT_Q7;
	@Value("${CSAT_Value_Q8}")
	String CSAT_Q8;

	ArrayList<ClientSatisfactionData> parseMethod2(AtlassianHostUser hostUser, JSONObject response)
			throws Exception, ParseException {
		ClientSatisfactionData clientSatisfactionData;
		Map<String, String> fieldList = jiraFieldList.getJiraFieldList(hostUser);

		ArrayList<ClientSatisfactionData> CSATdata = new ArrayList<>();
		String cn = fieldList.get("Client Name");
		String sd = fieldList.get("Sent Date");
		String rd = fieldList.get("Received Date");
		String qsd = fieldList.get("Quarter");
		String qed = fieldList.get("Year");
		String qa1 = fieldList.get(CSAT_Q1);
		String qa2 = fieldList.get(CSAT_Q2);
		String qa3 = fieldList.get(CSAT_Q3);
		String qa4 = fieldList.get(CSAT_Q4);
		String qa5 = fieldList.get(CSAT_Q5);
		String qa6 = fieldList.get(CSAT_Q6);
		String qa7 = fieldList.get(CSAT_Q7);
		String qa8 = fieldList.get(CSAT_Q8);

		JSONArray jsonArray = response.getJSONArray("issues");
		for (int i = 0; i < jsonArray.length(); i++) {

			JSONObject j = jsonArray.getJSONObject(i);
			clientSatisfactionData = new ClientSatisfactionData();
			JSONObject fields = j.getJSONObject("fields");

			clientSatisfactionData.setClient_Name(fields.get(cn).toString());

			Date date1 = formatter.parse(fields.get(sd).toString());
			String finalDate1 = newFormat.format(date1);
			clientSatisfactionData.setSent_Date(finalDate1);

			Date date2 = formatter.parse(fields.get(rd).toString());
			String finalDate2 = newFormat.format(date2);
			clientSatisfactionData.setReceived_Date(finalDate2);

			/*
			 * Date date3 = formatter.parse(fields.get(qsd).toString()); String finalDate3 =
			 * newFormat.format(date3);
			 * clientSatisfactionData.setQuarter_Start_Date(finalDate3);
			 */
			clientSatisfactionData.setQuarter_Start_Date(fields.getJSONObject(qsd).getString("value"));

			/*
			 * Date date4 = formatter.parse(fields.get(qed).toString()); String finalDate4 =
			 * newFormat.format(date4);
			 * clientSatisfactionData.setQuarter_End_Date(finalDate4);
			 */

			clientSatisfactionData.setYear(fields.get(qed).toString());

			clientSatisfactionData.setCSAT_Q1(fields.getJSONObject(qa1).getString("value"));
			clientSatisfactionData.setCSAT_Q2(fields.getJSONObject(qa2).getString("value"));
			clientSatisfactionData.setCSAT_Q3(fields.getJSONObject(qa3).getString("value"));
			clientSatisfactionData.setCSAT_Q4(fields.getJSONObject(qa4).getString("value"));
			clientSatisfactionData.setCSAT_Q5(fields.getJSONObject(qa5).getString("value"));
			clientSatisfactionData.setCSAT_Q6(fields.getJSONObject(qa6).getString("value"));
			clientSatisfactionData.setCSAT_Q7(fields.get(qa7).toString());
			clientSatisfactionData.setCSAT_Q8(fields.get(qa8).toString());

			String respondentSkipped = "Respondent skipped this question";

			if (clientSatisfactionData.getCSAT_Q1().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q1(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q2().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q2(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q3().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q3(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q4().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q4(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q5().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q5(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q6().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q6(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q7().contentEquals("null")) {

				clientSatisfactionData.setCSAT_Q7(respondentSkipped);
			}
			if (clientSatisfactionData.getCSAT_Q8().contentEquals("null")) {
				clientSatisfactionData.setCSAT_Q8(respondentSkipped);
			}

			CSATdata.add(clientSatisfactionData);

		}
		return CSATdata;
	}

	@Autowired
	private RiskService rs;
	@Autowired
	RiskAcceptableExecutor riskAcceptableExecutor;
	@Autowired
	CriticalDeliveryRisk criticalDeliveryRisk;

	@RequestMapping(value = "/deliveryRiskControl", method = RequestMethod.GET)
	public String deliveryRiskControl(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		String snapProject = "New Project Design";
		Map<String, String> projkeys = projectService.getProjectsKeys();
		String pkey2 = projkeys.get(projectChosen);

		int errorFlag = 0;
		try {

			// Delivery Risks Treated to Acceptable Level

			CompletableFuture<ArrayList<ArrayList<String>>> acceptableLevelResults = riskAcceptableExecutor
					.startThreads(hostUser, snapProject, pkey2);

			// Delivery Risk Exposure Rating - After Risk Treatment

			CompletableFuture<ArrayList<ResultRangeData>> exposureRatingResults = rs.getProjectRangeData(snapProject,
					hostUser, pkey2);

			// Critical Delivery Risks Identified

			String riskUrl = "https://seneca-global.atlassian.net/rest/api/2/search?jql=project='" + snapProject
					+ "' AND issuetype= 'Risk_Identified' AND 'Risk Probability (RP)'!='2' AND 'Risk Probability (RP)'!='1' AND 'Reported Date' >= startOfMonth({date}) AND 'Reported Date' <= endOfMonth({date}) AND 'Client Name' ~ {promain}";

			CompletableFuture<ArrayList<ArrayList<String>>> criticalDeliveryResults = criticalDeliveryRisk
					.startThreads(hostUser, snapProject, riskUrl, pkey2);

			// Risk Occurrence

			String riskUrl2 = "/rest/api/2/search?jql=project='" + projectChosen
					+ "' AND issuetype = 'Risk Assessment' AND 'Risk Occurrence Date' >= startOfMonth({date}) AND 'Risk Occurrence Date' <= endOfMonth({date})";

			CompletableFuture<ArrayList<ArrayList<String>>> riskOccurrenceResults = criticalDeliveryRisk
					.ocurrenceThreads(hostUser, pkey2, riskUrl2);

			// Risk Recurrence

			/*
			 * CompletableFuture<ArrayList<ArrayList<String>>> recurrenceResults =
			 * criticalDeliveryRisk .riskRecurrenceThreads(hostUser, snapProject, pkey2);
			 */

			// Wait until all the threads are done

			CompletableFuture.allOf(acceptableLevelResults, exposureRatingResults, criticalDeliveryResults,
					riskOccurrenceResults).join();

			ArrayList<ResultRangeData> ratingResults = exposureRatingResults.get();
			JSONArray jsonRatingResults = new JSONArray(ratingResults);

			JSONArray riskRatingLine = new JSONArray();

			for (int i = 0; i < jsonRatingResults.length(); i++) {
				JSONArray j = new JSONArray();
				j.put(((JSONObject) jsonRatingResults.get(i)).getString("month"));
				j.put(((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk"));
				if ((((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk")) == 0) {
					j.put("N/A");
					j.put("N/A");
				} else {
					j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk")));
					j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_risk")));
				}
				j.put(((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual"));
				if ((((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual")) == 0) {
					j.put("N/A");
					j.put("N/A");
				} else {
					j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual")));
					j.put(Integer.toString(((JSONObject) jsonRatingResults.get(i)).getInt("avg_residual")));
				}
				riskRatingLine.put(j);
			}
			model.addAttribute("riskRatingLine", riskRatingLine);

			model.addAttribute("project_title", projectChosen);
			model.addAttribute("alr", acceptableLevelResults.get());
			model.addAttribute("err", exposureRatingResults.get());
			model.addAttribute("cdr", criticalDeliveryResults.get());
			model.addAttribute("ror", riskOccurrenceResults.get());
			/* model.addAttribute("rr", recurrenceResults.get()); */
		} catch (Exception e) {
			System.out.println(e);
			errorFlag = 1;
		}
		return "deliveryRiskView";
	}

	@Autowired
	private WebHookHandler obj;

	@ResponseBody
	@RequestMapping(consumes = "application/json", produces = "application/json", value = "/issueUpdated", method = RequestMethod.POST)
	public String issueUpdated(@AuthenticationPrincipal AtlassianHostUser hostUser, @RequestBody String json)
			throws ServletException, IOException, JSONException {

		int errorFlag = 0;
		try {

			String receivedData = json;

			System.out.println("webhook issue updated" + json);

			obj.updateIssue(json, hostUser);

		} catch (Exception e) {
			System.out.println(e);
			errorFlag = 1;
		}

		if (errorFlag == 1) {
			return "webHookDataErrorView";
		} else {
			return "webHookData";
		}

	}

	/*
	 * @RequestMapping(value = "/dMProjectList", method = RequestMethod.GET) public
	 * String dMProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
	 *
	 * @ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws
	 * IOException { List<String> projects = ProjectService.list;
	 * model.addAttribute("projectlist", projects); return "dMProjectList"; }
	 */

	@Autowired
	DeliveryService d;

	// Individual defects count
	@RequestMapping(value = "/iReport", method = RequestMethod.GET)
	public String iReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {
		isProjectSelected(mychoice);
		if (projectChosen != null) {
			// String p = null;
			ArrayList<EffectivenessData> data = null;
			int errorFlag = 0;

			try {
				// p = mychoice.getProjectname();
				model.addAttribute("projectname", projectChosen);
				data = d.runThreadsOne(projectChosen, hostUser);
				model.addAttribute("data", data);
			} catch (Exception e) {
				errorFlag = 1;
			}
			model.addAttribute("project_title", projectChosen);
			if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "DELIVERY MANAGEMENT REPORTS");
				model.addAttribute("continue", "/defectsReport");
				model.addAttribute("back", "/tests");
				return "exceptionErrorView";
			} else {
				model.addAttribute("continue", "/defectsReport");
				model.addAttribute("back", "/tests");
				return "iReport";
			}
		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "iReport");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "iReport");
			return "landingPage";
		}
	}
	/*
	 * @RequestMapping(value = "/countProjectList", method = RequestMethod.GET)
	 * public String countProjectList(@AuthenticationPrincipal AtlassianHostUser
	 * hostUser,
	 *
	 * @ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws
	 * IOException { List<String> projects = ProjectService.list;
	 * model.addAttribute("projectlist", projects); return "countProjectList"; }
	 */

	// Defect Distribution Data
	@Autowired
	DEMonthsThread t;

	@RequestMapping(value = "/defectsReport", method = RequestMethod.GET)
	public String defectsReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {
		isProjectSelected(mychoice);
		if (projectChosen != null) {
			int rHCount = 0, rMCount = 0, rLCount = 0, rTCount = 0, tHCount = 0, tMCount = 0, tLCount = 0, tTCount = 0,
					iHCount = 0, iMCount = 0, iLCount = 0, iTCount = 0, dHCount = 0, dMCount = 0, dLCount = 0,
					dTCount = 0;
			int errorFlag = 0;
			// String p = null;
			try {
				// p = mychoice.getProjectname();
				model.addAttribute("project_title", projectChosen);
				ArrayList<DEffectivenessData> data = t.runMonthsThreads(projectChosen, hostUser);
				model.addAttribute("data", data);
				CumulativeData cumulativeData = new CumulativeData();
				for (DEffectivenessData d : data) {
					rHCount += d.getrHCount();
					rMCount += d.getrMCount();
					rLCount += d.getrLCount();
					rTCount += d.getrTCount();
					tHCount += d.gettHCount();
					tMCount += d.gettMCount();
					tLCount += d.gettLCount();
					tTCount += d.gettTCount();
					iHCount += d.getiHCount();
					iLCount += d.getiLCount();
					iMCount += d.getiMCount();
					iTCount += d.getiTCount();
					dHCount += d.getdHCount();
					dMCount += d.getdMCount();
					dLCount += d.getdLCount();
					dTCount += d.getdTCount();
				}
				cumulativeData.setrHCount(rHCount);
				cumulativeData.setrMCount(rMCount);
				cumulativeData.setrLCount(rLCount);
				cumulativeData.setrTCount(rTCount);
				cumulativeData.settHCount(tHCount);
				cumulativeData.settLCount(tLCount);
				cumulativeData.settMCount(tMCount);
				cumulativeData.settTCount(tTCount);
				cumulativeData.setiHCount(iHCount);
				cumulativeData.setiLCount(iLCount);
				cumulativeData.setiMCount(iMCount);
				cumulativeData.setiTCount(iTCount);
				cumulativeData.setdHCount(dHCount);
				cumulativeData.setdLCount(dLCount);
				cumulativeData.setdMCount(dMCount);
				cumulativeData.setdTCount(dTCount);
				model.addAttribute("cData", cumulativeData);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;
			}
		}
		return "defectsReport";

	}

	// Delivery Improvement Initiatives

	@Autowired
	IntiativeThreadService r;

	@RequestMapping(value = "/improvementProjectList", method = RequestMethod.GET)
	public String improvementProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		if (projectChosen != null) {
			int errorFlag = 0;
			ArrayList<InitiativeData> data = null;
			// String p = null;
			try {
				// p = mychoice.getProjectname();

				data = r.runInitiativeThreads(projectChosen, hostUser);
				model.addAttribute("data", data);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;

			}
			model.addAttribute("project_title", projectChosen);
			if (data.isEmpty()) {
				model.addAttribute("selectedIssueType", "DELIVERY IMPROVEMENT INITIATIVES");
				model.addAttribute("project_title", projectChosen);
				model.addAttribute("continue", "/velocityProjectList");
				model.addAttribute("back", "/cia");
				return "errorView";
			} else if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "DELIVERY IMPROVEMENT INITIATIVES");
				model.addAttribute("continue", "/velocityProjectList");
				model.addAttribute("back", "/cia");
				return "exceptionErrorView";
			}

			else
				return "improvementResult";
		}

		else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "improvementProjectList");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "improvementProjectList");
			return "landingPage";
		}
	}

	// Client Complaints
	@Autowired
	ComplaintsService c;

	@RequestMapping(value = "/ccResult", method = RequestMethod.GET)
	public String ccResult(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {
		isProjectSelected(mychoice);
		ArrayList<ComplaintData> data = new ArrayList<ComplaintData>();
		ArrayList<ComplaintData> data1 = new ArrayList<ComplaintData>();
		if (projectChosen != null) {
			int errorFlag = 0;
			try {
				data = c.getComplaintData(hostUser, projectChosen);
				data1 = c.getClosedData(hostUser, projectChosen, -2, 0);
				data.addAll(data1);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;
			}
			model.addAttribute("project_title", projectChosen);
			model.addAttribute("data", data);
			if (data.isEmpty()) {
				model.addAttribute("project_title", projectChosen);
				model.addAttribute("selectedIssueType", "CLIENT COMPLAINTS");
				model.addAttribute("continue", "/cia");
				model.addAttribute("back", "/appreciationReport");
				return "errorView";

			} else if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "CLIENT COMPLAINTS");
				model.addAttribute("continue", "/cia");
				model.addAttribute("back", "/appreciationReport");
				return "exceptionErrorView";

			} else {
				model.addAttribute("continue", "/cia");
				model.addAttribute("back", "/appreciationReport");
				return "ccResult";
			}

		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "ccResult");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "ccResult");
			return "landingPage";
		}

	}

	@RequestMapping(value = "/cSFAControlMethod", method = RequestMethod.GET)
	public String cSFAControlMethod(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException, ParseException, InterruptedException, ExecutionException {
		isProjectSelected(mychoice);
		Map<String, ArrayList<ClientSatisfactionFA>> result = new HashMap<String, ArrayList<ClientSatisfactionFA>>();

		int errorFlag = 0;

		try {

			result = CliSatSer.getCSFA(hostUser, projectChosen);

		} catch (Exception e) {
			System.out.println(e);
		}
		for (Map.Entry<String, ArrayList<ClientSatisfactionFA>> entry : result.entrySet()) {
			System.out.println(entry.getKey());
			for (int i = 0; i < entry.getValue().size(); i++) {
				System.out.println(ReflectionToStringBuilder.toString(entry.getValue().get(i)));
			}

		}
		model.addAttribute("dataCFA", result);

		if (result.isEmpty()) {

			model.addAttribute("selectedIssueType", "Client Satisfaction Feedback Ascpect");
			return "errorView";

		} else if (errorFlag == 1) {
			model.addAttribute("selectedIssueType", "Client Satisfaction Feedback Ascpect");
			return "exceptionErrorView";
		} else {

			return "csfAspect";
		}

	}
	/*
	 * @RequestMapping(value = "/velocityProjectList", method = RequestMethod.GET)
	 * public String velocityProjectList(@ModelAttribute("mychoice") SelectChoice
	 * mychoice, Model model) throws IOException { List<String> projects =
	 * ProjectService.list; model.addAttribute("projectlist", projects); return
	 * "velocityProjectList"; }
	 *
	 * @RequestMapping(value = "/velocityReport", method = RequestMethod.GET) public
	 * String velocityReport(@ModelAttribute("mychoice") SelectChoice mychoice,
	 * Model model) throws IOException, JSONException { return "velocityReport"; }
	 */

	/*
	 * @Autowired private AuditsService auditsService;
	 *
	 * @RequestMapping(value = "/SelectAuditReports", method = RequestMethod.GET)
	 * public String SelectAuditReports(@ModelAttribute("mychoice") SelectChoice
	 * mychoice, Model model) throws IOException {
	 *
	 * List<String> projects = ProjectService. List<String> years =
	 * monthsService.getFinancialyears(); List<String> cycles =
	 * monthsService.getCycles(); model.addAttribute("projectlist", projects);
	 * model.addAttribute("years", years); model.addAttribute("cycles", cycles);
	 * return "SelectAuditReports"; }
	 */

	/*
	 * @RequestMapping(value = "/auditReports", method = RequestMethod.GET) public
	 * String AuditReports(@AuthenticationPrincipal AtlassianHostUser hostUser,
	 *
	 * @ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws
	 * Exception { String p = mychoice.getProjectname(); String cycle =
	 * mychoice.getCycle();
	 *
	 * String audityear = mychoice.getAudityear();
	 *
	 * AuditInfo inf = auditsService.getAuditReports(p, cycle, audityear, hostUser);
	 * model.addAttribute("result", inf); model.addAttribute("projectName",
	 * mychoice.getProjectname()); return "InternalAuditReport"; }
	 */

	@RequestMapping(value = "/appreciationPList", method = RequestMethod.GET)
	public String appreciationProjectList(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws IOException {
		List<String> projects = projectService.getProjects(hostUser);
		model.addAttribute("projectlist", projects);
		return "appreciationProjectList";
	}

	@Autowired
	AppreciationService a;

	@RequestMapping(value = "/appreciationReport", method = RequestMethod.GET)
	public String appreciationReport(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws IOException, JSONException {
		isProjectSelected(mychoice);
		ArrayList<AppreciationData> data = new ArrayList<AppreciationData>();
		if (projectChosen != null) {
			int errorFlag = 0;
			try {

				data = a.getAppreciationData(hostUser, projectChosen);
			} catch (Exception e) {
				System.out.println(e);
				errorFlag = 1;
			}
			model.addAttribute("project_title", projectChosen);
			model.addAttribute("data", data);
			if (data.isEmpty()) {
				model.addAttribute("project_title", projectChosen);
				model.addAttribute("selectedIssueType", "CLIENT APPRECIATIONS");
				model.addAttribute("continue", "/ccResult");
				model.addAttribute("back", "/clientSatisfactionControl");
				return "errorView";

			} else if (errorFlag == 1) {
				model.addAttribute("selectedIssueType", "CLIENT APPRECIATIONS");
				model.addAttribute("continue", "/ccResult");
				model.addAttribute("back", "/clientSatisfactionControl");
				return "exceptionErrorView";

			} else {
				model.addAttribute("continue", "/ccResult");
				model.addAttribute("back", "/clientSatisfactionControl");
				return "appreciationReport";
			}

		} else if (projectList == null) {
			collectProjectList(hostUser, mychoice, model);
			model.addAttribute("pageNavigator", "appreciationReport");
			return "landingPage";
		} else {
			model.addAttribute("projectlist", projectList);
			model.addAttribute("pageNavigator", "appreciationReport");
			return "landingPage";
		}

	}

	@RequestMapping(value = "/modeProjectList", method = RequestMethod.GET)
	public String modeProjectList(@ModelAttribute("mychoice") SelectChoice mychoice, Model model)
			throws IOException, JSONException {
		Map<Integer, ProjectData> projectIdsMap = projectService.getProjectData();
		model.addAttribute("projectlist", projectIdsMap);
		return "modeProjectList";
	}

	@Autowired
	SprintService ss;

	@RequestMapping(value = "/modeResult", method = RequestMethod.GET)
	public String modeResult(@AuthenticationPrincipal AtlassianHostUser hostUser,
			@ModelAttribute("mychoice") SelectChoice mychoice, Model model) throws Exception {
		int monthsDifference = 0;
		ArrayList<MetricsData> list = new ArrayList<MetricsData>();
		int errorFlag = 0;
		String id = mychoice.getCatId();
		String[] arrOfStr = id.split("~");
		String projectChosen = arrOfStr[2];
		model.addAttribute("projectname", arrOfStr[2]);
		if (arrOfStr[1].equals("Release Mode")) {
			list = ss.getSprintsData(Integer.parseInt(arrOfStr[0]), hostUser);
			model.addAttribute("type", "Iteration");
		} else {
			try {

				Date selectedDate = mychoice.getSelectedDate();
				if (selectedDate != null)
					monthsDifference = monthsService.getMonthsDifference(selectedDate);
				list = metricsService.getMonthlyDefects(projectChosen, hostUser, monthsDifference);
				model.addAttribute("type", "Month-Year");
			} catch (Exception e) {
				System.out.println(e);

				errorFlag = 1;

			}
		}

		model.addAttribute("proj", projectChosen);
		model.addAttribute("result", list);

		JSONArray jsonData = new JSONArray(list);

		JSONArray defectsBar = new JSONArray();
		JSONArray header = new JSONArray();
		header.put("Month");
		header.put("Delivery Defects");
		header.put("Testing Defects");
		header.put("Review Defects");
		defectsBar.put(header);

		for (int i = 0; i < jsonData.length(); i++) {
			JSONArray j = new JSONArray();
			j.put(((JSONObject) jsonData.get(i)).getString("month"));
			j.put(((JSONObject) jsonData.get(i)).getInt("deld"));
			j.put(((JSONObject) jsonData.get(i)).getInt("tesd"));
			j.put(((JSONObject) jsonData.get(i)).getInt("revd"));
			defectsBar.put(j);
		}
		model.addAttribute("defectsBar", defectsBar);

		JSONArray LineEff = new JSONArray();
		JSONArray heading = new JSONArray();
		heading.put("Month");
		heading.put("Review Effectiveness(%)");
		LineEff.put(heading);

		for (int i = 0; i < jsonData.length(); i++) {
			JSONArray j = new JSONArray();
			j.put(((JSONObject) jsonData.get(i)).getString("month"));
			if (((JSONObject) jsonData.get(i)).getString("revef") == "N/A") {
				j.put(0);
			} else {
				j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("revef")));
			}
			LineEff.put(j);
		}

		model.addAttribute("LineEff", LineEff);

		JSONArray TestLineEff = new JSONArray();
		JSONArray head = new JSONArray();
		head.put("Month");
		head.put("Testing Effectiveness(%)");
		TestLineEff.put(head);

		for (int i = 0; i < jsonData.length(); i++) {
			JSONArray j = new JSONArray();

			j.put(((JSONObject) jsonData.get(i)).getString("month"));
			if (((JSONObject) jsonData.get(i)).getString("testeff") == "N/A") {
				j.put(0);
			} else {
				j.put(Float.valueOf(((JSONObject) jsonData.get(i)).getString("testeff")));
			}
			TestLineEff.put(j);
		}

		model.addAttribute("TestLineEff", TestLineEff);

		if (list.isEmpty()) {
			model.addAttribute("selectedIssueType", "DEVELOPMENT EFFECTIVENESS");
			model.addAttribute("project_title", projectChosen);
			model.addAttribute("continue", "/tests");
			model.addAttribute("back", "/improvementResult");
			return "errorView";

		} else if (errorFlag == 1) {
			model.addAttribute("selectedIssueType", "DEVELOPMENT EFFECTIVENESS");
			model.addAttribute("continue", "/tests");
			model.addAttribute("back", "/improvementResult");
			return "exceptionErrorView";
		} else {

			return "MonthlyMetricsTable";

		}

	}

}