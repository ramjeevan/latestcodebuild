<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>


<body>

<div class="container">
  <h3>Login with your JIRA credentials</h3>
    <form method="POST" modelAttribute="loginForm" class="form-signin">
     

        <div class="form-group ${error != null ? 'has-error' : ''}">
            <span>${message}</span>
            <input required="required" name="username" type="text" class="form-control" placeholder="Username"
                   autofocus="true"/>
            <input name="password" type="password" class="form-control" placeholder="Password"/>
            <span>${error}</span>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

            <button class="btn btn-lg btn-primary btn-block" type="submit">Log In</button>
           
        </div>

    </form>

</div>

</body>
</html>
